export class Address {
  //#region  Private Memebers
  #houseInfo;
  #street;
  #landmark;
  #pinCode;
  #geoLocation;
  //#endregion
  //#region  Construtor
  constructor(houseInfo, street, landmark, pinCode, geoLocation) {
    this.#houseInfo = houseInfo;
    this.#street = street;
    this.#landmark = landmark;
    this.#pinCode = pinCode;
    this.#geoLocation = geoLocation;
  }
  //#endregion
  //#region  Getter
  get houseInfo() {
    return this.#houseInfo;
  }
  get street() {
    return this.#street;
  }
  get landmark() {
    return this.#landmark;
  }
  get pinCode() {
    return this.#pinCode;
  }
  get geoLocation() {
    return this.#geoLocation;
  }
  //#endregion
}

export const AddressType = {
  HOME: 'home',
  OFFICE: 'office',
};
