import { AddressType } from './address.model.js';
export class User {
  //#region  Private Memebers
  #email;
  #name;
  #phoneNumber;
  #customerId;
  #address = {};
  //#endregion

  //#region  Construtor
  constructor(email, name, phoneNumber, customerId) {
    this.#email = email;
    this.#name = name;
    this.#phoneNumber = phoneNumber;
    this.#customerId = customerId;
  }
  //#endregion

  //#region  Getter

  /**
   * @returns {string} email
   */
  get email() {
    return this.#email;
  }

  /**
   * @returns {string} name
   */
  get name() {
    return this.#name;
  }

  /**
   * @returns {string} phoneNumber
   */
  get phoneNumber() {
    return this.#phoneNumber;
  }

  /**
   * @returns {string} customerId
   */
  get customerId() {
    return this.#customerId;
  }

  /**
   * @returns {Address} home address
   */
  get homeAddress() {
    return this.#address[AddressType.HOME];
  }

  /**
   * @returns {Address} office address
   */
  get OfficeAddress() {
    return this.#address[AddressType.OFFICE];
  }
  //#endregion

  /**
   * @param {Address} address
   */
  set addressHome(address) {
    this.#address[AddressType.HOME] = address;
  }

  /**
   * @param {Address} address
   */
  set addressOffice(address) {
    this.#address[AddressType.OFFICE] = address;
  }
}
