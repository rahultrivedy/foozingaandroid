import NetInfo from '@react-native-community/netinfo';

NetInfo.fetch().then(state => {
  global.isinternet = state.isConnected;
});

export function subscribeToNetInfo(eventHandler) {
  NetInfo.addEventListener(eventHandler);
}
