import axios from 'axios';

const baseUrl =
  'https://us-central1-foozingaandroid.cloudfunctions.net/paymentApi/';
export function handleError(error) {
  throw error;
}
export function handleSuccess(response) {
  return response.data;
}
export function get(uri, access_token) {
  return axios
    .get(baseUrl + uri, {
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
    })
    .then(handleSuccess)
    .catch(handleError);
}
export function post(uri, data, access_token) {
  console.log(baseUrl + uri, data);
  return axios
    .post(baseUrl + uri, data, {
      headers: {
        Authorization: `Bearer ${access_token}`,
      },
    })
    .then(handleSuccess)
    .catch(handleError);
}
