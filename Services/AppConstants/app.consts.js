export const REGION_CODE = '+91';
export const CODE_TIMEOUT_SECONDS = 60;
export const IS_NUM_REGX = /^\d+$/;
export const QUERY_PARAMETER_MOBILE = 'mobileNumber';
export const QUERY_PARAMETER_USERID = 'userId';
export const QUERY_OPERATION_EQUAL = '==';
export const DOC_USERS = 'users';
export const NEW_USER_LIST = 'NewSubscriptions';
export const DOC_NEW_USER = 'DKqoeOLJUtUD6sHgrYJX';
export const DOC_REGISTERED_USER = 'RegisteredUserCollection';

export const DOC_SUBSCRIPTIONS = 'subscriptions';
export const APPLICATION_CONSTANTS = 'applicationConstants';
export const PRICE_DETAILS = '1L45bfcRO9KQtm8qLjY3';
// export const ETHNICITY_DIET = 'T2enihOdkiW8NQ3MvVkx';
export const ETHNICITY_DIET = 'Zr752bt3qfkN8JXM7m22';
export const COOK_INFO = 'iEufN44FrVCO8LERdFlT';
export const SERVICE_AREA = 'MdRKLr2s5Kkw5yq18hFt';
export const COUPON_CODES = 'CouponCodes';
export const FAQ_QNA = 'FAQs';

export const ADDRESS_HOME_SCREEN = 'HOME';
export const ADDRESS_OFFICE_SCREEN = 'OFFICE';
export const SERVICE_AREA_COLL = 'ServiceArea';
export const MEALTYPE_DINNER = 'DINNER';
export const MEALTYPE_LUNCH = 'LUNCH';

export const SUBSCRIPTION_PAYMENTINITIATED_STATUS = 'PAYMENTINITIATED';
export const SUBSCRIPTION_ACTIVE_STATUS = 'ACTIVE';
export const SUBSCRIPTION_PAYMENTFAILED_STATUS = 'PAYMENTFAILED';
export const SUBSCRIPTION_EXPIRED_STATUS = 'EXPIRED';

export const PAYMENT_PAYMENTINITIATED_STATUS = 'PAYMENTINITIATED';
export const PAYMENT_PROCESSING_STATUS = 'PROCESSING';
export const PAYMENT_FAILED_STATUS = 'PAYMENTFAILED';
export const PAYMENT_SUCCESS_STATUS = 'SUCCESS';
