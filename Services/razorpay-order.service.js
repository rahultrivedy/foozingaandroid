import { post } from './api.service';

export function posCreateOrder(amount, userId, subscriptionId, access_token) {
  console.log('POST CREATE ORDER');
  return post(
    'api/createorder',
    {
      amount: amount,
      userId: userId,
      subscriptionId: subscriptionId,
    },
    access_token,
  );
}

export function postVerifyPayment(order, userId, subscriptionId, access_token) {
  return post(
    'api/confirmPayment',
    { order: order, userId: userId, subscriptionId: subscriptionId },
    access_token,
  );
}
