import firestore from '@react-native-firebase/firestore';
import * as Constants from '../AppConstants/app.consts';

export class ApplicationConstants {
  #firestoreRef;
  constructor() {
    this.#firestoreRef = firestore().collection(
      Constants.APPLICATION_CONSTANTS.toString(),
    );
  }

  getCookDetails(cookId) {
    return this.#firestoreRef
      .doc(Constants.COOK_INFO)
      .collection(cookId)
      .get();
  }

  getServiceArea() {
    return this.#firestoreRef
      .doc(Constants.SERVICE_AREA)
      .collection(Constants.SERVICE_AREA_COLL)
      .get();
  }

  getCouponCodes() {
    return this.#firestoreRef.doc(Constants.COUPON_CODES).get();
  }

  getFAQs() {
    return this.#firestoreRef.doc(Constants.FAQ_QNA).get();
  }

  getApplicationConstants() {
    //var data = { Pricing: {}, DietEthnicity: {} };
    return this.#firestoreRef.get();
  }
}
