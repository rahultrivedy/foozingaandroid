import firestore from '@react-native-firebase/firestore';
import * as Constants from '../AppConstants/app.consts';
import moment from 'moment';

export class NewRegistrationService {
  #firestoreRef;
  constructor() {
    this.#firestoreRef = firestore().collection(
      Constants.DOC_REGISTERED_USER.toString(),
    );
  }

  addRegisteredUserToList(userId, name, phone, onSuccess, onError) {
    let todaysDate = moment();
    const data = {
      phone: phone,
      name: name,
      userSince: todaysDate.format('DD/MM/YYYY'),
      userSinceTimestamp: todaysDate._d,
    };
    this.#firestoreRef
      .doc(userId)
      .set(data)
      .then(result => {
        console.log(result);
        onSuccess(result);
      })
      .catch(error => {
        onError(error);
      });
  }
  deleteRegisteredUserFromList(userId) {
    this.#firestoreRef
      .doc(userId)
      .delete()
      .then(() => console.log('User Deleted from New User Doc'))
      .catch(error => console.log(error));
  }
}
