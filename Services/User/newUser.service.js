import firestore from '@react-native-firebase/firestore';
import * as Constants from '../AppConstants/app.consts';

export class NewUserService {
  #firestoreRef;
  constructor() {
    this.#firestoreRef = firestore().collection(
      Constants.NEW_USER_LIST.toString(),
    );
  }

  addNewUserToList(userId, name, startDate, onSuccess, onError) {
    const data = {
      name: name,
      startDate: startDate,
    };
    this.#firestoreRef
      .doc(userId)
      .set(data)
      .then(result => {
        console.log(result);
        onSuccess(result);
      })
      .catch(error => {
        onError(error);
      });
  }
  deleteNewUserFromList(userId) {
    this.#firestoreRef
      .doc(userId)
      .delete()
      .then(() => console.log('User Deleted from New User Doc'))
      .catch(error => console.log(error));
  }
}
