import firestore from '@react-native-firebase/firestore';
import * as Constants from '../AppConstants/app.consts';

export class FeedbackService {
  #firestoreRef;
  constructor() {
    this.#firestoreRef = firestore().collection('feedbacks&Suggestions');
  }

  addFeedback(userId, data, onSuccess, onError) {
    this.#firestoreRef
      .doc('OtqDjKKqB9HnIerVrzm0')
      .collection('feedbacks')
      .add(data)
      .then(result => {
        onSuccess(result);
      })
      .catch(error => {
        onError(error);
      });
  }

  addSuggestion(userId, data, onSuccess, onError) {
    this.#firestoreRef
      .doc('OtqDjKKqB9HnIerVrzm0')
      .collection('suggestions')
      .add(data)
      .then(result => {
        onSuccess(result);
      })
      .catch(error => {
        onError(error);
      });
  }
}
