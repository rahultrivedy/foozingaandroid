import firestore from '@react-native-firebase/firestore';
import * as Constants from '../AppConstants/app.consts';
import moment from 'moment';

export class UserService {
  #firestoreRef;
  constructor() {
    this.#firestoreRef = firestore().collection(Constants.DOC_USERS.toString());
  }

  readUserProfileDetails() {
    return this.#firestoreRef.onSnapshot(this.#getCollection);
  }

  #getCollection = querySnapshot => {
    const userArr = [];
    querySnapshot.forEach(res => {
      const { name, email, mobile } = res.data();
      userArr.push({
        key: res.id,
        res,
        name,
        email,
        mobile,
      });
    });
    return userArr;
  };

  getUserDetails(userId) {
    return this.#firestoreRef.doc(userId).get();
  }

  getCookInfo(userId) {
    return this.#firestoreRef.doc(userId).get();
  }

  getSubscriptionDetails(userId, activeSubscriptionID) {
    return this.#firestoreRef
      .doc(userId)
      .collection(Constants.DOC_SUBSCRIPTIONS.toString())
      .doc(activeSubscriptionID)
      .get();
  }

  createUser(user, onSuccess, onError) {
    let todaysDate = moment();
    // console.log(todaysDate._d);
    const data = {
      name: user.name,
      email: user.email,
      mobileNumber: user.phoneNumber,
      address: {
        home: {},
        office: {},
      },
      cook: '',
      endDate: '',
      startDate: '',
      userSince: todaysDate.format('DD/MM/YYYY'),
      userSinceTimestamp: todaysDate._d,
    };
    this.#firestoreRef
      .doc(user.customerId)
      .set(data)
      .then(result => {
        onSuccess(result);
      })
      .catch(error => {
        onError(error);
      });
  }

  createHomeAddress(userId, data, onSuccess, onError) {
    this.#firestoreRef
      .doc(userId)
      .update({
        'address.home': data,
      })
      .then(result => {
        onSuccess(result);
        console.log('SUCCESS FROM USER SERVICE');
      })
      .catch(error => {
        onError(error);
      });
  }

  createOfficeAddress(userId, data, onSuccess, onError) {
    this.#firestoreRef
      .doc(userId)
      .update({
        'address.office': data,
      })
      .then(result => {
        onSuccess(result);
      })
      .catch(error => {
        onError(error);
      });
  }

  createSubscription(userId, data, onSuccess, onError) {
    this.#firestoreRef
      .doc(userId)
      .collection('subscriptions')
      .add(data)
      .then(result => {
        onSuccess(result);
      })
      .catch(error => {
        onError(error);
      });
  }

  updateActiveSubscription(userId, subscriptionId, onSuccess, onError) {
    this.#firestoreRef
      .doc(userId)
      .update({
        activeSubscriptionID: subscriptionId,
      })
      .then(result => {
        onSuccess(result);
      })
      .catch(error => {
        onError(error);
      });
  }

  updateFutureSubscription(userId, subscriptionId, onSuccess, onError) {
    this.#firestoreRef
      .doc(userId)
      .update({
        futureSubscriptionID: subscriptionId,
      })
      .then(result => {
        onSuccess(result);
      })
      .catch(error => {
        onError(error);
      });
  }

  updateFutureToActiveSubscription(
    userId,
    futureSubscriptionId,
    endDate,
    startDate,
    onSuccess,
    onError,
  ) {
    let startDateTimestamp = moment(startDate, 'DD/MM/YYYY');
    let endDateTimestamp = moment(endDate, 'DD/MM/YYYY');
    this.#firestoreRef
      .doc(userId)
      .update({
        activeSubscriptionID: futureSubscriptionId,
        futureSubscriptionID: '',
        endDate: endDate,
        endDateTimestamp: endDateTimestamp._d,
        startDate: startDate,
        startDateTimestamp: startDateTimestamp._d,
      })
      .then(result => {
        onSuccess(result);
      })
      .catch(error => {
        onError(error);
      });
  }

  updateEndDate(userId, endDate, onSuccess, onError) {
    let endDateTimestamp = moment(endDate, 'DD/MM/YYYY');
    this.#firestoreRef
      .doc(userId)
      .update({
        endDate: endDate,
        endDateTimeStamp: endDateTimestamp._d,
      })
      .then(result => {
        onSuccess(result);
      })
      .catch(error => {
        onError(error);
      });
  }

  updateSubStartDate(userId, startDate, onSuccess, onError) {
    let startDateTimestamp = moment(startDate, 'DD/MM/YYYY');
    this.#firestoreRef
      .doc(userId)
      .update({
        startDate: startDate,
        startDateTimeStamp: startDateTimestamp._d,
      })
      .then(result => {
        onSuccess(result);
      })
      .catch(error => {
        onError(error);
      });
  }

  updateUserName(userId, userName, onSuccess, onError) {
    this.#firestoreRef
      .doc(userId)
      .update({
        name: userName,
      })
      .then(result => {
        console.log(result);
        onSuccess(result);
      })
      .catch(error => {
        onError(error);
      });
  }

  updateCookInfo(userId, cookId, onSuccess, onError) {
    this.#firestoreRef
      .doc(userId)
      .update({
        cook: '',
      })
      .then(result => {
        onSuccess(result);
      })
      .catch(error => {
        onError(error);
      });
  }

  async checkUserExists(phoneNumber, userId, onSuccess, onError) {
    var doc = await this.#firestoreRef.doc(phoneNumber);
    if (doc) {
      if (doc.id === phoneNumber) {
        this.#firestoreRef
          .doc(userId)
          .get()
          .then(documentSnapshot => {
            if (documentSnapshot.exists) {
              onSuccess(true);
            } else {
              onSuccess(false);
            }
          });
        return;
      }
    }
    onSuccess(false);
  }
  //listen to doc change
  userDocListener = (userId, callback) => {
    this.#firestoreRef.doc(userId).onSnapshot(documentSnapshot => {
      callback(documentSnapshot);
    });
  };
}
