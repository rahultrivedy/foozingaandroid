import { combineReducers } from 'redux';
import {
  NEW_SUBSCRIPTION_PLAN,
  NEW_SUBSCRIPTION_PLAN_MEAL_INFORMATION,
} from './action.service';
import moment from 'moment';
const initialState = {
  foodEthnicity: '',
  diet: '',
  subscriptionType: '',
  subscriptionFrequency: 0,
  mealInformation: [],
  payment: {},
  startDate: '',
  endDate: '',
  subscriptionDate: '',
  subscriptionStatus: '',
  quantity: 1,
};
const mealInformation = {
  mealType: '',
  addresstype: '',
};
const payment = {
  amount: '',
  totalAmount: '',
  discount: '',
  gstValue: '',
  paymentDate: '',
  paymentStatus: '',
};
function subscription(state = {}, action) {
  switch (action.type) {
    case NEW_SUBSCRIPTION_PLAN:
      return {
        foodEthnicity: action.foodEthnicity,
        diet: action.diet,
        subscriptionType: action.subscriptionType,
        subscriptionFrequency: action.subscriptionFrequency,
        subscriptionDate: moment(new Date()).format('MM/DD/YYYY [\n]hh:mm a'),
        startDate: action.startDate,
        endDate: action.endDate,
      };
    case NEW_SUBSCRIPTION_PLAN_MEAL_INFORMATION:
      return {
        ...state,
        mealInformation: action.mealInformation,
      };
    default:
      return state;
  }
}

const rootReducer = combineReducers({ subscription });

export default rootReducer;
