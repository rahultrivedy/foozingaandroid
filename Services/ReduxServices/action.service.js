import moment from 'moment';
/*
 * action types
 */
export const NEW_SUBSCRIPTION_PLAN = 'NEW_SUBSCRIPTION_PLAN';
export const NEW_SUBSCRIPTION_PLAN_MEAL_INFORMATION =
  'NEW_SUBSCRIPTION_PLAN_MEAL_INFORMATION';

/*
 * action creators
 */
export function newSubscription(
  foodEthnicity,
  diet,
  subscriptionType,
  subscriptionFrequency,
) {
  var startDate = moment(new Date())
    .add(1, 'days')
    .format('MM/DD/YYYY [\n]hh:mm a');
  var endDate = moment(new Date())
    .add(28, 'days')
    .format('MM/DD/YYYY [\n]hh:mm a');
  return {
    type: NEW_SUBSCRIPTION_PLAN,
    foodEthnicity,
    diet,
    subscriptionType,
    subscriptionFrequency,
    startDate,
    endDate,
  };
}

export function newSubscriptionMealInformation(mealInformation) {
  return { type: NEW_SUBSCRIPTION_PLAN_MEAL_INFORMATION, mealInformation };
}
