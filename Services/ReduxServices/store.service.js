import { createStore } from 'redux';
import rootReducer from './reducers.service';
var store = {};

export function configureStore() {
  store = createStore(rootReducer);
}

export { store };
