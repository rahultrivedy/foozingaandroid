import auth from '@react-native-firebase/auth';
import * as Constants from '../AppConstants/app.consts';

export class AuthService {
  #phoneSnapshot;
  constructor() {}

  verifyCode = (code, onSuccess, onError) => {
    if (this.#phoneSnapshot) {
      switch (this.#phoneSnapshot.state) {
        case auth.PhoneAuthState.CODE_SENT:
        case auth.PhoneAuthState.AUTO_VERIFY_TIMEOUT:
        case auth.PhoneAuthState.AUTO_VERIFIED:
          var credential = auth.PhoneAuthProvider.credential(
            this.#phoneSnapshot.verificationId,
            code,
          );
          console.log(credential);
          auth()
            .signInWithCredential(credential)
            .then(() => {
              onSuccess();
            })
            .catch(error => {
              onError(error.message);
            });
          break;
      }
    }
  };

  verifyPhoneNumber(phoneNumber, onSuccess, onError) {
    auth()
      .verifyPhoneNumber(phoneNumber, Constants.CODE_TIMEOUT_SECONDS)
      .on('state_changed', async phoneAuthSnapshot => {
        console.log('SnapShot:');
        console.log(phoneAuthSnapshot);
        if (
          phoneAuthSnapshot.state === 'verified' &&
          !phoneAuthSnapshot.code &&
          !phoneAuthSnapshot.verificationId &&
          !phoneAuthSnapshot.error
        ) {
          console.log('AUTO VERIFIED NUMBER');
        }
        switch (phoneAuthSnapshot.state) {
          case auth.PhoneAuthState.CODE_SENT: // or 'sent'
            console.log('CODE_SENT');
            this.#phoneSnapshot = phoneAuthSnapshot;
            break;
          case auth.PhoneAuthState.ERROR: // or 'error'
            console.log('ERROR', phoneAuthSnapshot.error.message);
            onError(phoneAuthSnapshot.error.message);
            break;
          case auth.PhoneAuthState.AUTO_VERIFY_TIMEOUT: // or 'timeout'
            console.log('AUTO_VERIFY_TIMEOUT');
            this.#phoneSnapshot = phoneAuthSnapshot;
            break;
          case auth.PhoneAuthState.AUTO_VERIFIED: // or 'verified'
            console.log('AUTO_VERIFIED');
            this.#phoneSnapshot = phoneAuthSnapshot;
            if (
              phoneAuthSnapshot.state === 'verified' &&
              !phoneAuthSnapshot.code &&
              !phoneAuthSnapshot.error
            ) {
              onSuccess();
            }
            break;
        }
      });
  }

  static onAuthStateChanged(onAuthStateChangedCallback) {
    auth().onAuthStateChanged(onAuthStateChangedCallback);
  }

  static getCurrentUserUserId() {
    var user = auth().currentUser;
    var uid;
    if (user != null) {
      uid = user.uid;
    }
    return uid;
  }

  getAuthToken(callback) {
    auth()
      .currentUser.getIdToken()
      .then(result => {
        console.log('RESULT GET AUTH');
        console.log(result);
        return callback(result, null);
      })
      .catch(error => {
        console.log(error);
        callback(null, error);
      });
  }
}
