import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  profileAuthWrapper: {
    padding: 10,
    backgroundColor: '#fff',
  },
  headlineText: {
    fontFamily: 'Poppins-Medium',
    color: '#2a3452',
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  nameInputLabelStyle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 14,
  },
  nameInputStyle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 14,
    height: 20,
    padding: 0,
  },
  // continueButton: {
  //   backgroundColor: '#fc2250',
  //   marginBottom: 20,
  //   alignSelf: 'center',
  //   marginTop: 20,
  // },
  // continueButtonText: {
  //   fontFamily: 'PoppinsMedium-Regular',
  //   fontSize: 16,
  //   paddingLeft: 50,
  //   paddingRight: 50,
  //   color: '#fff',
  // },
  continueButton: {
    backgroundColor: '#fc2250',
    marginBottom: 40,
    alignSelf: 'center',
    marginTop: 30,
    width: 170,
    justifyContent: 'center',
  },
  continueButtonLoading: {
    marginBottom: 40,
    alignSelf: 'center',
    marginTop: 30,
    width: 170,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#fc2250',
  },
  continueButtonText: {
    textAlign: 'center',
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#fff',
  },
  fullNameErrorMessageStyle: {
    color: '#fc2250',
    fontSize: 16,
    // fontStyle: 'italic',
    fontFamily: 'PoppinsMedium-Regular',
  },
});
