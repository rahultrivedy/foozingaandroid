import { Component } from 'react';
import { render } from '../AuthProfileScreen/view';
import { AuthService } from '../../../../Services/Authentication/auth.service';
import { User } from '../../../../models/user.model';
import { UserService } from '../../../../Services/User/user.service';
import { NewRegistrationService } from '../../../../Services/User/newRegistration.service';

export default class AuthProfileScreen extends Component {
  #userService;
  #newRegistrationService;
  constructor() {
    super();
    this.state = {
      fullName: '',
      email: '',
      fullNameErrorMessage: '',
      loading: false,
    };
    this.#userService = new UserService();
    this.#newRegistrationService = new NewRegistrationService();
  }

  componentDidMount() {}

  onTextChangeName = fullName => {
    this.setState({ fullName: fullName });
  };

  onTextChangeEmail = email => {
    this.setState({ email: email });
  };

  onSubmitButtonPress() {
    this.setState({
      loading: true,
    });
    var isText = /^[a-zA-Z\s]*$/;
    var isEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!isText.test(this.state.fullName) || this.state.fullName.length < 2) {
      this.setState({
        fullNameErrorMessage: 'Please check the entered name',
        loading: false,
      });
    } else if (!isEmail.test(this.state.email) || this.state.email.length < 2) {
      this.setState({
        fullNameErrorMessage: 'Please check the entered Email',
        loading: false,
      });
    } else {
      const { phoneNumber } = this.props.route.params;
      var userid = AuthService.getCurrentUserUserId();
      var userName = this.state.fullName.trim();
      var userEmail = this.state.email.trim().toLowerCase();
      var user = new User(userEmail, userName, phoneNumber, userid);
      this.#newRegistrationService.addRegisteredUserToList(
        userid,
        userName,
        phoneNumber,
        resultNewUser => console.log(resultNewUser),
      );
      this.#userService.createUser(user, this.onSuccess, this.onError);
    }
  }

  onSuccess = () => {
    this.props.route.params.RBSheet.close(true);
    // this.setState({
    //   loading: false,
    // });
    //redirect to logged in page
  };

  onError(errorMessage) {
    this.setState({
      loading: false,
    });
    console.error(errorMessage);
  }

  render() {
    return render(this);
  }
}
