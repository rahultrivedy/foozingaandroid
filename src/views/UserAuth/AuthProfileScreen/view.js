import React from 'react';
import { Text, View, ActivityIndicator } from 'react-native';
import {
  Content,
  Button,
  Title,
  Subtitle,
  Item,
  Input,
  Label,
} from 'native-base';
import { styles } from '../AuthProfileScreen/style';

export function render(viewState) {
  return (
    <Content
      contentContainerStyle={styles.profileAuthWrapper}
      showsVerticalScrollIndicator={false}
      keyboardShouldPersistTaps={'handled'}>
      <View>
        <Title maxFontSizeMultiplier={1} style={styles.headlineText}>
          Profile Details
        </Title>
        <Text maxFontSizeMultiplier={1} style={styles.subHeadingText}>
          Please enter the details to complete your profile
        </Text>
      </View>
      {viewState.state.fullNameErrorMessage.length > 1 ? (
        <View>
          <Text
            maxFontSizeMultiplier={1}
            style={styles.fullNameErrorMessageStyle}>
            {viewState.state.fullNameErrorMessage}
          </Text>
        </View>
      ) : null}
      <View style={{ marginTop: 20 }}>
        <Item stackedLabel>
          <Label maxFontSizeMultiplier={1} style={styles.nameInputLabelStyle}>
            Full Name
          </Label>
          <Input
            maxFontSizeMultiplier={1}
            autoFocus={true}
            autoCapitalize="words"
            autoCorrect={false}
            style={styles.nameInputStyle}
            onChangeText={text => viewState.onTextChangeName(text)}
          />
        </Item>
        <Item stackedLabel last style={{ marginTop: 20 }}>
          <Label maxFontSizeMultiplier={1} style={styles.nameInputLabelStyle}>
            Email Address
          </Label>
          <Input
            autoCapitalize="none"
            maxFontSizeMultiplier={1}
            autoCorrect={false}
            keyboardType={'email-address'}
            style={styles.nameInputStyle}
            onChangeText={text => viewState.onTextChangeEmail(text)}
          />
        </Item>
      </View>
      <View style={{ marginTop: 20 }}>
        {viewState.state.loading ? (
          <Button
            disabled
            rounded
            bordered
            style={styles.continueButtonLoading}>
            <ActivityIndicator small color="#fc2250" />
          </Button>
        ) : (
          <Button
            rounded
            style={styles.continueButton}
            onPress={() => viewState.onSubmitButtonPress()}>
            <Text maxFontSizeMultiplier={1} style={styles.continueButtonText}>
              Submit
            </Text>
          </Button>
        )}
      </View>
    </Content>
  );
}
