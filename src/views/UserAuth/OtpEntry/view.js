import React from 'react';
import { Text, View, ActivityIndicator, Animated } from 'react-native';
import { Button, Title, Subtitle } from 'native-base';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import { styles } from '../OtpEntry/style';
import { CountdownCircleTimer } from 'react-native-countdown-circle-timer';
import LottieView from 'lottie-react-native';

export function render(viewState) {
  // console.log(otpEntryMessage(viewState));
  return (
    <View style={styles.phoneAuthWrapper}>
      <View>
        <Title maxFontSizeMultiplier={1} style={styles.headlineText}>
          Enter OTP
        </Title>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <Text maxFontSizeMultiplier={1} style={styles.subHeadingText}>
            OTP sent to{' '}
            <Subtitle style={styles.subHeadingPhoneNumber}>
              {viewState.props.route.params.phoneNumber}
            </Subtitle>
          </Text>
          <Button
            style={styles.changeButtonStyle}
            onPress={() => viewState.onChangePhonePress()}>
            <Text maxFontSizeMultiplier={1} style={styles.addressButtonText}>
              Change
            </Text>
          </Button>
        </View>
      </View>
      {viewState.state.otpEntryMessage.length > 1 ? (
        <View>{otpEntryMessage(viewState)}</View>
      ) : null}
      {viewState.state.autoVerifyMessage ? (
        <View
          style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <LottieView
            style={{ width: 50, height: 50, flex: 1 }}
            source={require('../../../assets/LottieAnimation/google-processing.json')}
            autoPlay
            loop={false}
          />
          <Text
            maxFontSizeMultiplier={1}
            style={{
              fontFamily: 'Poppins-Regular',
              fontSize: 18,
              color: '#000',
              textAlign: 'center',
            }}>
            {viewState.state.autoVerifyMessage}
          </Text>
        </View>
      ) : (
        <View>
          <View style={{ flex: 1 }}>
            <OTPInputView
              maxFontSizeMultiplier={1}
              autoFocus={true}
              keyboardType={'phone-pad'}
              style={{ width: '100%', height: 200 }}
              pinCount={6}
              code={viewState.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
              onCodeChanged={code => {
                viewState.setState({ code });
              }}
              autoFocusOnLoad={true}
              codeInputFieldStyle={styles.underlineStyleBase}
              codeInputHighlightStyle={styles.underlineStyleHighLighted}
              onCodeFilled={code => {
                // console.log(`Code is ${code}`);
                viewState.setState({
                  codeSuccess: true,
                });
              }}
            />
          </View>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 140,
            }}>
            {viewState.state.resendOtpShow ? (
              <Button
                transparent
                onPress={() => viewState.resendOtpButtonPress()}>
                <Text
                  maxFontSizeMultiplier={1}
                  style={styles.addressButtonText}>
                  Resend OTP
                </Text>
              </Button>
            ) : (
              <CountdownCircleTimer
                isPlaying
                duration={60}
                size={40}
                strokeWidth={2}
                // onComplete={() => viewState.onTimerComplete()}
                onComplete={() => {
                  viewState.setState({
                    resendOtpShow: true,
                  });
                }}
                strokeLinecap={'square'}
                colors={[['#fc2250', 0.4], ['#632dc2', 0.2]]}>
                {({ remainingTime, animatedColor }) => (
                  <Animated.Text
                    maxFontSizeMultiplier={1}
                    style={{ color: animatedColor }}>
                    {remainingTime}
                  </Animated.Text>
                )}
              </CountdownCircleTimer>
            )}
          </View>
          <View>
            {viewState.state.loading ? (
              <Button
                disabled
                rounded
                bordered
                style={styles.continueButtonLoading}>
                <ActivityIndicator small color="#fc2250" />
              </Button>
            ) : (
              <Button
                rounded
                style={styles.continueButton}
                onPress={() => viewState.onSubmitButtonPress()}>
                <Text
                  maxFontSizeMultiplier={1}
                  style={styles.continueButtonText}>
                  Submit
                </Text>
              </Button>
            )}
          </View>
        </View>
      )}
    </View>
  );
}

export function otpEntryMessage(viewState) {
  return (
    <Text maxFontSizeMultiplier={1} style={styles.otpEntryMessageStyle}>
      {viewState.state.otpEntryMessage}
    </Text>
  );
}

export function otpVerificationErrorMessage(viewState) {
  return (
    <Text maxFontSizeMultiplier={1} style={styles.otpEntryMessageStyle}>
      {viewState.state.otpVerificationErrorMessage}
    </Text>
  );
}
