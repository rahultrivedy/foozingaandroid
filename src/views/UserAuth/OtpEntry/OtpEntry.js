import { Component } from 'react';
import { render } from '../OtpEntry/view';
import { AuthService } from '../../../../Services/Authentication/auth.service';
import { UserService } from '../../../../Services/User/user.service';
import * as Constants from '../../../../Services/AppConstants/app.consts';

export default class OtpEntry extends Component {
  #authService;
  #userService;
  #phoneNumber;
  constructor(props) {
    super();
    this.state = {
      code: '',
      otpEntryMessage: '',
      phoneNumber: '',
      isSubmitEnable: false,
      loading: false,
      resendOtpShow: false,
    };
    this.#authService = new AuthService();
    this.#userService = new UserService();
  }

  componentDidMount() {
    const { phoneNumber } = this.props.route.params;
    // console.log('PhoneNumber' + phoneNumber);
    this.#phoneNumber = phoneNumber;
    // console.log(this.#phoneNumber);
    this.#authService.verifyPhoneNumber(
      Constants.REGION_CODE + this.#phoneNumber,
      this.onAutoVerify,
      this.onErrorVerifyCode,
    );
  }

  onChangePhonePress() {
    this.props.navigation.navigate('PhoneNumber');
  }

  onSubmitButtonPress() {
    this.setState({
      loading: true,
    });
    var isNum = Constants.IS_NUM_REGX;
    if (this.state.code.length !== 6) {
      this.setState({
        otpEntryMessage: 'Fill in the sent code to proceed',
        loading: false,
      });
    } else if (!isNum.test(this.state.code)) {
      this.setState({
        otpEntryMessage: 'OTP can only be numbers',
        loading: false,
      });
    } else {
      this.#authService.verifyCode(
        this.state.code,
        this.onSuccess.bind(this),
        this.onErrorVerifyCode,
      );
    }
  }
  onSuccess() {
    var userId = AuthService.getCurrentUserUserId();
    // console.log('USERID FROM OTP: ' + userId);
    this.#userService.checkUserExists(
      this.#phoneNumber,
      userId,
      success => {
        if (success) {
          // console.log('User exists');
          this.props.route.params.RBSheet.close(success);
          return;
        } else {
          // console.log('User Does not Exist');
          this.setState({
            loading: false,
          });
          this.props.navigation.navigate('AuthProfileScreen', {
            phoneNumber: this.#phoneNumber,
          });
        }
      },
      this.onErrorUserExists,
    );
  }
  onErrorUserExists(errorMessage) {
    console.error(errorMessage);
  }
  onAutoVerify = () => {
    this.setState({
      autoVerifyMessage: 'Verified by Google\nLogging in ...',
    });
    setTimeout(
      () =>
        this.#authService.verifyCode(
          this.state.code,
          this.onSuccess.bind(this),
          this.onErrorVerifyCode,
        ),
      2000,
    );
  };
  onErrorVerifyCode = errorMessage => {
    this.setState({
      otpEntryMessage: 'OTP is incorrect',
      loading: false,
    });
  };

  resendOtpButtonPress = () => {
    // console.log(this.#phoneNumber);
    this.#authService.verifyPhoneNumber(
      Constants.REGION_CODE + this.#phoneNumber,
      this.onErrorVerifyCode,
    );
    this.setState({
      resendOtpShow: false,
    });
  };

  render() {
    return render(this);
  }
}
