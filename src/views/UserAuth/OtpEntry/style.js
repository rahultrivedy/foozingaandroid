import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  phoneAuthWrapper: {
    flex: 1,
    padding: 10,
    backgroundColor: '#fff',
  },
  headlineText: {
    fontFamily: 'Poppins-Medium',
    color: '#2a3452',
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  subHeadingPhoneNumber: {
    fontFamily: 'Poppins-Medium',
    color: '#656c82',
  },
  phoneInputStyle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 40,
    textAlign: 'center',
  },
  continueButton: {
    backgroundColor: '#fc2250',
    marginBottom: 40,
    alignSelf: 'center',
    marginTop: 30,
    width: 170,
    justifyContent: 'center',
  },
  continueButtonLoading: {
    marginBottom: 40,
    alignSelf: 'center',
    marginTop: 30,
    width: 170,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#fc2250',
  },
  continueButtonText: {
    textAlign: 'center',
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#fff',
  },
  gridPad: { padding: 30 },
  txtMargin: { margin: 3 },
  inputRadius: { borderColor: '#000', borderWidth: 1, textAlign: 'center' },

  borderStyleBase: {
    width: 30,
    height: 45,
  },
  borderStyleHighLighted: {
    borderColor: '#632dc2',
  },

  underlineStyleBase: {
    width: 40,
    height: 55,
    borderWidth: 2,
    color: '#2a3452',
    fontFamily: 'Poppins-Regular',
    fontSize: 18,
  },

  underlineStyleHighLighted: {
    borderColor: '#632dc2',
  },

  otpEntryMessageStyle: {
    color: '#fc2250',
    fontSize: 16,
    // fontStyle: 'italic',
    fontFamily: 'PoppinsMedium-Regular',
  },
  changeButtonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5f5f5',
    borderRadius: 10,
    borderColor: '#eeeeee',
    borderWidth: 1,
    width: 80,
    height: 30,
  },
  addressButtonText: {
    fontFamily: 'Poppins-Medium',
    fontSize: 12,
    color: '#fc2250',
    textAlign: 'center',
  },
});
