import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  phoneAuthWrapper: {
    flex: 1,
    padding: 10,
    backgroundColor: '#fff',
  },
  headlineText: {
    fontFamily: 'Poppins-Medium',
    color: '#2a3452',
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  phoneInputStyle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 18,
  },
  continueButton: {
    backgroundColor: '#fc2250',
    marginBottom: 20,
    alignSelf: 'center',
    marginTop: 80,
    width: 170,
    justifyContent: 'center',
  },
  continueButtonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    paddingLeft: 50,
    paddingRight: 50,
    color: '#fff',
  },
});
