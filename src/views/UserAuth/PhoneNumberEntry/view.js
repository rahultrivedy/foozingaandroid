/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { Text, View } from 'react-native';
import { Button, Title, Subtitle, Item, Input, Label } from 'native-base';
import { styles } from '../PhoneNumberEntry/style';

export function render(viewState) {
  return (
    <View style={styles.phoneAuthWrapper}>
      <View>
        <Title maxFontSizeMultiplier={1} style={styles.headlineText}>
          Mobile Number Verification
        </Title>
        <Subtitle maxFontSizeMultiplier={1} style={styles.subHeadingText}>
          Please enter your mobile number to proceed
        </Subtitle>
      </View>
      <View style={{ marginTop: 50 }}>
        <Item inlineLabel>
          <Label maxFontSizeMultiplier={1} style={styles.phoneInputStyle}>
            +91 -{' '}
          </Label>
          <Input
            maxFontSizeMultiplier={1}
            placeholder="x  x  x  x  x  x  x  x  x  x"
            maxLength={10}
            autoFocus={true}
            autoCompleteType={'tel'}
            dataDetectorTypes={'phoneNumber'}
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType={'phone-pad'}
            style={styles.phoneInputStyle}
            onChangeText={text => viewState.onTextChange(text)}
          />
        </Item>
      </View>
      {viewState.state.isSubmitEnable ? (
        <View style={{ marginTop: 20 }}>
          <Button
            rounded
            style={styles.continueButton}
            onPress={() => viewState.onSubmit()}>
            <Text maxFontSizeMultiplier={1} style={styles.continueButtonText}>
              Submit
            </Text>
          </Button>
        </View>
      ) : null}
    </View>
  );
}
