import { Component } from 'react';
import { render } from '../PhoneNumberEntry/view';
import { AuthService } from '../../../../Services/Authentication/auth.service';
import { UserService } from '../../../../Services/User/user.service';
import * as Constants from '../../../../Services/AppConstants/app.consts';

export default class LoginPhoneNumber extends Component {
  #authService;
  #userService;
  #phoneNumber;
  constructor() {
    super();
    this.state = { mobileNumber: undefined, isSubmitEnable: false };
    this.#authService = new AuthService();
    this.#userService = new UserService();
  }

  onTextChange(phoneNumber) {
    // console.log(phoneNumber);
    let number = phoneNumber.trim();
    var phoneRegex = /^\d{10}$/;
    if (phoneRegex.test(number) === true) {
      this.setState({ mobileNumber: phoneNumber, isSubmitEnable: true });
    } else {
      this.setState({ mobileNumber: undefined, isSubmitEnable: false });
    }
  }

  onSubmit() {
    // Try VERIFY PHONE HERE
    // this.#phoneNumber = this.state.phoneNumber;
    // console.log(this.#phoneNumber);
    // this.#authService.verifyPhoneNumber(
    //   Constants.REGION_CODE + this.#phoneNumber,
    //   this.onErrorVerifyCode,
    // );

    this.props.navigation.navigate('OtpEntry', {
      phoneNumber: this.state.mobileNumber,
    });
  }

  render() {
    return render(this);
  }
}
