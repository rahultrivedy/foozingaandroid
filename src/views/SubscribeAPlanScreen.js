import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  Switch,
  Dimensions,
} from 'react-native';
import { CustomHeader } from '../components/CustomHeader';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  Container,
  Content,
  Text,
  Button,
  Accordion,
  Radio,
  Tab,
  Tabs,
  Picker,
  Item,
  Input,
  Textarea,
} from 'native-base';
import Calendar from '../components/DatePicker.js';
import moment from 'moment';

// import RNPickerSelect from 'react-native-picker-select';
export default class SubscribeAPlan extends Component {
  VegPriceTwiceADay = [];
  VegPriceOnceADay = [];
  NonVegPriceTwiceADay = [];
  NonVegPriceOnceADay = [];
  constructor(props) {
    super(props);
    // console.log(props);
    let ethnicity = '';
    let selectedNumberOfTiffins = '';
    let selectedDiet = '';
    if (this.props.route.params) {
      if (this.props.route.params.ethnicity) {
        ethnicity = this.props.route.params.ethnicity;
      }
      if (this.props.route.params.selectedNumberOfTiffins) {
        selectedNumberOfTiffins = this.props.route.params
          .selectedNumberOfTiffins;
      }
      if (this.props.route.params.selectedDiet) {
        selectedDiet = this.props.route.params.selectedDiet;
      }
    }
    this.state = {
      selectedEthnicity: ethnicity ? ethnicity : '',
      selectedDiet: selectedDiet ? selectedDiet : '',
      selectedPlan: '',
      selectedPlanDetails: '',
      selectedTab: '',
      radio: [false, false],
      expandedEthnicity: ethnicity ? 1 : 0,
      expandedDiet: ethnicity ? 0 : 1,
      expandedPlan: 0,
      selectedNumberOfTiffins: selectedNumberOfTiffins
        ? selectedNumberOfTiffins
        : '1',
      startDate: '',
      errorSunday: false,
      switchValue: false,
      allergyText:
        'Allergies :  \n\nRoti / Rice Preference :  \n\nSpecial Requests : ',
      // minDate: 1,
    };
  }
  componentDidMount() {
    if (this.props.route.params.startDate) {
      var todaysDate = moment();
      var dateDiffFromToday = this.props.route.params.startDate.diff(
        todaysDate,
        'days',
      );
      var defaultStartDate = new Date(this.props.route.params.startDate);
      // console.log(defaultStartDate);
      // console.log(dateDiffFromToday);
      this.setState({
        startDate: defaultStartDate,
        minDate: dateDiffFromToday + 1,
      });
    } else {
      var defaultStartDate = new Date();
      defaultStartDate.setDate(defaultStartDate.getDate() + 1);
      // console.log(defaultStartDate);
      this.setState({
        startDate: defaultStartDate,
        minDate: 1,
      });
    }
    if (this.props.route.params.selectedDiet) {
      this._accordionDiet.setSelected(-1);
      if (this.props.route.params.selectedDiet === 'Veg') {
        this.setState({
          radio: [true, false],
          selectedPlan: '1',
          selectedPlanDetails: {
            title: 'Monthly',
            id: '1',
            days: 28,
            price: 4999,
          },
          selectedTab: 'twiceADay',
        });
      } else {
        this.setState({
          radio: [false, true],
          selectedPlan: '7',
          selectedPlanDetails: {
            title: 'Monthly',
            id: '7',
            days: 28,
            price: 9999,
          },
          selectedTab: 'twiceADay',
        });
      }
    }
    this.NonVegPriceTwiceADay = this.props.route.params.pricing.NonVegPriceTwiceADay;
    this.NonVegPriceOnceADay = this.props.route.params.pricing.NonVegPriceOnceADay;
    this.VegPriceTwiceADay = this.props.route.params.pricing.VegPriceTwiceADay;
    this.VegPriceOnceADay = this.props.route.params.pricing.VegPriceOnceADay;
  }
  _renderHeader1 = (item, expanded) => {
    return (
      <View style={styles.renderHeaderViewStyle}>
        <Image
          source={item.icon}
          resizeMode="contain"
          style={{ height: 30, width: 30 }}
        />
        {!this.state.selectedEthnicity ? (
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <Text
              maxFontSizeMultiplier={1}
              style={styles.renderHeaderTextStyle}>
              {' '}
              {item.title}{' '}
            </Text>
            {expanded ? (
              <MaterialCommunityIcons name="chevron-up" size={30} />
            ) : (
              <MaterialCommunityIcons name="chevron-down" size={30} />
            )}
          </View>
        ) : (
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <Text
              maxFontSizeMultiplier={1}
              style={styles.renderHeaderTextStyle}>
              {' '}
              Ethnicity : {this.state.selectedEthnicity}{' '}
            </Text>
            {expanded ? (
              <MaterialCommunityIcons name="chevron-up" size={30} />
            ) : (
              <MaterialCommunityIcons name="chevron-down" size={30} />
            )}
          </View>
        )}
      </View>
    );
  };
  _renderContent1 = items => {
    // console.log(items);
    return (
      <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
        {items.content.map((item, index) =>
          item.status === 'active' ? (
            <TouchableOpacity
              style={styles.renderContent1ViewStyle}
              onPress={() => this.selectedEthnicity(item)}>
              <Text
                maxFontSizeMultiplier={1}
                style={styles.renderContent1TextStyle}
                key={index}>
                {item.name}
              </Text>
            </TouchableOpacity>
          ) : (
            <View
              style={styles.renderContent1ViewStyle}
              onPress={() => this.selectedEthnicity(item)}>
              <Text
                maxFontSizeMultiplier={1}
                style={styles.renderContent1TextStyleDisabled}
                key={index}>
                {item.name}
              </Text>
            </View>
          ),
        )}
      </View>
    );
  };

  _renderHeader2 = (item, expanded) => {
    return (
      <View style={styles.renderHeaderViewStyle}>
        <Image
          source={item.icon}
          resizeMode="contain"
          style={{ height: 30, width: 30 }}
        />
        {!this.state.selectedDiet ? (
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <Text
              maxFontSizeMultiplier={1}
              style={styles.renderHeaderTextStyle}>
              {' '}
              {item.title}{' '}
            </Text>
            {expanded ? (
              <MaterialCommunityIcons name="chevron-up" size={30} />
            ) : (
              <MaterialCommunityIcons name="chevron-down" size={30} />
            )}
          </View>
        ) : (
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <Text
              maxFontSizeMultiplier={1}
              style={styles.renderHeaderTextStyle}>
              {' '}
              Diet : {this.state.selectedDiet}{' '}
            </Text>
            {expanded ? (
              <MaterialCommunityIcons name="chevron-up" size={30} />
            ) : (
              <MaterialCommunityIcons name="chevron-down" size={30} />
            )}
          </View>
        )}
      </View>
    );
  };
  _renderContent2 = items => {
    return (
      <View style={styles.renderContent2UpperViewStyle}>
        {items.content.map((item, index) => (
          <View>
            <TouchableOpacity
              style={styles.renderContent2ViewStyle}
              onPress={() => this.selectedDiet(item)}>
              <Radio
                color={'#b7bac4'}
                selectedColor={'#5cb85c'}
                selected={this.state.radio[index]}
                onPress={() => this.selectedDiet(item)}
                // key={index}
              />
              <Image
                source={items.image[index]}
                resizeMode="contain"
                style={{ height: 30, width: 30 }}
              />
              <Text
                maxFontSizeMultiplier={1}
                style={styles.renderContent2TextStyle}
                key={index}>
                {item}
              </Text>
            </TouchableOpacity>
          </View>
        ))}
      </View>
    );
  };

  _renderHeader3 = (item, expanded) => {
    return this.state.selectedEthnicity && this.state.selectedDiet ? (
      <View style={styles.renderHeaderViewStyle}>
        <Image
          source={item.icon}
          resizeMode="contain"
          style={{ height: 30, width: 30 }}
        />
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Text maxFontSizeMultiplier={1} style={styles.renderHeaderTextStyle}>
            {' '}
            {item.title}{' '}
          </Text>
          {expanded ? (
            <MaterialCommunityIcons name="chevron-up" size={30} />
          ) : (
            <MaterialCommunityIcons name="chevron-down" size={30} />
          )}
        </View>
      </View>
    ) : null;
  };

  _renderContent3 = items => {
    return this.state.selectedEthnicity && this.state.selectedDiet ? (
      <View>
        <Tabs
          tabBarUnderlineStyle={{ backgroundColor: '#2196f3' }}
          tabContainerStyle={{ elevation: 0 }}
          onChangeTab={({ i }) => this.tabChange(i)}>
          <Tab
            heading="Two times a day"
            tabStyle={{ backgroundColor: '#fff' }}
            activeTabStyle={{ elevation: 0, backgroundColor: '#fff' }}
            activeTextStyle={{ fontFamily: 'Poppins-Medium', color: '#2196f3' }}
            textStyle={{ fontFamily: 'Poppins-Regular', color: '#656c82' }}>
            <View style={styles.tab1ViewStyle}>
              <Text maxFontSizeMultiplier={1} style={styles.tab1SubtitleStyle}>
                You get lunch and dinner. You can choose your home or office
                address for the meals.
              </Text>
              <Text maxFontSizeMultiplier={1} style={styles.tab1SubtitleStyle}>
                ** We are off on Sundays
              </Text>
              <View>
                <FlatList
                  data={
                    this.state.selectedDiet === 'Veg'
                      ? this.VegPriceTwiceADay
                      : this.NonVegPriceTwiceADay
                  }
                  renderItem={({ item }) => (
                    <TouchableOpacity onPress={() => this.selectedPlan(item)}>
                      <View style={styles.listItemStyle}>
                        <View style={{ flexDirection: 'row' }}>
                          {this.state.selectedPlan === item.id ? (
                            <Radio
                              color={'#b7bac4'}
                              selectedColor={'#5cb85c'}
                              selected
                              onPress={() => this.selectedPlan(item)}
                            />
                          ) : (
                            <Radio
                              color={'#b7bac4'}
                              selectedColor={'#5cb85c'}
                              onPress={() => this.selectedPlan(item)}
                            />
                          )}
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.listItemTitle}>
                            {item.title}{' '}
                          </Text>
                          {/* <View>{this.renderIcon(item.status, item.state)}</View> */}
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.listItemPriceCancelled}>
                            {item.actualPrice}{' '}
                          </Text>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.listItemPrice}>
                            {'₹ '} {item.price}{' '}
                          </Text>
                        </View>
                        <Text
                          style={{
                            marginLeft: 16,
                            ...styles.tab1SubtitleStyle,
                          }}>
                          {item.days} days including Sundays.
                        </Text>
                      </View>
                    </TouchableOpacity>
                  )}
                  // keyExtractor={item => item.id}
                  listKey={(item, index) => 'D' + index.toString()}
                />
              </View>
            </View>
          </Tab>
          <Tab
            heading="One time a day"
            tabStyle={{ elevation: 0, backgroundColor: '#fff' }}
            activeTabStyle={{ backgroundColor: '#fff' }}
            activeTextStyle={{ fontFamily: 'Poppins-Medium', color: '#2196f3' }}
            textStyle={{ fontFamily: 'Poppins-Regular', color: '#656c82' }}>
            <View style={styles.tab1ViewStyle}>
              <Text maxFontSizeMultiplier={1} style={styles.tab1SubtitleStyle}>
                You get lunch or dinner. You can choose your home or office
                address for the meals.
              </Text>
              <Text maxFontSizeMultiplier={1} style={styles.tab1SubtitleStyle}>
                ** We are off on Sundays
              </Text>
              <View>
                <FlatList
                  data={
                    this.state.selectedDiet === 'Veg'
                      ? this.VegPriceOnceADay
                      : this.NonVegPriceOnceADay
                  }
                  renderItem={({ item }) => (
                    <TouchableOpacity onPress={() => this.selectedPlan(item)}>
                      <View style={styles.listItemStyle}>
                        <View style={{ flexDirection: 'row' }}>
                          {this.state.selectedPlan === item.id ? (
                            <Radio
                              color={'#b7bac4'}
                              selectedColor={'#5cb85c'}
                              selected
                              onPress={() => this.selectedPlan(item)}
                            />
                          ) : (
                            <Radio
                              color={'#b7bac4'}
                              selectedColor={'#5cb85c'}
                              onPress={() => this.selectedPlan(item)}
                            />
                          )}
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.listItemTitle}>
                            {item.title}{' '}
                          </Text>
                          {/* <View>{this.renderIcon(item.status, item.state)}</View> */}
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.listItemPriceCancelled}>
                            {item.actualPrice}{' '}
                          </Text>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.listItemPrice}>
                            {'₹'}
                            {item.price}{' '}
                          </Text>
                        </View>
                        <Text
                          style={{
                            marginLeft: 16,
                            ...styles.tab1SubtitleStyle,
                          }}>
                          {item.days} days including Sundays.
                        </Text>
                      </View>
                    </TouchableOpacity>
                  )}
                  // keyExtractor={item => item.id}
                  listKey={(item, index) => 'E' + index.toString()}
                />
              </View>
            </View>
          </Tab>
        </Tabs>
      </View>
    ) : null;
  };
  //OnPress Select Ethnicity - Change Ethnicity Header and collapse Accordion
  selectedEthnicity = item => {
    // console.log(item);
    this._accordionEthnicity.setSelected(-1);
    this._accordionDiet.setSelected(0);
    this.setState({
      selectedEthnicity: item.name,
    });
  };

  //OnPress Select Diet - Change Diet Header
  selectedDiet = item => {
    // console.log(this._accordionPlan.props.expanded);
    // if (this._accordionPlan.props.expanded === 0) {
    //   this.setState({
    //     selectedTab: 'twiceADay',
    //   });
    // } else {}
    this._accordionDiet.setSelected(-1);
    // this._accordionPlan.setSelected(-1);
    this.setState({
      selectedDiet: item,
    });
    if (item.trim() === 'Veg') {
      this.setState({
        radio: [true, false],
        selectedPlan: '1',
        selectedPlanDetails: {
          title: 'Monthly',
          id: '1',
          days: this.VegPriceTwiceADay[1].days,
          price: this.VegPriceTwiceADay[1].price,
        },
        selectedTab: 'twiceADay',
      });
    } else {
      this.setState({
        radio: [false, true],
        selectedPlan: '7',
        selectedPlanDetails: {
          title: 'Monthly',
          id: '7',
          days: this.NonVegPriceTwiceADay[1].days,
          price: this.NonVegPriceTwiceADay[1].price,
        },
        selectedTab: 'twiceADay',
      });
    }
  };

  //OnPress Select Plan - Change Plan
  selectedPlan = item => {
    this.setState({
      selectedPlan: item.id,
      selectedPlanDetails: item,
    });
  };

  onSwitchChange() {
    this.setState({
      switchValue: !this.state.switchValue,
    });
  }

  tabChange = item => {
    if (this.state.selectedDiet === 'Veg') {
      if (item === 0) {
        this.setState({
          selectedPlan: '1',
          selectedTab: 'twiceADay',
          selectedPlanDetails: {
            title: 'Monthly',
            id: '1',
            days: this.VegPriceTwiceADay[1].days,
            price: this.VegPriceTwiceADay[1].price,
          },
        });
      } else {
        this.setState({
          selectedPlan: '4',
          selectedTab: 'oneTimeADay',
          selectedPlanDetails: {
            title: 'Monthly',
            id: '4',
            days: this.VegPriceOnceADay[1].days,
            price: this.VegPriceOnceADay[1].price,
          },
        });
      }
    } else {
      if (item === 0) {
        this.setState({
          selectedPlan: '7',
          selectedTab: 'twiceADay',
          selectedPlanDetails: {
            title: 'Monthly',
            id: '7',
            days: this.NonVegPriceTwiceADay[1].days,
            price: this.NonVegPriceTwiceADay[1].price,
          },
        });
      } else {
        this.setState({
          selectedPlan: '10',
          selectedTab: 'oneTimeADay',
          selectedPlanDetails: {
            title: 'Monthly',
            id: '10',
            days: this.NonVegPriceOnceADay[1].days,
            price: this.NonVegPriceOnceADay[1].price,
          },
        });
      }
    }
  };
  dateChange = date => {
    const changedDate = moment(date, 'DD-MM-YYYY');
    if (changedDate.format('dddd').toLowerCase() === 'sunday') {
      this.setState({
        errorSunday: true,
      });
    } else {
      this.setState({
        startDate: date,
        errorSunday: false,
      });
    }
  };
  onTextChangeAllergies = text => {
    this.setState({
      allergyText: text,
    });
  };

  onSubmitButtonPress = () => {
    let allergyText = '';
    if (this.state.switchValue) {
      allergyText = this.state.allergyText;
    }
    // console.log(this.props.route.params.userDetails);
    this.props.navigation.navigate('DeliveryPreference', {
      selectedDiet: this.state.selectedDiet,
      selectedEthnicity: this.state.selectedEthnicity,
      selectedPlan: this.state.selectedPlanDetails,
      subscriptionFrequency: this.state.selectedTab,
      selectedNumberOfTiffins: this.state.selectedNumberOfTiffins,
      startDate: this.state.startDate,
      userDetails: this.props.route.params.userDetails,
      allergies: allergyText,
    });
  };
  // Render Function
  render() {
    // console.log(this.state.selectedDiet);
    // console.log(this.state.selectedEthnicity);
    // console.log(this.state.selectedPlanDetails);
    // console.log(this.state.selectedTab);
    var ethnicityDataArray = [
      {
        icon: require('../assets/Images/ChooseYourEthnicity.png'),
        title: 'Choose your Ethnicity',
        content: this.props.route.params.dietEthnicity.Ethnicity,
      },
    ];
    var dietDataArray = [
      {
        icon: require('../assets/Images/SelectYourDiet.png'),
        title: 'Select your Diet',
        content: this.props.route.params.dietEthnicity.Diet,
        image: [
          require('../assets/Images/SelectDietVeg.png'),
          require('../assets/Images/SelectDietNonVeg.png'),
        ],
      },
    ];

    var planDataArray = [
      {
        icon: require('../assets/Images/ChooseAPlan.png'),
        title: 'Choose a Plan',
        content: 'Lorem ipsum dolor sit amet',
      },
    ];
    // console.log('MIN DATE: ', this.state.minDate);
    return (
      <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
        {CustomHeader('Subscribe a Plan', true, this.props)}
        <Content contentContainerStyle={styles.contentStyle}>
          <View style={styles.contentUsableStyle}>
            <View>
              <Accordion
                icon="add"
                expandedIcon="remove"
                ref={c => (this._accordionEthnicity = c)}
                dataArray={ethnicityDataArray}
                expanded={this.state.expandedEthnicity}
                animation={true}
                headerStyle={{ backgroundColor: '#fff' }}
                contentStyle={{ backgroundColor: '#fff' }}
                renderHeader={this._renderHeader1}
                renderContent={this._renderContent1}
                key="ethnicityAccordion"
              />
              <Accordion
                icon="add"
                expandedIcon="remove"
                ref={c => (this._accordionDiet = c)}
                dataArray={dietDataArray}
                expanded={this.state.expandedDiet}
                animation={true}
                headerStyle={{ backgroundColor: '#fff' }}
                contentStyle={{ backgroundColor: '#fff' }}
                renderHeader={this._renderHeader2}
                renderContent={this._renderContent2}
                key="dietAccordion"
              />
              <Accordion
                ref={c => (this._accordionPlan = c)}
                dataArray={planDataArray}
                expanded={this.state.expandedPlan}
                animation={true}
                headerStyle={{ backgroundColor: '#fff' }}
                contentStyle={{ backgroundColor: '#fff' }}
                renderHeader={this._renderHeader3}
                renderContent={this._renderContent3}
                key="planAccordion"
              />
            </View>
            {this.state.minDate ? (
              <View style={{ flexDirection: 'row', marginTop: 12 }}>
                <Text maxFontSizeMultiplier={1} style={styles.calendarTitle}>
                  Start Date
                </Text>
                <Calendar
                  minDate={this.state.minDate}
                  notifyChange={date => this.dateChange(date)}
                />
              </View>
            ) : null}
            {this.state.errorSunday ? (
              <Text maxFontSizeMultiplier={1} style={styles.errorMessageStyle}>
                ** Sundays are Off ** {'\n'} Please choose a different date
              </Text>
            ) : null}
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: 12,
              }}>
              <Text maxFontSizeMultiplier={1} style={styles.headlineText}>
                Number of tiffins
              </Text>
              <Picker
                maxFontSizeMultiplier={1}
                // useNativeAndroidPickerStyle={false}
                selectedValue={this.state.selectedNumberOfTiffins}
                onValueChange={value =>
                  this.setState({ selectedNumberOfTiffins: value })
                }>
                <Picker.Item label="1" value="1" />
                <Picker.Item label="2" value="2" />
                <Picker.Item label="3" value="3" />
                <Picker.Item label="4" value="4" />
              </Picker>
            </View>
            {this.state.expandedPlan === 0 ? (
              <View style={styles.allergiesInput}>
                <View style={styles.allergiesSwitch}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.allergyLabelStyle}>
                    ** Allergies or special requests? {'\n'} (Roti / Rice
                    preference)
                  </Text>
                  <Switch
                    thumbColor={this.state.switchValue ? '#ec407a' : '#F9F9F9'}
                    trackColor={{ true: '#7e57c2' }}
                    onChange={() => this.onSwitchChange()}
                    value={this.state.switchValue}
                  />
                </View>
                {this.state.switchValue ? (
                  <Item regular style={styles.allergyInputTextStyleTextInput}>
                    <Textarea
                      onChangeText={text => this.onTextChangeAllergies(text)}
                      value={this.state.allergyText}
                      rowSpan={5}
                      placeholder="Mention your allergies and special requests here."
                      style={styles.allergyInputTextStyle}
                    />
                  </Item>
                ) : null}
              </View>
            ) : null}
          </View>
          <View>
            {this.state.selectedPlan && !this.state.errorSunday ? (
              <Button
                rounded
                style={styles.continueButton}
                onPress={() => this.onSubmitButtonPress()}>
                <Text
                  maxFontSizeMultiplier={1}
                  style={styles.continueButtonText}>
                  Continue
                </Text>
              </Button>
            ) : (
              <Button rounded style={styles.continueButtonDisabled}>
                <Text
                  maxFontSizeMultiplier={1}
                  style={styles.continueButtonText}>
                  Continue
                </Text>
              </Button>
            )}
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
  },
  contentUsableStyle: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
  },
  headlineText: {
    fontFamily: 'Poppins-Medium',
    color: '#2a3452',
    // marginLeft: 8,
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  renderHeaderViewStyle: {
    flexDirection: 'row',
    padding: 15,
    alignItems: 'center',
  },
  renderHeaderTextStyle: {
    fontFamily: 'Poppins-Bold',
    fontSize: 14,
    color: '#2a3452',
    marginLeft: 10,
  },
  renderContent1ViewStyle: {
    backgroundColor: '#eceff1',
    borderColor: '#d8dadf',
    margin: 5,
    borderRadius: 6,
  },
  renderContent1TextStyle: {
    fontFamily: 'Poppins-Regular',
    padding: 8,
  },
  renderContent1TextStyleDisabled: {
    fontFamily: 'Poppins-Regular',
    padding: 8,
    color: '#e0e0e0',
  },
  renderContent2UpperViewStyle: {
    flexDirection: 'row',
    flex: 1,
    // flexWrap: 'wrap',
    justifyContent: 'center',
  },
  renderContent2ViewStyle: {
    flex: 1,
    backgroundColor: '#eceff1',
    borderColor: '#d8dadf',
    margin: 5,
    borderRadius: 6,
    width: Dimensions.get('window').width / 2.4,
    height: 120,
    justifyContent: 'center',
    alignItems: 'center',
  },
  renderContent2TextStyle: {
    fontFamily: 'Poppins-Regular',
    padding: 8,
    textAlign: 'center',
  },
  tabHeaderStyle: {
    backgroundColor: 'grey',
  },
  tab1ViewStyle: {
    padding: 15,
    // alignItems: 'center',
    justifyContent: 'center',
  },
  tab1SubtitleStyle: {
    color: '#656c82',
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
  },
  listItemStyle: {
    // flex: 1,
    padding: 15,
    borderBottomColor: '#f5f5f5',
    borderBottomWidth: 1,
  },
  listItemTitle: {
    flex: 1,
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    marginLeft: 10,
    // backgroundColor: 'blue',
  },
  listItemPrice: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    // backgroundColor: 'yellow',
  },
  listItemPriceCancelled: {
    fontFamily: 'Poppins-Light',
    fontSize: 18,
    textDecorationLine: 'line-through',
    color: 'grey',
    marginRight: 8,
  },
  continueButton: {
    backgroundColor: '#fc2250',
    marginTop: 40,
    marginBottom: 40,
    alignSelf: 'center',
  },
  continueButtonDisabled: {
    backgroundColor: '#d8dadf',
    marginTop: 40,
    marginBottom: 40,
    alignSelf: 'center',
  },
  continueButtonText: {
    paddingLeft: 50,
    paddingRight: 50,
  },
  calendarTitle: {
    flex: 1,
    textAlign: 'left',
    marginRight: 20,
    fontFamily: 'Poppins-Medium',
    // fontSize: 14,
    color: '#2a3452',
    paddingVertical: 10,
  },
  errorMessageStyle: {
    textAlign: 'center',
    // marginRight: 20,
    fontFamily: 'Poppins-Medium',
    fontSize: 14,
    color: '#c62828',
    marginVertical: 10,
  },
  allergiesSwitch: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  allergiesInput: {
    marginTop: 20,
    marginBottom: 20,
    // marginLeft: 16,
    // marginRight: 16,
  },
  allergyLabelStyle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 14,
    color: '#131e40',
  },
  allergyInputTextStyle: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#131e40',
  },
  allergyInputTextStyleTextInput: {
    marginTop: 16,
  },
});
