import React, { Component } from 'react';
import { StyleSheet, View, ImageBackground, Image } from 'react-native';
import * as Animatable from 'react-native-animatable';
import { CustomHeader } from '../components/CustomHeader';
import { Container, Content, Button, Text } from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// import { NavigationActions } from 'react-navigation';
import GLOBAL from '../components/Global.js';

export default class CookProfilePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cookName: '',
      fssaiCode: '',
      messageToCustomer: '',
      speciality: '',
    };
  }
  componentDidMount() {
    if (this.props) {
      if (this.props.route.params) {
        let testArray = this.props.route.params.cookMenuDetails.name.split(' ');
        this.setState({
          cookName: this.props.route.params.cookMenuDetails.name,
          cookFirstName: testArray[0],
          fssaiCode: this.props.route.params.cookMenuDetails.fssaiCode,
          messageToCustomer: this.props.route.params.cookMenuDetails
            .messageToCustomer,
          speciality: this.props.route.params.cookMenuDetails.foodSpeciality,
          cuisine: this.props.route.params.cookMenuDetails.ethnicity,
          homeChefSince: this.props.route.params.cookMenuDetails.startDate,
          photo: this.props.route.params.cookMenuDetails.photoLink,
        });
      }
    }
  }

  continueButtonPress = () => {
    this.props.navigation.goBack();
  };
  render() {
    return (
      // <StyleProvider style={getTheme(material)}>
      <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
        {CustomHeader('HomeChef Profile', true, this.props)}
        <Content contentContainerStyle={styles.contentStyle}>
          <View style={styles.contentUsableStyle}>
            <View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginTop: 16,
                }}>
                <Text maxFontSizeMultiplier={1} style={styles.headlineText2}>
                  Foozinga Certified{'   '}
                </Text>
                <MaterialCommunityIcons
                  name="check-decagram"
                  style={{
                    fontSize: 24,
                    color: '#fc2250',
                    textAlign: 'center',
                  }}
                />
              </View>
              <Text
                style={{
                  fontFamily: 'Poppins-Light',
                  fontSize: 12,
                  textAlign: 'center',
                }}>
                (Foozinga Home Chef since - {this.state.homeChefSince})
              </Text>
            </View>
            <ImageBackground
              style={styles.profileImageView}
              source={require('../assets/Images/internetNotWorkingBckGrnd.jpg')}
              resizeMode="cover"
              imageStyle={{
                borderRadius: 0,
                flex: 1,
                opacity: 1,
                borderRadius: 10,
              }}>
              <Image
                style={styles.SummaryFoodImage}
                source={{
                  uri: this.state.photo,
                }}
              />
              <View style={{ marginLeft: 8 }}>
                <Text style={styles.headlineText}>{this.state.cookName}</Text>
                <Text style={styles.informationText}>
                  <Text style={{ fontFamily: 'Poppins-Medium' }}>
                    FSSAI No.:{' '}
                  </Text>
                  {'\n' + this.state.fssaiCode}
                </Text>
                <Text
                  style={{
                    ...styles.informationText,
                    textTransform: 'capitalize',
                  }}>
                  <Text style={{ fontFamily: 'Poppins-Medium' }}>
                    Cuisine:{' '}
                  </Text>
                  {'\n' + this.state.cuisine}
                </Text>
              </View>
            </ImageBackground>
          </View>
          <View style={styles.contentUsableStyle2}>
            {this.state.messageToCustomer ? (
              <Text
                maxFontSizeMultiplier={1}
                style={{
                  ...styles.headlineText,
                  textTransform: 'none',
                  marginTop: 16,
                }}>
                Message from {this.state.cookFirstName} :{'\n'}
                <Text maxFontSizeMultiplier={1} style={styles.descriptionText}>
                  "{this.state.messageToCustomer}"
                </Text>
              </Text>
            ) : null}
            {this.state.speciality ? (
              <Text
                maxFontSizeMultiplier={1}
                style={{
                  ...styles.headlineText,
                  textTransform: 'none',
                  marginTop: 16,
                }}>
                Food Speciality :{'\n'}
                <Text maxFontSizeMultiplier={1} style={styles.descriptionText}>
                  {this.state.cookFirstName} prepares a mean{' '}
                  <Text style={{ fontFamily: 'Poppins-Medium', fontSize: 16 }}>
                    {this.state.speciality}
                  </Text>
                  . Be sure to try it at least once during your subscription.
                  {'\n\n'}
                </Text>
                <Text
                  style={{ ...styles.descriptionText, fontStyle: 'italic' }}>
                  Tip: If you do not have it in the menu during your
                  subscription, don't shy away from sending us a request.{'   '}
                  <MaterialCommunityIcons
                    name="emoticon-wink-outline"
                    style={{
                      fontSize: 24,
                      color: 'black',
                    }}
                  />
                </Text>
              </Text>
            ) : null}
          </View>
        </Content>
      </Container>
      // </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  contentStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
  },
  contentUsableStyle: {
    marginLeft: 16,
    marginRight: 16,
    // flex: 1,
  },
  contentUsableStyle2: {
    marginLeft: 16,
    marginRight: 16,
    marginTop: 16,
    flex: 1,
  },
  profileImageView: {
    flexDirection: 'row',
    borderRadius: 20,
    height: 240,
    alignItems: 'center',
  },
  SummaryFoodImage: {
    height: 180,
    width: 150,
    resizeMode: 'contain',
    borderRadius: 20,
    alignSelf: 'center',
  },
  headlineText: {
    fontFamily: 'Poppins-Medium',
    color: '#131e40',
    fontSize: 18,
    textTransform: 'capitalize',
  },
  headlineText2: {
    fontFamily: 'Poppins-Medium',
    color: '#131e40',
    fontSize: 20,
    textTransform: 'capitalize',
    textAlign: 'center',
  },
  informationText: {
    marginTop: 12,
    fontFamily: 'Poppins-Regular',
    color: '#131e40',
    fontSize: 18,
  },
  descriptionText: {
    marginTop: 12,
    fontFamily: 'Poppins-Regular',
    color: '#131e40',
    fontSize: 16,
  },
  continueButton: {
    backgroundColor: '#fc2250',
    marginBottom: 40,
    alignSelf: 'center',
    marginTop: 30,
  },
  continueButtonText: {
    paddingLeft: 50,
    paddingRight: 50,
    color: '#fff',
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
  },
});
