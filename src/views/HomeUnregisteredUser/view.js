import React from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  Modal,
  ImageBackground,
} from 'react-native';
import { CustomHeader } from '../../components/CustomHeader';
import { Container, Content, Title, Subtitle, Button } from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { styles } from '../HomeUnregisteredUser/style';
import GLOBAL from '../../components/Global.js';
import RBSheet from 'react-native-raw-bottom-sheet';
import PhoneAuthNavigator from '../../navigation/PhoneNumberAuth.js';
import LottieView from 'lottie-react-native';
import * as Animatable from 'react-native-animatable';

export function render(viewState) {
  return (
    <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
      {CustomHeader(null, null, viewState.props, 0)}
      <Content contentContainerStyle={styles.contentStyle}>
        <View style={styles.contentUsableStyle}>
          <Title maxFontSizeMultiplier={1} style={styles.headlineText}>
            Homefood for the Homesick
          </Title>
          <Text maxFontSizeMultiplier={1} style={styles.subHeadingText}>
            Home cooked food based on your preferred ethnicity
          </Text>
          <Text maxFontSizeMultiplier={1} style={styles.subHeadingText}>
            Click on an ethnicity to view a sample menu.
          </Text>
          <View style={styles.ethnicityGrid}>
            <TouchableOpacity
              onPress={() => viewState.showSampleMenu('Bengali')}>
              <View style={styles.gridShapeStyle} />
              <Text maxFontSizeMultiplier={1} style={styles.gridText}>
                Bengali
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => viewState.showSampleMenu('Nth. Indian')}>
              <View style={styles.gridShapeStyle} />
              <Text maxFontSizeMultiplier={1} style={styles.gridText}>
                Nth. Indian
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => viewState.showSampleMenu('Assamese')}>
              <View style={styles.gridShapeStyle} />
              <Text maxFontSizeMultiplier={1} style={styles.gridText}>
                Assamese
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => viewState.showSampleMenu('Andhra')}>
              <View style={styles.gridShapeStyle} />
              <Text maxFontSizeMultiplier={1} style={styles.gridText}>
                Andhra
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.ethnicityGrid2}>
            <TouchableOpacity onPress={() => viewState.showSampleMenu('Odia')}>
              <View style={styles.gridShapeStyle} />
              <Text maxFontSizeMultiplier={1} style={styles.gridText}>
                Odia
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => viewState.showSampleMenu('Kerala')}>
              <View style={styles.gridShapeStyle} />
              <Text maxFontSizeMultiplier={1} style={styles.gridText}>
                Kerala
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => viewState.showSampleMenu('Tamil')}>
              <View style={styles.gridShapeStyle} />
              <Text maxFontSizeMultiplier={1} style={styles.gridText}>
                Tamil
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.gridShapeStyle2}
              onPress={() =>
                viewState.setState({
                  modalVisible: true,
                })
              }>
              <View>
                <View style={styles.gridIconStyle}>
                  <MaterialCommunityIcons name="chevron-right" size={30} />
                </View>
              </View>
              <Text maxFontSizeMultiplier={1} style={styles.gridTextAction}>
                View All
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <Animatable.View
              animation="bounceIn"
              useNativeDriver={true}
              style={styles.centerAddressContent}>
              <ImageBackground
                source={require('../../assets/Images/bangalore-map-color.jpg')}
                style={{}}
                imageStyle={{ borderRadius: 10, width: '95%' }}>
                <TouchableOpacity
                  style={{
                    // paddingLeft: 20,
                    paddingVertical: 10,
                    flexDirection: 'row',
                    backgroundColor: 'rgba(255, 255, 255, 0.1)',
                  }}
                  onPress={() =>
                    viewState.props.navigation.push('ServiceAreaCheck')
                  }>
                  <View style={{ flexDirection: 'column', marginLeft: 10 }}>
                    <View
                      style={{ flexDirection: 'row', alignItems: 'center' }}>
                      <Image
                        style={{ width: 30, height: 30 }}
                        source={require('../../assets/Images/location-pin-3.png')}
                      />
                      <Text
                        maxFontSizeMultiplier={1}
                        style={styles.headlineTextServiceCheck}>
                        Get your meals delivered at home or office
                      </Text>
                    </View>
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.subHeadingTextServiceCheck}>
                      Check if we are available at your location
                    </Text>
                  </View>
                  <View style={styles.serviceAreaCheckIconButton}>
                    <MaterialCommunityIcons
                      name="chevron-right"
                      style={styles.serviceAreaCheckIcon}
                    />
                  </View>
                </TouchableOpacity>
              </ImageBackground>
            </Animatable.View>
          </View>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <View style={styles.referralContent}>
              <View style={{}}>
                <Text
                  maxFontSizeMultiplier={1}
                  style={{ fontSize: 16, ...styles.headlineText }}>
                  Two Day Trial
                </Text>
                <Text
                  maxFontSizeMultiplier={1}
                  style={styles.subHeadingTextTrial}>
                  <Text style={{ fontWeight: 'bold' }}>
                    Only for new customers
                  </Text>
                  {'\n'}Connect with us on
                  {'\n'}WhatsApp for a two day trial
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    viewState.setState({ modalTrialVisible: true })
                  }>
                  <Text maxFontSizeMultiplier={1} style={styles.gridTextAction}>
                    Request Trial
                  </Text>
                </TouchableOpacity>
              </View>
              <Image
                style={styles.referralContentImage}
                source={require('../../assets/Images/UnregisteredUserReferal.png')}
                resizeMode="contain"
              />
            </View>
          </View>
        </View>
      </Content>
      <Modal
        animationType="slide"
        transparent={true}
        // presentationStyle="fullScreen"
        statusBarTranslucent={true}
        visible={viewState.state.modalVisible}
        onRequestClose={() => {
          viewState.setState({
            modalVisible: false,
          });
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text maxFontSizeMultiplier={1} style={styles.modalText}>
              Please login to continue.
            </Text>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
              <Button
                style={{ marginHorizontal: 10 }}
                transparent
                onPress={() => {
                  viewState.setState({ modalVisible: false });
                }}>
                <Text maxFontSizeMultiplier={1} style={styles.textStyle}>
                  Cancel
                </Text>
              </Button>
              <Button
                style={{ marginHorizontal: 10 }}
                transparent
                onPress={() => {
                  viewState.RBSheet.open();
                }}>
                <Text maxFontSizeMultiplier={1} style={styles.textStyleLogin}>
                  Login
                </Text>
              </Button>
            </View>
          </View>
        </View>
        <RBSheet
          ref={ref => {
            viewState.RBSheet = ref;
          }}
          closeOnPressMask={false}
          onClose={args => {
            if (args === true) {
              // console.log('loggedIn user');
              GLOBAL.screen.setState({
                loggedIn: true,
              });
            }
          }}
          height={450}
          openDuration={250}
          customStyles={{
            container: {
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
              padding: 30,
            },
          }}>
          <TouchableOpacity
            style={styles.closeButtonStyle}
            onPress={() => {
              viewState.RBSheet.close();
            }}>
            <MaterialCommunityIcons
              name="close"
              style={{ fontSize: 25, color: 'black' }}
            />
          </TouchableOpacity>
          <PhoneAuthNavigator prop={viewState} />
        </RBSheet>
      </Modal>
      {/* Menu Modal */}
      <Modal
        animationType="slide"
        transparent={false}
        presentationStyle="overFullScreen"
        statusBarTranslucent={false}
        visible={viewState.state.modalVisibleMenu}
        onRequestClose={() => {
          viewState.setState({
            modalVisibleMenu: false,
          });
        }}>
        <Content>
          <View style={styles.centeredViewMenu}>
            <View style={styles.modalViewMenu}>
              <Text maxFontSizeMultiplier={1} style={styles.modalTextTitleMenu}>
                {viewState.state.clickedEthnicity}
              </Text>
              <Text maxFontSizeMultiplier={1} style={styles.modalTextInfoMenu}>
                Here is a sample menu from one of our premiere{' '}
                {viewState.state.clickedEthnicity} homeChefs.
              </Text>
              <Image
                style={styles.sampleMenuImage}
                source={viewState.state.clickedEthnicityImageSource}
                resizeMode="contain"
              />
              <Text maxFontSizeMultiplier={1} style={styles.modalTextInfoMenu}>
                <Text maxFontSizeMultiplier={1} style={{ fontWeight: 'bold' }}>
                  Note:
                </Text>{' '}
                This menu changes every week. {'\n'}Non-Veg - 6 times a week
                alternated between lunch & dinner
              </Text>
              <Text maxFontSizeMultiplier={1} style={styles.modalTextInfoMenu}>
                ** We are off on Sundays.
              </Text>
              <View
                style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                <Button
                  style={{ marginHorizontal: 10 }}
                  transparent
                  onPress={() => {
                    viewState.setState({ modalVisibleMenu: false });
                  }}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.textStyleCancel}>
                    Back
                  </Text>
                </Button>
                {/* <Button
                  style={{ marginHorizontal: 10 }}
                  transparent
                  onPress={() => viewState.gotoSubscribeAPlan(user)}>
                  <Text style={styles.textStyleConnect}>Select</Text>
                </Button> */}
              </View>
            </View>
          </View>
        </Content>
      </Modal>
      {/* Modal View for Request Trial */}
      <Modal
        animationType="slide"
        transparent={true}
        // presentationStyle="fullScreen"
        statusBarTranslucent={true}
        visible={viewState.state.modalTrialVisible}
        onRequestClose={() => {
          viewState.setState({
            modalTrialVisible: false,
          });
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text maxFontSizeMultiplier={1} style={styles.modalText}>
              Connect over WhatsApp ?
            </Text>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
              <Button
                style={{ marginHorizontal: 10 }}
                transparent
                onPress={() => {
                  viewState.setState({ modalTrialVisible: false });
                }}>
                <Text maxFontSizeMultiplier={1} style={styles.textStyleCancel}>
                  Cancel
                </Text>
              </Button>
              <Button
                style={{ marginHorizontal: 10 }}
                transparent
                onPress={() => {
                  viewState.sendOnWhatsApp();
                }}>
                <Text maxFontSizeMultiplier={1} style={styles.textStyleConnect}>
                  Connect
                </Text>
              </Button>
            </View>
          </View>
        </View>
      </Modal>
    </Container>
  );
}
