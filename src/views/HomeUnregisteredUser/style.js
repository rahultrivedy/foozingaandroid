import { StyleSheet } from 'react-native';
export const styles = StyleSheet.create({
  contentStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
  },
  contentUsableStyle: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
  },
  headlineText: {
    fontFamily: 'Poppins-Medium',
    color: '#2a3452',
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  ethnicityGridGroup: {
    justifyContent: 'space-between',
  },
  ethnicityGrid: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 16,
  },
  ethnicityGrid2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  gridText: {
    fontFamily: 'Poppins-Regular',
    textAlign: 'center',
    color: '#656c82',
  },
  gridShapeStyle: {
    width: 73,
    height: 62,
    backgroundColor: 'skyblue',
    flexDirection: 'column',
    borderRadius: 10,
  },
  gridShapeStyle2: {
    width: 73,
    height: 72,
    flexDirection: 'column',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5,
    // backgroundColor: 'green',
  },
  gridIconStyle: {
    width: 50,
    height: 50,
    borderRadius: 50,
    backgroundColor: '#eef2f5',
    alignItems: 'center',
    justifyContent: 'center',
  },
  gridTextAction: {
    fontFamily: 'Poppins-Regular',
    color: '#fc2250',
  },
  centerAddressContent: {
    flexDirection: 'row',
    marginTop: 16,
    backgroundColor: '#f8f8f8',
    alignItems: 'center',
    borderRadius: 5,
  },
  centerAddressContentImage: {
    width: 82,
    height: 60,
  },
  headlineTextServiceCheck: {
    fontFamily: 'Poppins-Medium',
    color: '#000',
    fontSize: 16,
    width: '85%',
    // backgroundColor: 'rgba(0,0,0, 0.1)',
  },
  subHeadingTextServiceCheck: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#000',
    width: '85%',
    marginLeft: 30,
    // backgroundColor: 'rgba(0,0,0, 0.1)',
  },
  serviceAreaCheckIconButton: {
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: '#fff',
    // marginRight: 0,
    right: 20,
  },
  serviceAreaCheckIcon: {
    color: '#fc2250',
    fontSize: 32,
    backgroundColor: '#fff',
    // marginRight: 10,
    borderColor: '#fc2250',
    borderWidth: 0.5,
  },
  referralContent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    // marginTop: 20,
  },
  subHeadingTextTrial: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  referralContentImage: {
    height: 129,
    width: 168,
    paddingLeft: 20,
  },
  //Modal
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
  modalView: {
    backgroundColor: 'white',
    borderRadius: 10,
    paddingHorizontal: 50,
    paddingVertical: 25,
    paddingBottom: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  },
  textStyle: {
    fontFamily: 'Poppins-Medium',
    color: '#fc2250',
    fontSize: 16,
    // textAlign: 'center',
  },
  textStyleLogin: {
    fontFamily: 'Poppins-Medium',
    fontSize: 18,
    color: '#fc2250',
  },
  modalText: {
    fontFamily: 'Poppins-Regular',
    marginBottom: 15,
    textAlign: 'center',
  },
  // Modal END

  //ModalMenu Start
  centeredViewMenu: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // marginTop: 50,
    backgroundColor: 'rgba(0,0,0,0.7)',
  },
  modalViewMenu: {
    backgroundColor: '#fff',
    // borderRadius: 10,
    padding: 25,
    paddingBottom: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  },
  modalTextTitleMenu: {
    fontFamily: 'Poppins-Medium',
    fontSize: 24,
    marginTop: 8,
    marginBottom: 12,
    color: '#2a3452',
  },
  modalTextInfoMenu: {
    fontFamily: 'Poppins-Regular',
    marginBottom: 15,
    // textAlign: 'center',
  },
  modalTextMenu: {
    fontFamily: 'Poppins-Regular',
    marginBottom: 15,
    textAlign: 'center',
  },
  textStyleConnect: {
    fontFamily: 'Poppins-Regular',
    fontSize: 18,
    color: '#fc2250',
  },
  textStyleCancel: {
    fontFamily: 'Poppins-Regular',
    fontSize: 18,
    color: '#fc2250',
  },
  sampleMenuImage: {
    height: 420,
    width: 320,
    alignSelf: 'center',
  },
  // Modal END
});
