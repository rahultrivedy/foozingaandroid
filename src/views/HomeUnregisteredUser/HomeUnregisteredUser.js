import { Component } from 'react';
import { View, Image, Text, TouchableOpacity, Linking } from 'react-native';
import { render } from '../HomeUnregisteredUser/view';
import { UserService } from '../../../Services/User/user.service';
import { ApplicationConstants } from '../../../Services/ApplicationConstants/application.service';
import { AuthService } from '../../../Services/Authentication/auth.service';
import * as Constants from '../../../Services/AppConstants/app.consts';
// import GLOBAL from '../../components/Global.js';

export default class HomeUnregisteredUser extends Component {
  pricing = {};
  dietEthnicity = {};
  constructor() {
    super();
  }
  state = {
    dietEth: [],
    modalVisible: false,
    modalVisibleMenu: false,
    modalTrialVisible: false,
    clickedEthnicity: '',
    clickedEthnicityImageSource: require('../../assets/Images/EthnicityImages/BengaliSampleMenu.jpg'),
  };

  sendOnWhatsApp() {
    // let userId = AuthService.getCurrentUserUserId();
    // let userIdTrimmed = userId.substring(userId.length - 4);
    let message = '*Request Two Day Trial*' + '\n' + 'Name: ';
    let url = 'whatsapp://send?text=' + message + '&phone=91' + '7204630078';
    Linking.openURL(url)
      .then(data => {
        // console.log('WhatsApp Opened');
        this.setState({
          modalTrialVisible: false,
        });
      })
      .catch(() => {
        alert('Make sure Whatsapp installed on your device');
      });
  }

  showSampleMenu(ethnicity) {
    this.selectEthnicityImage('', ethnicity);
    this.setState({
      clickedEthnicity: ethnicity,
      modalVisibleMenu: true,
    });
  }

  selectEthnicityImage(ethnicity, selectedEthnicity) {
    var ethnicityImages = [
      {
        name: 'Bengali',
        image: require('../../assets/Images/EthnicityImages/Bengali.jpg'),
        sampleMenuImage: require('../../assets/Images/EthnicityImages/BengaliSampleMenu.jpg'),
      },
      {
        name: 'Nth. Indian',
        image: require('../../assets/Images/EthnicityImages/Nth-Indian.jpg'),
        sampleMenuImage: require('../../assets/Images/EthnicityImages/Nth-IndianSampleMenu.jpg'),
        // sampleMenuImage: require('../../assets/Images/EthnicityImages/BengaliSampleMenu.jpg'),
      },
      {
        name: 'Assamese',
        image: require('../../assets/Images/EthnicityImages/Assamese.jpg'),
        sampleMenuImage: require('../../assets/Images/EthnicityImages/AssameseSampleMenu.jpg'),
        // sampleMenuImage: require('../../assets/Images/EthnicityImages/BengaliSampleMenu.jpg'),
      },
      {
        name: 'Odia',
        image: require('../../assets/Images/EthnicityImages/Oriya.png'),
        sampleMenuImage: require('../../assets/Images/EthnicityImages/OriyaSampleMenu.jpg'),
        // sampleMenuImage: require('../../assets/Images/EthnicityImages/BengaliSampleMenu.jpg'),
      },
      {
        name: 'Andhra',
        image: require('../../assets/Images/EthnicityImages/Bihari.jpg'),
        // sampleMenuImage: require('../../assets/Images/EthnicityImages/BihariSampleMenu.jpg'),
        sampleMenuImage: require('../../assets/Images/EthnicityImages/AndhraSampleMenu.jpg'),
      },
      {
        name: 'Gujarati',
        image: require('../../assets/Images/EthnicityImages/Gujarati.jpg'),
        // sampleMenuImage: require('../../assets/Images/EthnicityImages/GujratiSampleMenu.jpg'),
        sampleMenuImage: require('../../assets/Images/EthnicityImages/BengaliSampleMenu.jpg'),
      },
      {
        name: 'Tamil',
        image: require('../../assets/Images/EthnicityImages/Assamese.jpg'),
        // sampleMenuImage: require('../../assets/Images/EthnicityImages/AssameseSampleMenu.jpg'),
        sampleMenuImage: require('../../assets/Images/EthnicityImages/TamilSampleMenu.jpg'),
      },
      {
        name: 'Kerala',
        image: require('../../assets/Images/EthnicityImages/Assamese.jpg'),
        // sampleMenuImage: require('../../assets/Images/EthnicityImages/AssameseSampleMenu.jpg'),
        sampleMenuImage: require('../../assets/Images/EthnicityImages/KerelaSampleMenu.jpg'),
      },
    ];
    if (ethnicity) {
      let filteredClasses = ethnicityImages.filter(image =>
        ethnicity.includes(image.name),
      );
      if (filteredClasses[0]) {
        return filteredClasses[0].image;
      } else {
        return require('../../assets/Images/EthnicityImages/defaultEthnicity.png');
      }
    } else if (selectedEthnicity) {
      let filteredClasses = ethnicityImages.filter(image =>
        selectedEthnicity.includes(image.name),
      );
      if (filteredClasses[0]) {
        this.setState({
          clickedEthnicityImageSource: filteredClasses[0].sampleMenuImage,
        });
      } else {
        // return require('../../assets/Images/EthnicityImages/defaultEthnicity.png');
        // console.log('NORETURN');
      }
    }
  }

  render() {
    return render(this);
  }
}
