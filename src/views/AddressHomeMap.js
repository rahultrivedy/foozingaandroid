import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  PermissionsAndroid,
  Alert,
  ScrollView,
  Linking,
  Modal,
} from 'react-native';
import {
  Container,
  Title,
  Button,
  Icon,
  Item,
  Label,
  Input,
  Form,
} from 'native-base';
import MapView, { PROVIDER_GOOGLE, AnimatedRegion } from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import Geocoder from 'react-native-geocoding';
Geocoder.init('AIzaSyBYAEY9EGT0in18UIVzM0tlSILfCW120cI');
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import GeoFencing from 'react-native-geo-fencing';
import * as Constants from '../../Services/AppConstants/app.consts';
import { UserService } from '../../Services/User/user.service';
import { AuthService } from '../../Services/Authentication/auth.service';
import { ApplicationConstants } from '../../Services/ApplicationConstants/application.service';
import GLOBAL from '../components/Global';

export default class AddressHomeMap extends Component {
  #applicationConstants;
  #userService;
  constructor() {
    super();
    global.AddressAutoCounter = 1;
    this.#userService = new UserService();
    this.#applicationConstants = new ApplicationConstants();
  }

  state = {
    addressChangeAllowed: true,
    deliveryLocation: '',
    initialPosition: null,
    finalPosition: null,
    coordinate: new AnimatedRegion({
      latitude: null,
      longitude: null,
    }),
    geoCodeAddress: '',
    geoCodeAddressAuto: null,
    geoCodeAddressLine1: '',
    geoCodeAddressLine1Auto: null,
    onRegionChangeCounter: 0,
    geoCodeLatitude: '',
    geoCodeLongitude: '',
    // inputStyle: 'styles.inputStyleFocused',
    inputStyleHouse: 'gray',
    inputStyleBuilding: 'gray',
    inputStyleLandmark: 'gray',
    addressLandmark: '',
    addressHouseNo: '',
    addressBuildingName: '',
    addressAvailable: false,
    serviceAvailable: true,
    polygonGeoArray: '',
    processing: false,
  };
  requestLocationPermission = async () => {
    //Calling the permission function
    const response = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );
    // console.log(response);
    if (response === 'granted') {
      this.setState({
        processing: true,
      });
      this.locateCurrentPosition();
    }
  };

  componentDidMount() {
    this.#applicationConstants
      .getServiceArea()
      .then(querySnapshot => {
        let polygonGeoArray = [];
        querySnapshot.forEach(documentSnapshot => {
          // console.log(documentSnapshot.data());
          let arrayGeoCoords = [];
          for (var key in documentSnapshot.data()) {
            var coord = {};
            if (documentSnapshot.data().hasOwnProperty(key)) {
              var val = documentSnapshot.data()[key];
              coord.lat = val.latitude;
              coord.lng = val.longitude;
              arrayGeoCoords.push(coord);
            }
          }
          // console.log(arrayGeoCoords);
          polygonGeoArray.push(arrayGeoCoords);
        });
        // console.log('array of polygons');
        // console.log(polygonGeoArray);
        this.setState({
          polygonGeoArray: polygonGeoArray,
        });
      })
      .catch(error => {
        console.log(error);
      });
    // console.log('props in component did  mount');
    // console.log(this.props);
    this.focusListener = this.props.navigation.addListener('focus', () => {
      // console.log('FOCUS IS HERE');
      if (this.props.route.params) {
        // console.log(this.props.route.params);
        if (this.props.route.params.params) {
          if (this.props.route.params.params.addressAutocomplete) {
            var addressComponent = this.props.route.params.params
              .addressAutocomplete;
            // console.log(this.props.route.params.params.addressAutocomplete.vicinity);
            // console.log(addressComponent.formatted_address);
            if (addressComponent.formatted_address) {
              let region = {
                latitude: addressComponent.geometry.location.lat,
                longitude: addressComponent.geometry.location.lng,
                latitudeDelta: 0.09,
                longitudeDelta: 0.035,
              };
              this.checkIfServiceAvailable(region);
              this.setState({
                geoCodeAddress: addressComponent.formatted_address,
                geoCodeAddressLine1: addressComponent.name,
                finalPosition: region,
              });
              if (this._map) {
                this._map.animateToRegion({
                  latitude: addressComponent.geometry.location.lat,
                  longitude: addressComponent.geometry.location.lng,
                  latitudeDelta: 0.0005,
                  longitudeDelta: 0.005,
                });
              }
              global.AddressAutoCounter = 0;
              // this.onRegionChange(region);
            } else if (addressComponent.isCurrentLocation) {
              // console.log(addressComponent);
              this.requestLocationPermission();
            }
          }
        } else if (this.props.route.params.deliveryLocation) {
          this.setState({
            deliveryLocation: this.props.route.params.deliveryLocation,
          });
          if (this.props.route.params.fromScreen) {
            if (!this.props.route.params.addressAvailable) {
              // console.log('ADDRESS NOT AVAILABLE');
              this.setState({
                fromScreen: this.props.route.params.fromScreen,
                userName: this.props.route.params.userName,
                addressAvailable: false,
                addressChangeAllowed: true,
              });
            } else {
              // console.log('ADDRESS AVAILABLE');
              if (GLOBAL.screen.state.subscribed) {
                // console.log('SUBSCRIBED');
                this.setState({
                  fromScreen: this.props.route.params.fromScreen,
                  userName: this.props.route.params.userName,
                  addressAvailable: true,
                  addressChangeAllowed: false,
                });
              } else {
                // console.log('NOT SUBSCRIBED');
                this.setState({
                  fromScreen: this.props.route.params.fromScreen,
                  userName: this.props.route.params.userName,
                  addressAvailable: true,
                  addressChangeAllowed: true,
                });
              }
            }
          }
        }
      }
      this.props.route.params = null;
    });
    this.requestLocationPermission();
  }
  componentWillUnmount() {
    // Remove the event listener before removing the screen from the stack
    // this.focusListener.remove();
  }

  checkIfServiceAvailable(region) {
    let point = {
      lat: region.latitude,
      lng: region.longitude,
    };
    if (this.state.polygonGeoArray) {
      let allRequests = [];
      let responseArray = [];
      for (var i = 0; i < this.state.polygonGeoArray.length; i++) {
        try {
          allRequests.push(
            GeoFencing.containsLocation(point, this.state.polygonGeoArray[i])
              .then(response => responseArray.push(response))
              .catch(error => console.log('response', error)),
          );
        } catch (e) {
          // handle error
          console.log(e);
        }
      }
      Promise.all(allRequests)
        .then(values => {
          // console.log('Values of PROMISES');
          // console.log(values);
          if (values.includes(1)) {
            this.setState({ serviceAvailable: true });
          } else {
            this.setState({ serviceAvailable: false });
          }
        })
        .catch(error => console.log(error));
    }
    // GeoFencing.containsLocation(point, this.state.serviceArea.polygon1)
    //   .then(() => this.setState({ serviceAvailable: true }))
    //   // .catch(() => this.setState({ serviceAvailable: false }));

    //   .catch(() =>
    //     GeoFencing.containsLocation(point, this.state.serviceArea.polygon2)
    //       .then(() => this.setState({ serviceAvailable: true }))
    //       .catch(() => this.setState({ serviceAvailable: false })),
    //   );
    // return;
  }

  locateCurrentPosition = () => {
    Geolocation.getCurrentPosition(
      position => {
        // console.log(position);

        let region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.09,
          longitudeDelta: 0.035,
        };
        this.checkIfServiceAvailable(region);
        this.setState({
          initialPosition: region,
          finalPosition: region,
        });
        this._map.animateToRegion({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.0005,
          longitudeDelta: 0.005,
        });
        Geocoder.from(position.coords.latitude, position.coords.longitude)
          .then(json => {
            var addressComponent =
              json.results[json.results.length - 7].formatted_address;
            var addressLine1 =
              json.results[0].address_components[0].short_name +
              ', ' +
              json.results[0].address_components[1].short_name;
            // console.log('geocoder');
            // console.log(json);
            this.setState({
              geoCodeAddress: addressComponent,
              geoCodeAddressLine1: addressLine1,
            });
          })
          .catch(error => console.warn(error));
        this.setState({
          processing: false,
        });
      },
      error => Alert.alert(error.message),
      { enableHighAccuracy: true, timeout: 10000, maximumAge: 1000 },
    );
  };

  backButton = () => {
    this.props.navigation.goBack();
  };

  markerAnimateToCurrentPosition = () => {
    const newCoordinate = {
      latitude: this.state.initialPosition.latitude,
      longitude: this.state.initialPosition.longitude,
    };
    // console.log(newCoordinate);
    this._map.animateToRegion({
      latitude: newCoordinate.latitude,
      longitude: newCoordinate.longitude,
      latitudeDelta: 0.005,
      longitudeDelta: 0.005,
    });
    if (this.marker) {
      // console.log(newCoordinate);
      this.marker.animateMarkerToCoordinate(newCoordinate, 500);
    }
  };

  onRegionChange = region => {
    this.checkIfServiceAvailable(region);
    // console.log('AFTER CHECK');
    if (global.AddressAutoCounter >= 1) {
      Geocoder.from(region.latitude, region.longitude)
        .then(json => {
          var addressComponent =
            json.results[json.results.length - 7].formatted_address;
          var addressLine1 =
            json.results[0].address_components[0].short_name +
            ', ' +
            json.results[0].address_components[1].short_name;
          // console.log(json);
          this.setState({
            geoCodeAddress: addressComponent,
            geoCodeAddressLine1: addressLine1,
          });
        })
        .catch(error => console.warn(error));
      this.setState({
        finalPosition: region,
      });
    }
    global.AddressAutoCounter = global.AddressAutoCounter + 1;
  };

  onChangePress = () => {
    this.props.navigation.navigate('AddressHomeMapAuto');
  };

  onFocusInput1 = () => {
    this.setState({
      inputStyleHouse: '#fc2250',
      // focusInputBorderBottom: 'green',
    });
  };
  onFocusInput2 = () => {
    this.setState({
      inputStyleBuilding: '#fc2250',
      // focusInputBorderBottom: 'green',
    });
  };
  onFocusInput3 = () => {
    this.setState({
      inputStyleLandmark: '#fc2250',
      // focusInputBorderBottom: 'green',
    });
  };

  onBlurInput1 = () => {
    this.setState({
      inputStyleHouse: 'gray',
      // focusInputBorderBottom: 'green',
    });
  };
  onBlurInput2 = () => {
    this.setState({
      inputStyleBuilding: 'gray',
      // focusInputBorderBottom: 'green',
    });
  };
  onBlurInput3 = () => {
    this.setState({
      inputStyleLandmark: 'gray',
      // focusInputBorderBottom: 'green',
    });
  };
  onTextChangeLandmark = text => {
    this.setState({
      addressLandmark: text,
    });
  };
  onTextChangeBuildingName = text => {
    this.setState({
      addressBuildingName: text,
    });
  };
  onTextChangeHouseNo = text => {
    this.setState({
      addressHouseNo: text,
    });
  };
  onSubmitButtonPress = () => {
    /* check if navigation is from the Account Tab */
    if (!this.state.addressChangeAllowed) {
      let addressDetails = {
        addressHouseNo: this.state.addressHouseNo.trim(),
        addressBuildingName: this.state.addressBuildingName.trim(),
        addressLandmark: this.state.addressLandmark.trim(),
        finalPosition: this.state.finalPosition,
      };
      this.sendOnWhatsApp(addressDetails);
    } else {
      let userId = AuthService.getCurrentUserUserId();
      if (this.state.deliveryLocation === Constants.ADDRESS_HOME_SCREEN) {
        this.#userService.createHomeAddress(
          userId,
          {
            addressHouseNo: this.state.addressHouseNo.trim(),
            addressBuildingName: this.state.addressBuildingName.trim(),
            addressLandmark: this.state.addressLandmark.trim(),
            finalPosition: this.state.finalPosition,
          },
          result => {
            // console.log(result);
            // console.log('SUCCESS');
            this.props.navigation.goBack();
            // this.props.navigation.navigate('DeliveryPreference', {
            //   addressHouseNo: this.state.addressHouseNo.trim(),
            //   addressBuildingName: this.state.addressBuildingName.trim(),
            //   addressLandmark: this.state.addressLandmark.trim(),
            //   finalPosition: this.state.finalPosition,
            // });
          },
          error => {
            console.error(error);
          },
        );
      }

      if (this.state.deliveryLocation === Constants.ADDRESS_OFFICE_SCREEN) {
        this.#userService.createOfficeAddress(
          userId,
          {
            addressHouseNo: this.state.addressHouseNo,
            addressBuildingName: this.state.addressBuildingName,
            addressLandmark: this.state.addressLandmark,
            finalPosition: this.state.finalPosition,
          },
          result => {
            console.log(result);
            this.props.navigation.goBack();
          },
          error => {
            console.error(error);
          },
        );
      }
      // console.log('submit button');
    }
    // if (this.state.fromScreen === 'manageAddress') {
    // } else {
    // }
  };

  sendOnWhatsApp(addressDetails) {
    let message =
      'Name: ' +
      this.state.userName +
      '\n' +
      this.state.deliveryLocation +
      ' Address Change: ' +
      '\n' +
      addressDetails.addressHouseNo +
      ' ' +
      addressDetails.addressBuildingName +
      ' ' +
      addressDetails.addressLandmark +
      '\n' +
      'Location: ' +
      addressDetails.finalPosition.latitude +
      ', ' +
      addressDetails.finalPosition.latitude;
    let url = 'whatsapp://send?text=' + message + '&phone=91' + '7204630078';
    Linking.openURL(url)
      .then(data => {
        // console.log('WhatsApp Opened');
        this.setState({
          modalVisible: false,
          checkDinner: false,
          checkLunch: false,
          startDate: '',
          endDate: '',
        });
        this.props.navigation.goBack();
      })
      .catch(() => {
        alert('Make sure Whatsapp installed on your device');
      });
  }

  render() {
    return (
      <Container style={{ flex: 1, backgroundColor: 'rgba(255,255,255, 0.1)' }}>
        {/* {CustomHeader('Add Home Address')} */}
        <View style={styles.contentMapStyle}>
          <View style={styles.mapContainer}>
            <MapView
              style={styles.map}
              // customMapStyle={mapStyle}
              ref={map => (this._map = map)}
              // showsUserLocation={true}
              provider={PROVIDER_GOOGLE}
              loadingEnabled
              initialRegion={this.state.initialPosition}
              onRegionChangeComplete={this.onRegionChange}
            />
            {this.state.initialPosition ? (
              <View style={styles.markerFixed}>
                <Image
                  style={styles.marker}
                  source={require('../assets/Images/location-pin-3.png')}
                />
              </View>
            ) : null}
            {!this.state.processing ? (
              <TouchableOpacity
                style={styles.backButton}
                onPress={() => this.backButton()}>
                <MaterialCommunityIcons
                  name="arrow-left-circle"
                  style={{ color: '#000', fontSize: 32 }}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity style={styles.backButton}>
                <MaterialCommunityIcons
                  name="arrow-left-circle"
                  style={{ color: '#000', fontSize: 32 }}
                />
              </TouchableOpacity>
            )}
            {!this.state.processing ? (
              <TouchableOpacity
                style={styles.currentLocationIcon}
                onPress={() => this.markerAnimateToCurrentPosition()}>
                <Icon
                  type="MaterialIcons"
                  name="my-location"
                  style={{ color: '#424242', fontSize: 24 }}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity style={styles.currentLocationIcon}>
                <Icon
                  type="MaterialIcons"
                  name="my-location"
                  style={{ color: '#424242', fontSize: 24 }}
                />
              </TouchableOpacity>
            )}
          </View>
        </View>
        <View style={styles.contentAddressStyle}>
          <ScrollView contentContainerStyle={styles.addressCardView}>
            <View style={styles.addressInputStyle}>
              <View style={styles.addressLineStyle}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <MaterialCommunityIcons
                    name="map-marker-radius"
                    style={{
                      color: '#fc2250',
                      fontSize: 20,
                      marginRight: 12,
                      marginBottom: 5,
                    }}
                  />
                  <Title maxFontSizeMultiplier={1} style={styles.addressLine1}>
                    {this.state.geoCodeAddressLine1}
                  </Title>
                </View>
                <Text
                  maxFontSizeMultiplier={1}
                  numberOfLines={2}
                  style={styles.addressLine2}>
                  {this.state.geoCodeAddress}
                </Text>
              </View>
              <View style={styles.addressChangeButtonView}>
                <Button
                  style={styles.changeButtonStyle}
                  // transparent
                  onPress={() => this.onChangePress()}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.addressButtonText}>
                    CHANGE
                  </Text>
                </Button>
              </View>
            </View>
            {!this.state.serviceAvailable ? (
              <View style={styles.serviceNotAvailableView}>
                <Text
                  maxFontSizeMultiplier={1}
                  style={styles.serviceNotAvailableText}>
                  We are not available at this location yet.
                </Text>
              </View>
            ) : null}
            <View>
              <Form>
                <Item
                  floatingLabel
                  style={[
                    styles.inputStyleFocused,
                    { borderBottomColor: this.state.inputStyleHouse },
                  ]}>
                  <Label
                    maxFontSizeMultiplier={1}
                    style={[
                      styles.inputLabelStyle,
                      { color: this.state.inputStyleHouse },
                    ]}>
                    ** House / Flat / Block No.
                  </Label>
                  <Input
                    disabled={!this.state.serviceAvailable}
                    onFocus={() => this.onFocusInput1()}
                    onBlur={() => this.onBlurInput1()}
                    autoCapitalize="sentences"
                    maxFontSizeMultiplier={1}
                    autoCorrect={false}
                    selectionColor="red"
                    style={styles.nameInputStyle}
                    onChangeText={text => this.onTextChangeHouseNo(text)}
                  />
                </Item>
                <Item
                  floatingLabel
                  style={[
                    styles.inputStyleFocused,
                    { borderBottomColor: this.state.inputStyleBuilding },
                  ]}>
                  <Label
                    maxFontSizeMultiplier={1}
                    style={[
                      styles.inputLabelStyle,
                      { color: this.state.inputStyleBuilding },
                    ]}>
                    ** (Building / Society / Company) Name
                  </Label>
                  <Input
                    disabled={!this.state.serviceAvailable}
                    onFocus={() => this.onFocusInput2()}
                    onBlur={() => this.onBlurInput2()}
                    autoCapitalize="sentences"
                    maxFontSizeMultiplier={1}
                    autoCorrect={false}
                    style={styles.nameInputStyle}
                    onChangeText={text => this.onTextChangeBuildingName(text)}
                  />
                </Item>
                <Item
                  floatingLabel
                  style={[
                    styles.inputStyleFocused,
                    { borderBottomColor: this.state.inputStyleLandmark },
                  ]}>
                  <Label
                    maxFontSizeMultiplier={1}
                    style={[
                      styles.inputLabelStyle,
                      { color: this.state.inputStyleLandmark },
                    ]}>
                    Landmark
                  </Label>
                  <Input
                    disabled={!this.state.serviceAvailable}
                    onFocus={() => this.onFocusInput3()}
                    onBlur={() => this.onBlurInput3()}
                    autoCapitalize="sentences"
                    maxFontSizeMultiplier={1}
                    autoCorrect={false}
                    style={styles.nameInputStyle}
                    onChangeText={text => this.onTextChangeLandmark(text)}
                  />
                </Item>
              </Form>
            </View>
            {this.state.serviceAvailable &&
            this.state.addressHouseNo &&
            this.state.addressBuildingName ? (
              <View style={{ marginTop: 20 }}>
                <Button
                  disabled={!this.state.serviceAvailable}
                  rounded
                  style={styles.continueButton}
                  onPress={() => this.onSubmitButtonPress()}>
                  {!this.state.addressChangeAllowed ? (
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.continueButtonText}>
                      Request Change
                    </Text>
                  ) : (
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.continueButtonText}>
                      Save & Proceed
                    </Text>
                  )}
                </Button>
              </View>
            ) : (
              <View style={{ marginTop: 20 }}>
                <Button rounded disabled style={styles.continueButtonDisabled}>
                  {!this.state.addressChangeAllowed ? (
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.continueButtonText}>
                      Request Change
                    </Text>
                  ) : (
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.continueButtonText}>
                      Save & Proceed
                    </Text>
                  )}
                </Button>
              </View>
            )}
          </ScrollView>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentMapStyle: {
    flex: 2,
  },
  contentAddressStyle: {
    fontFamily: 'Poppins-Regular',
    backgroundColor: '#f5f5f5',
    flex: 3,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderWidth: 1,
    borderColor: '#424242',
  },
  headlineText: {
    fontFamily: 'Poppins-Medium',
    color: '#2a3452',
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  mapContainer: {
    // ...StyleSheet.absoluteFillObject,
    flex: 1,
  },
  map: {
    // height: '100%',
    ...StyleSheet.absoluteFillObject,
  },
  markerFixed: {
    left: '50%',
    marginLeft: -24,
    marginTop: -48,
    position: 'absolute',
    top: '50%',
  },
  marker: {
    height: 48,
    width: 48,
  },
  backButton: {
    top: 0,
    left: 0,
    position: 'absolute',
    margin: 10,
    backgroundColor: 'rgba(255,255,255,0.2)',
    borderRadius: 100,
    padding: 5,
  },
  currentLocationIcon: {
    bottom: 0,
    right: 0,
    position: 'absolute',
    margin: 20,
    backgroundColor: 'rgba(255,255,255,0.2)',
    borderRadius: 100,
    padding: 5,
  },
  addressCardView: {
    marginTop: 12,
    marginHorizontal: 8,
  },
  serviceNotAvailableView: {
    padding: 5,
    backgroundColor: '#f5f5f5',
  },
  serviceNotAvailableText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: 'red',
    marginVertical: 5,
  },
  addressInputStyle: {
    flexDirection: 'row',
    marginTop: 10,
  },
  addressLine1: {
    fontFamily: 'Poppins-SemiBold',
    color: '#2a3452',
    fontSize: 18,
    width: '85%',
  },
  addressLine2: {
    fontFamily: 'Poppins-Light',
    color: '#37474f',
    fontSize: 13,
    width: '85%',
    textAlign: 'left',
    marginTop: 5,
    // marginLeft: 20,
  },
  addressLineStyle: {
    flex: 4,
  },
  addressChangeButtonView: {
    flex: 1,
  },
  changeButtonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5f5f5',
    borderRadius: 10,
    borderColor: '#eeeeee',
    borderWidth: 1,
    width: 80,
    height: 30,
  },
  addressButtonText: {
    fontFamily: 'Poppins-Medium',
    fontSize: 12,
    color: '#fc2250',
    textAlign: 'center',
  },
  inputStyleFocused: {
    marginLeft: 25,
    marginRight: 25,
    // borderBottomColor: 'green',
  },
  inputLabelStyle: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
  },
  nameInputStyle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 12.5,
    // fontWeight: 'bold',
    // height: 50,
    // padding: 0,
  },
  continueButton: {
    backgroundColor: '#fc2250',
    marginBottom: 20,
    alignSelf: 'center',
    marginTop: 40,
  },
  continueButtonText: {
    fontFamily: 'PoppinsMedium-Regular',
    fontSize: 16,
    paddingLeft: 50,
    paddingRight: 50,
    color: '#fff',
  },
  continueButtonDisabled: {
    backgroundColor: '#ECECEC',
    marginBottom: 20,
    alignSelf: 'center',
    marginTop: 40,
  },
});
