import { Component } from 'react';
import { Linking } from 'react-native';
import { render } from '../HomeRegisteredUser/view';
import { UserService } from '../../../Services/User/user.service';
import { ApplicationConstants } from '../../../Services/ApplicationConstants/application.service';
import { AuthService } from '../../../Services/Authentication/auth.service';
import * as Constants from '../../../Services/AppConstants/app.consts';
import Item from '../../../native-base-theme/components/Item';
import UserContext from '../../components/userContext.js';

export default class HomeRegisteredUser extends Component {
  static contextType = UserContext;
  #userService;
  #applicationConstants;
  pricing = {};
  dietEthnicity = {};
  constructor() {
    super();
    this.#userService = new UserService();
    this.#applicationConstants = new ApplicationConstants();
    this.state = {
      dietEth: [],
      modalVisible: false,
      modalTrialVisible: false,
      clickedEthnicity: '',
      clickedEthnicityImageSource: require('../../assets/Images/EthnicityImages/BengaliSampleMenu.jpg'),
      userDetails: '',
    };
  }

  componentDidMount() {
    const user = this.context;
    // console.log('LOG IN COMPONENT');
    // console.log(user);
    this.setState({
      userDetails: user,
    });
    let userId = AuthService.getCurrentUserUserId();
    this.#applicationConstants
      .getApplicationConstants()
      .then(querySnapshot => {
        querySnapshot.forEach(documentSnapshot => {
          switch (documentSnapshot.id) {
            case Constants.PRICE_DETAILS:
              this.pricing = documentSnapshot.data();
              break;
            case Constants.ETHNICITY_DIET:
              this.dietEthnicity = documentSnapshot.data();
              this.setState({
                dietEth: documentSnapshot.data(),
              });
              break;
            default:
              // console.log('Document Id does not match.');
              break;
          }
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  sendOnWhatsApp() {
    let userId = AuthService.getCurrentUserUserId();
    let userIdTrimmed = userId.substring(userId.length - 4);
    let message = '';
    if (this.state.userDetails) {
      message =
        '*Request Two Day Trial*' +
        '\n' +
        'Name: ' +
        this.state.userDetails.name +
        '\n' +
        'CustomerId: ' +
        'xxxxx' +
        userIdTrimmed;
    } else {
      message =
        '*Request Two Day Trial*' +
        '\n' +
        'Name: ' +
        '\n' +
        'CustomerId: ' +
        'xxxxx' +
        userIdTrimmed;
    }
    let url = 'whatsapp://send?text=' + message + '&phone=91' + '7204630078';
    Linking.openURL(url)
      .then(data => {
        this.setState({
          modalTrialVisible: false,
        });
      })
      .catch(() => {
        alert('Make sure Whatsapp installed on your device');
      });
  }

  showSampleMenu(ethnicity) {
    this.selectEthnicityImage('', ethnicity);
    this.setState({
      clickedEthnicity: ethnicity,
      modalVisible: true,
    });
  }

  selectEthnicityImage(ethnicity, selectedEthnicity) {
    var ethnicityImages = [
      {
        name: 'Bengali',
        image: require('../../assets/Images/EthnicityImages/Bengali.jpg'),
        sampleMenuImage: require('../../assets/Images/EthnicityImages/BengaliSampleMenu.jpg'),
      },
      {
        name: 'Nth. Indian',
        image: require('../../assets/Images/EthnicityImages/Nth-Indian.jpg'),
        sampleMenuImage: require('../../assets/Images/EthnicityImages/Nth-IndianSampleMenu.jpg'),
        // sampleMenuImage: require('../../assets/Images/EthnicityImages/BengaliSampleMenu.jpg'),
      },
      {
        name: 'Assamese',
        image: require('../../assets/Images/EthnicityImages/Assamese.jpg'),
        sampleMenuImage: require('../../assets/Images/EthnicityImages/AssameseSampleMenu.jpg'),
        // sampleMenuImage: require('../../assets/Images/EthnicityImages/BengaliSampleMenu.jpg'),
      },
      {
        name: 'Odia',
        image: require('../../assets/Images/EthnicityImages/Oriya.png'),
        sampleMenuImage: require('../../assets/Images/EthnicityImages/OriyaSampleMenu.jpg'),
        // sampleMenuImage: require('../../assets/Images/EthnicityImages/BengaliSampleMenu.jpg'),
      },
      {
        name: 'Andhra',
        image: require('../../assets/Images/EthnicityImages/Bihari.jpg'),
        // sampleMenuImage: require('../../assets/Images/EthnicityImages/BihariSampleMenu.jpg'),
        sampleMenuImage: require('../../assets/Images/EthnicityImages/AndhraSampleMenu.jpg'),
      },
      {
        name: 'Gujarati',
        image: require('../../assets/Images/EthnicityImages/Gujarati.jpg'),
        // sampleMenuImage: require('../../assets/Images/EthnicityImages/GujratiSampleMenu.jpg'),
        sampleMenuImage: require('../../assets/Images/EthnicityImages/BengaliSampleMenu.jpg'),
      },
      {
        name: 'Tamil',
        image: require('../../assets/Images/EthnicityImages/Assamese.jpg'),
        // sampleMenuImage: require('../../assets/Images/EthnicityImages/AssameseSampleMenu.jpg'),
        sampleMenuImage: require('../../assets/Images/EthnicityImages/TamilSampleMenu.jpg'),
      },
      {
        name: 'Kerala',
        image: require('../../assets/Images/EthnicityImages/Assamese.jpg'),
        // sampleMenuImage: require('../../assets/Images/EthnicityImages/AssameseSampleMenu.jpg'),
        sampleMenuImage: require('../../assets/Images/EthnicityImages/KerelaSampleMenu.jpg'),
      },
    ];
    if (ethnicity) {
      let filteredClasses = ethnicityImages.filter(image =>
        ethnicity.includes(image.name),
      );
      if (filteredClasses[0]) {
        return filteredClasses[0].image;
      } else {
        return require('../../assets/Images/EthnicityImages/defaultEthnicity.png');
      }
    } else if (selectedEthnicity) {
      let filteredClasses = ethnicityImages.filter(image =>
        selectedEthnicity.includes(image.name),
      );
      if (filteredClasses[0]) {
        this.setState({
          clickedEthnicityImageSource: filteredClasses[0].sampleMenuImage,
        });
      } else {
        // return require('../../assets/Images/EthnicityImages/defaultEthnicity.png');
        // console.log('NORETURN');
      }
    }
  }
  gotoSubscribeAPlan = userDetails => {
    this.setState({
      modalVisible: false,
    });
    this.props.navigation.push('SubscribeAPlanNavigator', {
      screen: 'SubscribeAPlan',
      params: {
        ethnicity: this.state.clickedEthnicity,
        pricing: this.pricing,
        dietEthnicity: this.dietEthnicity,
        userDetails: userDetails,
      },
    });
  };
  render() {
    const user = this.context;
    return render(this, user);
  }
}
