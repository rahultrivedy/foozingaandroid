import React from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ImageBackground,
  Modal,
  Dimensions,
} from 'react-native';
import { CustomHeader } from '../../components/CustomHeader';
import { Container, Content, Title, Subtitle, Button } from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { styles } from '../HomeRegisteredUser/style';
import LottieView from 'lottie-react-native';
import * as Animatable from 'react-native-animatable';

export function render(viewState, user) {
  return (
    <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
      {CustomHeader(null, null, viewState.props, user.name)}
      <Content contentContainerStyle={styles.contentStyle}>
        {viewState.state.dietEth.Ethnicity ? (
          <View style={styles.contentUsableStyle}>
            <Title maxFontSizeMultiplier={1} style={styles.headlineText}>
              Homefood for the Homesick
            </Title>
            <Text maxFontSizeMultiplier={1} style={styles.subHeadingText}>
              Home cooked food based on your preferred ethnicity
            </Text>
            <View style={styles.ethnicityGrid}>
              {viewState.state.dietEth.Ethnicity.map((item, index) =>
                item.status === 'active' ? (
                  <TouchableOpacity
                    key={'TouchableOpacity' + index}
                    onPress={() => viewState.showSampleMenu(item.name)}>
                    <View key={'view' + index} style={styles.gridShapeStyle}>
                      <Image
                        style={{
                          width: Dimensions.get('window').width / 5,
                          height: Dimensions.get('window').width / 6,
                          flex: 1,
                          borderRadius: 10,
                        }}
                        source={viewState.selectEthnicityImage(item.name, '')}
                      />
                    </View>
                    <Text
                      maxFontSizeMultiplier={1}
                      key={'text' + index}
                      style={styles.gridText}>
                      {item.name}
                    </Text>
                  </TouchableOpacity>
                ) : (
                  <View key={'view' + index}>
                    <View key={index} style={styles.gridShapeStyleDisabled}>
                      <Text
                        maxFontSizeMultiplier={1}
                        key={'text' + index}
                        // numberOfLines={1}
                        style={{
                          ...styles.comingSoonText,
                          // transform: [{ rotate: '-15deg' }],
                        }}>
                        Coming Soon
                      </Text>
                    </View>
                    <Text
                      maxFontSizeMultiplier={1}
                      key={'text2' + index}
                      style={styles.gridText}>
                      {item.name}
                    </Text>
                  </View>
                ),
              )}
            </View>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <Animatable.View
                animation="bounceIn"
                useNativeDriver={true}
                style={styles.centerAddressContent}>
                <ImageBackground
                  source={require('../../assets/Images/bangalore-map-color.jpg')}
                  style={{}}
                  imageStyle={{ borderRadius: 10, width: '95%' }}>
                  <TouchableOpacity
                    style={{
                      // paddingLeft: 20,
                      paddingVertical: 10,
                      flexDirection: 'row',
                      backgroundColor: 'rgba(255, 255, 255, 0.1)',
                    }}
                    onPress={() =>
                      viewState.props.navigation.push('ServiceAreaCheck')
                    }>
                    <View style={{ flexDirection: 'column', marginLeft: 10 }}>
                      <View
                        style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image
                          style={{ width: 30, height: 30 }}
                          source={require('../../assets/Images/location-pin-3.png')}
                        />
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.headlineTextServiceCheck}>
                          Get your meals delivered at home or office
                        </Text>
                      </View>
                      <Text
                        maxFontSizeMultiplier={1}
                        style={styles.subHeadingTextServiceCheck}>
                        Check if we are available at your location
                      </Text>
                    </View>
                    <View style={styles.serviceAreaCheckIconButton}>
                      <MaterialCommunityIcons
                        name="chevron-right"
                        style={styles.serviceAreaCheckIcon}
                      />
                    </View>
                  </TouchableOpacity>
                </ImageBackground>
              </Animatable.View>
            </View>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <View style={styles.referralContent}>
                <View style={{}}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={{ fontSize: 16, ...styles.headlineText }}>
                    Two Day Trial
                  </Text>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.subHeadingTextTrial}>
                    <Text style={{ fontWeight: 'bold' }}>
                      Only for new customers
                    </Text>
                    {'\n'}Connect with us on
                    {'\n'}WhatsApp for a two day trial
                  </Text>
                  <TouchableOpacity
                    onPress={() =>
                      viewState.setState({ modalTrialVisible: true })
                    }>
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.gridTextAction}>
                      Request Trial
                    </Text>
                  </TouchableOpacity>
                </View>
                <Image
                  style={styles.referralContentImage}
                  source={require('../../assets/Images/UnregisteredUserReferal.png')}
                  resizeMode="contain"
                />
              </View>
            </View>
          </View>
        ) : (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {/* <ActivityIndicator small color="#fc2250" /> */}
            <LottieView
              style={{ width: 80, height: 80 }}
              source={require('../../assets/LottieAnimation/foodLoader.json')}
              autoPlay
              loop
            />
            <Text maxFontSizeMultiplier={1} style={styles.loaderText}>
              Bringing delicious homemade food {'\n'}to your HOME and OFFICE
            </Text>
          </View>
        )}
      </Content>
      {/* Modal View showed when clicked on a Diet ethnicity */}
      <Modal
        animationType="slide"
        transparent={false}
        presentationStyle="overFullScreen"
        statusBarTranslucent={false}
        visible={viewState.state.modalVisible}
        onRequestClose={() => {
          viewState.setState({
            modalVisible: false,
          });
        }}>
        <Content
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}>
          <View style={styles.centeredViewSampleMenu}>
            <View style={styles.modalView}>
              <View style={{ alignItems: 'center', flexDirection: 'row' }}>
                <TouchableOpacity
                  // style={{ marginHorizontal: 10 }}
                  // transparent
                  onPress={() => {
                    viewState.setState({ modalVisible: false });
                  }}>
                  <MaterialCommunityIcons
                    name="chevron-left"
                    style={styles.modalBackButton}
                    // size={24}
                  />
                </TouchableOpacity>
                <Text
                  maxFontSizeMultiplier={1}
                  style={{ ...styles.modalTextTitle, marginLeft: 12 }}>
                  {viewState.state.clickedEthnicity}
                </Text>
              </View>
              <Text maxFontSizeMultiplier={1} style={styles.modalTextInfo}>
                Here is a sample menu from one of our premiere{' '}
                {viewState.state.clickedEthnicity} homeChefs.
              </Text>
              <Image
                style={styles.sampleMenuImage}
                source={viewState.state.clickedEthnicityImageSource}
                resizeMode="contain"
              />
              <Text maxFontSizeMultiplier={1} style={styles.modalTextInfo}>
                <Text style={{ fontWeight: 'bold' }}>Note:</Text> This menu
                changes every week. {'\n'}Non-Veg - 6 times a week alternated
                between lunch & dinner
              </Text>
              <Text maxFontSizeMultiplier={1} style={styles.modalTextInfo}>
                ** We are off on Sundays.
              </Text>
              <View>
                <TouchableOpacity
                  style={{ ...styles.selectButtonStyle, marginHorizontal: 10 }}
                  // transparent
                  onPress={() => viewState.gotoSubscribeAPlan(user)}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.textStyleSelect}>
                    Select
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Content>
      </Modal>
      {/* Modal View for Request Trial */}
      <Modal
        animationType="slide"
        transparent={true}
        // presentationStyle="fullScreen"
        statusBarTranslucent={true}
        visible={viewState.state.modalTrialVisible}
        onRequestClose={() => {
          viewState.setState({
            modalTrialVisible: false,
          });
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text maxFontSizeMultiplier={1} style={styles.modalText}>
              Connect over WhatsApp ?
            </Text>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
              <Button
                style={{ marginHorizontal: 10 }}
                transparent
                onPress={() => {
                  viewState.setState({ modalTrialVisible: false });
                }}>
                <Text maxFontSizeMultiplier={1} style={styles.textStyleCancel}>
                  Cancel
                </Text>
              </Button>
              <Button
                style={{ marginHorizontal: 10 }}
                transparent
                onPress={() => {
                  viewState.sendOnWhatsApp();
                }}>
                <Text maxFontSizeMultiplier={1} style={styles.textStyleConnect}>
                  Connect
                </Text>
              </Button>
            </View>
          </View>
        </View>
      </Modal>
    </Container>
  );
}
