import { StyleSheet, Dimensions } from 'react-native';
export const styles = StyleSheet.create({
  contentStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
  },
  contentUsableStyle: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  centerAddressContent: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 16,
    alignItems: 'center',
    borderRadius: 10,
    // borderColor: '#fc2250',
    // borderWidth: 0.5,
    width: '90%',
    // backgroundColor: 'green',
  },
  centerAddressContentImage: {
    // width: Dimensions.get('window').width / 5,
    // height: Dimensions.get('window').width,
    backgroundColor: 'green',
  },
  headlineTextServiceCheck: {
    fontFamily: 'Poppins-Medium',
    color: '#000',
    fontSize: 16,
    width: '85%',
    // backgroundColor: 'rgba(0,0,0, 0.1)',
  },
  subHeadingTextServiceCheck: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#000',
    width: '85%',
    marginLeft: 30,
    // backgroundColor: 'rgba(0,0,0, 0.1)',
  },
  serviceAreaCheckIconButton: {
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: '#fff',
    // marginRight: 0,
    right: 20,
  },
  serviceAreaCheckIcon: {
    color: '#fc2250',
    fontSize: 32,
    backgroundColor: '#fff',
    // marginRight: 10,
    borderColor: '#fc2250',
    borderWidth: 0.5,
  },
  headlineText: {
    fontFamily: 'Poppins-Medium',
    color: '#2a3452',
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  ethnicityGridGroup: {
    justifyContent: 'space-between',
  },
  ethnicityGrid: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 16,
    flexWrap: 'wrap',
    alignItems: 'center',
    alignContent: 'space-around',
    // backgroundColor: 'grey',
  },
  ethnicityGrid2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    flexWrap: 'wrap',
  },
  gridText: {
    fontFamily: 'Poppins-Regular',
    textAlign: 'center',
    color: '#656c82',
  },
  gridShapeStyle: {
    width: Dimensions.get('window').width / 5,
    height: Dimensions.get('window').width / 6,
    backgroundColor: '#b71c1c',
    flexDirection: 'column',
    borderRadius: 10,
    // elevation: 10,
  },
  gridShapeStyleDisabled: {
    width: Dimensions.get('window').width / 5,
    height: Dimensions.get('window').width / 6,
    backgroundColor: '#e3f2fd',
    flexDirection: 'column',
    borderRadius: 10,
  },
  // gridShapeStyle2: {
  //   width: 73,
  //   height: 72,
  //   flexDirection: 'column',
  //   borderRadius: 10,
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   marginTop: 10,
  //   // backgroundColor: 'green',
  // },
  gridIconStyle: {
    width: 50,
    height: 50,
    borderRadius: 50,
    backgroundColor: '#eef2f5',
    alignItems: 'center',
    justifyContent: 'center',
  },
  gridTextAction: {
    fontFamily: 'Poppins-Regular',
    color: '#fc2250',
  },
  comingSoonText: {
    position: 'absolute',
    bottom: 15,
    backgroundColor: '#f5f5f5',
    color: '#616161',
    textAlign: 'center',
  },
  referralContent: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 16,
  },
  subHeadingTextTrial: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  referralContentImage: {
    height: 129,
    width: 168,
    paddingLeft: 20,
  },
  sampleMenuImage: {
    height: 420,
    width: 320,
    alignSelf: 'center',
  },
  loaderText: {
    color: '#fc2250',
    marginTop: 50,
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    textAlign: 'center',
  },
  //Modal
  centeredViewSampleMenu: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    // marginTop: 50,
    backgroundColor: 'rgba(0,0,0,0.7)',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
    backgroundColor: 'rgba(0,0,0,0.7)',
  },
  modalView: {
    backgroundColor: '#fff',
    // borderRadius: 10,
    padding: 25,
    paddingBottom: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  },
  modalBackButton: {
    color: '#000',
    fontSize: 24,
    backgroundColor: '#fff',
    // marginRight: 10,
    borderColor: '#000',
    borderWidth: 0.5,
  },
  modalTextTitle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 24,
    marginTop: 8,
    // marginBottom: 12,
    color: '#2a3452',
  },
  modalTextInfo: {
    fontFamily: 'Poppins-Regular',
    marginBottom: 15,
    // textAlign: 'center',
  },
  modalText: {
    fontFamily: 'Poppins-Regular',
    marginBottom: 15,
    textAlign: 'center',
  },
  textStyleConnect: {
    fontFamily: 'Poppins-Regular',
    fontSize: 18,
    color: '#fc2250',
  },
  textStyleSelect: {
    flex: 1,
    fontFamily: 'Poppins-Regular',
    fontSize: 18,
    color: '#fff',
    textAlign: 'center',
  },
  textStyleCancel: {
    fontFamily: 'Poppins-Regular',
    fontSize: 18,
    color: '#fc2250',
  },
  selectButtonStyle: {
    flex: 1,
    paddingHorizontal: 56,
    paddingVertical: 6,
    borderRadius: 36,
    backgroundColor: '#fc2250',
  },
  // Modal END
});
