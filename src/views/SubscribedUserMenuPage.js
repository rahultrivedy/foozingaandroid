import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  ScrollView,
  Image,
  ImageBackground,
} from 'react-native';
import { CustomHeader } from '../components/CustomHeader';
import { Container, Content, Text, StyleProvider } from 'native-base';
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';

/* Menu Page displayed above slide menu once user has subscribed */
export default class SubscribedUserMenuPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  // componentDidMount() {
  //   console.log(this.props.route.params.cookMenuDetails);
  // }

  render() {
    return (
      <StyleProvider style={getTheme(material)}>
        <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
          {CustomHeader('Weekly Menu', 1, this.props)}
          <Content contentContainerStyle={styles.contentStyle}>
            <View style={styles.contentUsableStyle}>
              <View style={{ flex: 1 }}>
                <View style={styles.updateCardImageViewStyle}>
                  <Text maxFontSizeMultiplier={1} style={styles.headlineText}>
                    Menu for the Week
                  </Text>
                </View>
                <View style={styles.timeUpdateStatusStyle}>
                  <Text maxFontSizeMultiplier={1} style={styles.subHeadingText}>
                    The menu is updated at the start of every week and is
                    provided by your home chef.
                  </Text>
                </View>
                {this.props.route.params.cookMenuDetails ? (
                  <View>
                    <View style={styles.menuGrid}>
                      <Text
                        maxFontSizeMultiplier={1}
                        style={styles.dayHeadlineText}>
                        Monday
                      </Text>
                      <View style={styles.gridContainer}>
                        <View style={styles.gridShapeStyle}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.menuListHeader}>
                            Lunch
                          </Text>
                          <FlatList
                            data={
                              this.props.route.params.cookMenuDetails.Monday
                                .Lunch
                            }
                            renderItem={({ item }) => (
                              <Text
                                maxFontSizeMultiplier={1}
                                style={styles.listItemStyle}>
                                {item}
                              </Text>
                            )}
                            keyExtractor={item => item}
                          />
                        </View>
                        <View style={styles.gridShapeStyle}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.menuListHeader}>
                            Dinner
                          </Text>
                          <FlatList
                            data={
                              this.props.route.params.cookMenuDetails.Monday
                                .Dinner
                            }
                            renderItem={({ item }) => (
                              <Text
                                maxFontSizeMultiplier={1}
                                style={styles.listItemStyle}>
                                {item}
                              </Text>
                            )}
                            keyExtractor={item => item}
                          />
                        </View>
                      </View>
                    </View>
                    <View style={styles.menuGrid}>
                      <Text
                        maxFontSizeMultiplier={1}
                        style={styles.dayHeadlineText}>
                        Tuesday
                      </Text>
                      <View style={styles.gridContainer}>
                        <View style={styles.gridShapeStyle}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.menuListHeader}>
                            Lunch
                          </Text>
                          <FlatList
                            data={
                              this.props.route.params.cookMenuDetails.Tuesday
                                .Lunch
                            }
                            renderItem={({ item }) => (
                              <Text
                                maxFontSizeMultiplier={1}
                                style={styles.listItemStyle}>
                                {item}
                              </Text>
                            )}
                            keyExtractor={item => item}
                          />
                        </View>
                        <View style={styles.gridShapeStyle}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.menuListHeader}>
                            Dinner
                          </Text>
                          <FlatList
                            data={
                              this.props.route.params.cookMenuDetails.Tuesday
                                .Dinner
                            }
                            renderItem={({ item }) => (
                              <Text
                                maxFontSizeMultiplier={1}
                                style={styles.listItemStyle}>
                                {item}
                              </Text>
                            )}
                            keyExtractor={item => item}
                          />
                        </View>
                      </View>
                    </View>
                    <View style={styles.menuGrid}>
                      <Text
                        maxFontSizeMultiplier={1}
                        style={styles.dayHeadlineText}>
                        Wednesday
                      </Text>
                      <View style={styles.gridContainer}>
                        <View style={styles.gridShapeStyle}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.menuListHeader}>
                            Lunch
                          </Text>
                          <FlatList
                            data={
                              this.props.route.params.cookMenuDetails.Wednesday
                                .Lunch
                            }
                            renderItem={({ item }) => (
                              <Text
                                maxFontSizeMultiplier={1}
                                style={styles.listItemStyle}>
                                {item}
                              </Text>
                            )}
                            keyExtractor={item => item}
                          />
                        </View>
                        <View style={styles.gridShapeStyle}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.menuListHeader}>
                            Dinner
                          </Text>
                          <FlatList
                            data={
                              this.props.route.params.cookMenuDetails.Wednesday
                                .Dinner
                            }
                            renderItem={({ item }) => (
                              <Text
                                maxFontSizeMultiplier={1}
                                style={styles.listItemStyle}>
                                {item}
                              </Text>
                            )}
                            keyExtractor={item => item}
                          />
                        </View>
                      </View>
                    </View>
                    <View style={styles.menuGrid}>
                      <Text
                        maxFontSizeMultiplier={1}
                        style={styles.dayHeadlineText}>
                        Thursday
                      </Text>
                      <View style={styles.gridContainer}>
                        <View style={styles.gridShapeStyle}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.menuListHeader}>
                            Lunch
                          </Text>
                          <FlatList
                            data={
                              this.props.route.params.cookMenuDetails.Thursday
                                .Lunch
                            }
                            renderItem={({ item }) => (
                              <Text
                                maxFontSizeMultiplier={1}
                                style={styles.listItemStyle}>
                                {item}
                              </Text>
                            )}
                            keyExtractor={item => item}
                          />
                        </View>
                        <View style={styles.gridShapeStyle}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.menuListHeader}>
                            Dinner
                          </Text>
                          <FlatList
                            data={
                              this.props.route.params.cookMenuDetails.Thursday
                                .Dinner
                            }
                            renderItem={({ item }) => (
                              <Text
                                maxFontSizeMultiplier={1}
                                style={styles.listItemStyle}>
                                {item}
                              </Text>
                            )}
                            keyExtractor={item => item}
                          />
                        </View>
                      </View>
                    </View>
                    <View style={styles.menuGrid}>
                      <Text
                        maxFontSizeMultiplier={1}
                        style={styles.dayHeadlineText}>
                        Friday
                      </Text>
                      <View style={styles.gridContainer}>
                        <View style={styles.gridShapeStyle}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.menuListHeader}>
                            Lunch
                          </Text>
                          <FlatList
                            data={
                              this.props.route.params.cookMenuDetails.Friday
                                .Lunch
                            }
                            renderItem={({ item }) => (
                              <Text
                                maxFontSizeMultiplier={1}
                                style={styles.listItemStyle}>
                                {item}
                              </Text>
                            )}
                            keyExtractor={item => item}
                          />
                        </View>
                        <View style={styles.gridShapeStyle}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.menuListHeader}>
                            Dinner
                          </Text>
                          <FlatList
                            data={
                              this.props.route.params.cookMenuDetails.Friday
                                .Dinner
                            }
                            renderItem={({ item }) => (
                              <Text
                                maxFontSizeMultiplier={1}
                                style={styles.listItemStyle}>
                                {item}
                              </Text>
                            )}
                            keyExtractor={item => item}
                          />
                        </View>
                      </View>
                    </View>
                    <View style={styles.menuGrid}>
                      <Text
                        maxFontSizeMultiplier={1}
                        style={styles.dayHeadlineText}>
                        Saturday
                      </Text>
                      <View style={styles.gridContainer}>
                        <View style={styles.gridShapeStyle}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.menuListHeader}>
                            Lunch
                          </Text>
                          <FlatList
                            data={
                              this.props.route.params.cookMenuDetails.Saturday
                                .Lunch
                            }
                            renderItem={({ item }) => (
                              <Text
                                maxFontSizeMultiplier={1}
                                style={styles.listItemStyle}>
                                {item}
                              </Text>
                            )}
                            keyExtractor={item => item}
                          />
                        </View>
                        <View style={styles.gridShapeStyle}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.menuListHeader}>
                            Dinner
                          </Text>
                          <FlatList
                            data={
                              this.props.route.params.cookMenuDetails.Saturday
                                .Dinner
                            }
                            renderItem={({ item }) => (
                              <Text
                                maxFontSizeMultiplier={1}
                                style={styles.listItemStyle}>
                                {item}
                              </Text>
                            )}
                            keyExtractor={item => item}
                          />
                        </View>
                      </View>
                    </View>
                  </View>
                ) : null}
              </View>
              <View style={styles.infoTextView}>
                <Text maxFontSizeMultiplier={1} style={styles.infoText}>
                  ** We are off on Sundays **
                </Text>
              </View>
            </View>
          </Content>
        </Container>
      </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  contentStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
  },
  contentUsableStyle: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
  },
  headlineText: {
    fontFamily: 'Poppins-Medium',
    color: '#131e40',
    fontSize: 18,
    textAlign: 'center',

    // textTransform: 'capitalize',
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
    textAlign: 'center',
  },
  continueButton: {
    backgroundColor: '#fc2250',
    marginBottom: 40,
    alignSelf: 'center',
    // marginTop: 30,
  },
  continueButtonText: {
    paddingLeft: 50,
    paddingRight: 50,
    color: '#fff',
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
  },
  dayHeadlineText: {
    fontFamily: 'Poppins-Medium',
    color: '#fc2250',
    fontSize: 18,
    // textAlign: 'center',
  },
  menuGrid: {
    marginTop: 24,
  },
  gridContainer: {
    flexDirection: 'row',
    marginTop: 12,
    justifyContent: 'space-around',
  },
  gridShapeStyle: {
    width: '48%',
    // height: 62,
    backgroundColor: '#f5f5f5',
    flexDirection: 'column',
    borderRadius: 10,
  },
  menuListHeader: {
    // backgroundColor: 'rgba(0,255,255,0.2)',
    backgroundColor: '#e0e0e0',
    marginVertical: 10,
    textAlign: 'center',
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    color: '#1565c0',
  },
  gridText: {
    fontFamily: 'Poppins-Regular',
    // textAlign: 'center',
    color: '#000',
  },
  listItemStyle: {
    marginTop: 5,
    marginLeft: 10,
    color: '#000',
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
  },
  infoTextView: {
    marginVertical: 30,
  },
  infoText: {
    fontFamily: 'Poppins-Regular',
    textAlign: 'center',
    color: '#131e40',
  },
  // UPDATE CARD ON TOP
  updateCardContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  updateCardStyle: {
    width: 340,
    height: 96,
    borderRadius: 6,
    backgroundColor: '#f8f8f8',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  updateCardImageViewStyle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  updateCardImageStyle: {
    height: 60,
    width: 60,
  },
  updateActiveText: {
    marginLeft: 16,
    flex: 1,
    justifyContent: 'space-evenly',
  },
});
