import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Linking,
} from 'react-native';
import { CustomHeader } from '../../components/CustomHeader';
import { Container, Title, Button, Content } from 'native-base';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';

export default class HelpAndFaqLegal extends Component {
  constructor() {
    super();
  }
  state = {};

  componentDidMount() {}

  onPressTnC = () => {
    Linking.openURL('https://foozinga.com/terms-and-conditions/').catch(err =>
      console.error('An error occurred', err),
    );
  };
  onPressPrivacyPolicy = () => {
    Linking.openURL('https://foozinga.com/privacy-policy/').catch(err =>
      console.error('An error occurred', err),
    );
  };
  onPressRefundsAndCancellations = () => {
    Linking.openURL('https://foozinga.com/refunds-and-cancellations/').catch(
      err => console.error('An error occurred', err),
    );
  };

  render() {
    return (
      <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
        {CustomHeader('Legal', true, this.props)}
        <Content contentContainerStyle={styles.contentStyle}>
          <View style={styles.contentUsableStyle}>
            <View style={styles.contentUsableStyle2}>
              {/* ListItem 1 */}
              <TouchableOpacity
                style={styles.listViewStyle}
                onPress={() => {
                  this.onPressTnC();
                }}>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                  }}>
                  <Text maxFontSizeMultiplier={1} style={styles.listItemTitle}>
                    Terms & Conditions
                  </Text>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.listItemSubtitle}>
                    App terms and conditions - Redirects to web
                  </Text>
                </View>
                <MaterialCommunityIcons
                  name="chevron-right"
                  style={{
                    fontSize: 24,
                    color: '#632dc2',
                    alignSelf: 'center',
                  }}
                />
              </TouchableOpacity>
              {/* DIVIDER */}
              <View style={styles.dividerLine} />
              {/* ListItem 2 */}
              <TouchableOpacity
                style={styles.listViewStyle}
                onPress={() => {
                  this.onPressPrivacyPolicy();
                }}>
                <View style={{ flex: 1, flexDirection: 'column' }}>
                  <Text maxFontSizeMultiplier={1} style={styles.listItemTitle}>
                    Privacy Policy
                  </Text>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.listItemSubtitle}>
                    Customer's privacy policy - Redirects to web
                  </Text>
                </View>
                <MaterialCommunityIcons
                  name="chevron-right"
                  style={{
                    fontSize: 24,
                    color: '#632dc2',
                    alignSelf: 'center',
                  }}
                />
              </TouchableOpacity>
              {/* DIVIDER */}
              <View style={styles.dividerLine} />

              <TouchableOpacity
                style={styles.listViewStyle}
                onPress={() => {
                  this.onPressRefundsAndCancellations();
                }}>
                <View style={{ flex: 1, flexDirection: 'column' }}>
                  <Text maxFontSizeMultiplier={1} style={styles.listItemTitle}>
                    Refunds & Cancellations
                  </Text>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.listItemSubtitle}>
                    Refunds and cancellations - Redirects to web
                  </Text>
                </View>
                <MaterialCommunityIcons
                  name="chevron-right"
                  style={{
                    fontSize: 24,
                    color: '#632dc2',
                    alignSelf: 'center',
                  }}
                />
              </TouchableOpacity>
              {/* DIVIDER */}
              <View style={styles.dividerLine} />
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
  },
  contentUsableStyle: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
  },
  contentUsableStyle2: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
    marginBottom: 60,
  },
  headlineText: {
    fontFamily: 'PoppinsMedium',
    color: '#131e40',
    fontSize: 18,
    textTransform: 'capitalize',
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  listViewStyle: {
    marginVertical: 2,
    flexDirection: 'row',
  },
  listItemTitle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
  },
  listItemSubtitle: {
    fontFamily: 'Poppins-ExtraLight',
  },
  dividerLine: {
    marginVertical: 12,
    marginHorizontal: 12,
    height: 0,
    borderStyle: 'solid',
    borderWidth: 0.7,
    borderColor: '#d8dadf',
  },
});
