import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import { CustomHeader } from '../../components/CustomHeader';
import { Container, Title, Button, Content } from 'native-base';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { ApplicationConstants } from '../../../Services/ApplicationConstants/application.service';

export default class generalFAQs extends Component {
  #applicationConstants;
  constructor() {
    super();
    this.state = {
      faqList: '',
      answer1: false,
      answer2: false,
    };
    this.#applicationConstants = new ApplicationConstants();
  }

  componentDidMount() {
    this.#applicationConstants.getFAQs().then(documentSnapshot => {
      // console.log(documentSnapshot.data());
      let faqArray = [];
      let i = 0;
      for (var key in documentSnapshot.data()) {
        if (documentSnapshot.data().hasOwnProperty(key)) {
          var val = documentSnapshot.data()[key];
          val.key = i;
          faqArray.push(val);
          i++;
        }
      }
      // console.log(faqArray);
      this.setState({
        faqList: faqArray,
      });
    });
  }

  onPressQuestions = item => {
    // console.log('ITEM');
    // console.log(item);
    if (this.state.selectedQuestion) {
      console.log(this.state.selectedQuestion);
      if (item.key !== this.state.selectedQuestion.key) {
        // console.log('DIFF KEY');
        this.setState({
          selectedQuestion: item,
        });
      } else {
        // console.log('SAME KEY');
        this.setState({
          selectedQuestion: '',
        });
      }
    } else {
      // console.log('NEW KEY');
      this.setState({
        selectedQuestion: item,
      });
    }
  };

  render() {
    return (
      <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
        {CustomHeader('General FAQs', true, this.props)}
        <View style={styles.contentStyle}>
          <View style={styles.contentUsableStyle}>
            <View style={styles.contentUsableStyle2}>
              {this.state.faqList ? (
                <View>
                  <FlatList
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    data={this.state.faqList}
                    renderItem={({ item }) => (
                      <View>
                        {item.question ? (
                          <View>
                            <TouchableOpacity
                              style={styles.listViewStyle}
                              onPress={() => {
                                this.onPressQuestions(item);
                              }}>
                              <View
                                style={{
                                  flex: 1,
                                  flexDirection: 'column',
                                  justifyContent: 'center',
                                }}>
                                <Text
                                  maxFontSizeMultiplier={1}
                                  style={styles.questionTextStyle}>
                                  {item.question}{' '}
                                </Text>
                              </View>
                              {this.state.selectedQuestion ? (
                                <View>
                                  {this.state.selectedQuestion.question ===
                                  item.question ? (
                                    <MaterialCommunityIcons
                                      name="chevron-up"
                                      style={{
                                        fontSize: 24,
                                        color: '#632dc2',
                                        alignSelf: 'center',
                                      }}
                                    />
                                  ) : (
                                    <MaterialCommunityIcons
                                      name="chevron-down"
                                      style={{
                                        fontSize: 24,
                                        color: '#632dc2',
                                        alignSelf: 'center',
                                      }}
                                    />
                                  )}
                                </View>
                              ) : (
                                <MaterialCommunityIcons
                                  name="chevron-down"
                                  style={{
                                    fontSize: 24,
                                    color: '#632dc2',
                                    alignSelf: 'center',
                                  }}
                                />
                              )}
                            </TouchableOpacity>
                            {this.state.selectedQuestion ? (
                              <View>
                                <TouchableOpacity
                                  style={styles.listViewStyle}
                                  onPress={() => {
                                    this.onPressQuestions(item);
                                  }}>
                                  {this.state.selectedQuestion.question ===
                                  item.question ? (
                                    <View style={styles.answerStyle}>
                                      <Text
                                        maxFontSizeMultiplier={1}
                                        style={styles.answerTextStyle}>
                                        {item.answer}
                                      </Text>
                                    </View>
                                  ) : null}
                                </TouchableOpacity>
                              </View>
                            ) : null}
                            <View style={styles.dividerLine} />
                          </View>
                        ) : null}
                      </View>
                    )}
                    keyExtractor={item => item.key.toString()}
                  />
                </View>
              ) : null}
            </View>
          </View>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
  },
  contentUsableStyle: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
  },
  contentUsableStyle2: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
    marginBottom: 60,
  },
  headlineText: {
    fontFamily: 'PoppinsMedium',
    color: '#131e40',
    fontSize: 18,
    textTransform: 'capitalize',
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  listViewStyle: {
    marginVertical: 2,
    flexDirection: 'row',
  },
  listItemTitle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
  },
  listItemSubtitle: {
    fontFamily: 'Poppins-ExtraLight',
  },
  questionTextStyle: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
  },
  answerTextStyle: {
    fontFamily: 'Poppins-Light',
    fontSize: 14,
  },
  dividerLine: {
    marginVertical: 12,
    marginHorizontal: 12,
    height: 0,
    borderStyle: 'solid',
    borderWidth: 0.7,
    borderColor: '#d8dadf',
  },
});
