import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { CustomHeader } from '../../components/CustomHeader';
import { Container, Title, Button, Content } from 'native-base';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';

export default class HelpAndFaq extends Component {
  constructor() {
    super();
  }
  state = {};

  componentDidMount() {
    this.setState({
      userDetails: this.props.route.params.userDetails,
    });
  }

  render() {
    return (
      <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
        {CustomHeader('Help & FAQs', true, this.props)}
        <Content contentContainerStyle={styles.contentStyle}>
          <View style={styles.contentUsableStyle}>
            <View style={styles.contentUsableStyle2}>
              {/* ListItem 1 */}
              <TouchableOpacity
                style={styles.listViewStyle}
                onPress={() => {
                  this.props.navigation.navigate('generalFAQs');
                }}>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                  }}>
                  <Text maxFontSizeMultiplier={1} style={styles.listItemTitle}>
                    General FAQ's
                  </Text>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.listItemSubtitle}>
                    Commonly asked questions - answered
                  </Text>
                </View>
                <MaterialCommunityIcons
                  name="chevron-right"
                  style={{
                    fontSize: 24,
                    color: '#632dc2',
                    alignSelf: 'center',
                  }}
                />
              </TouchableOpacity>
              {/* DIVIDER */}
              <View style={styles.dividerLine} />
              {/* ListItem 2 */}
              <TouchableOpacity
                style={styles.listViewStyle}
                onPress={() => {
                  this.props.navigation.navigate('HelpAndFaqLegal');
                }}>
                <View style={{ flex: 1, flexDirection: 'column' }}>
                  <Text maxFontSizeMultiplier={1} style={styles.listItemTitle}>
                    Legal
                  </Text>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.listItemSubtitle}>
                    Terms & Conditions, Privacy Policy, Refunds & Cancellations
                  </Text>
                </View>
                <MaterialCommunityIcons
                  name="chevron-right"
                  style={{
                    fontSize: 24,
                    color: '#632dc2',
                    alignSelf: 'center',
                  }}
                />
              </TouchableOpacity>
              {/* DIVIDER */}
              <View style={styles.dividerLine} />

              <TouchableOpacity
                style={styles.listViewStyle}
                onPress={() => {
                  this.props.navigation.navigate('HelpAndSupport', {
                    userDetails: this.state.userDetails,
                  });
                }}>
                <View style={{ flex: 1, flexDirection: 'column' }}>
                  <Text maxFontSizeMultiplier={1} style={styles.listItemTitle}>
                    Help & Support
                  </Text>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.listItemSubtitle}>
                    Connect with us for help with your account or subscription
                  </Text>
                </View>
                <MaterialCommunityIcons
                  name="chevron-right"
                  style={{
                    fontSize: 24,
                    color: '#632dc2',
                    alignSelf: 'center',
                  }}
                />
              </TouchableOpacity>
              {/* DIVIDER */}
              <View style={styles.dividerLine} />

              <TouchableOpacity
                style={styles.listViewStyle}
                onPress={() => {
                  this.props.navigation.navigate('AppIssue', {
                    userDetails: this.state.userDetails,
                  });
                }}>
                <View style={{ flex: 1, flexDirection: 'column' }}>
                  <Text maxFontSizeMultiplier={1} style={styles.listItemTitle}>
                    Report an App Issue
                  </Text>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.listItemSubtitle}>
                    Report an issue with the app
                  </Text>
                </View>
                <MaterialCommunityIcons
                  name="chevron-right"
                  style={{
                    fontSize: 24,
                    color: '#632dc2',
                    alignSelf: 'center',
                  }}
                />
              </TouchableOpacity>
              {/* DIVIDER */}
              <View style={styles.dividerLine} />

              <TouchableOpacity
                style={styles.listViewStyle}
                onPress={() => {
                  this.props.navigation.navigate('FeedbackSuggestion', {
                    userDetails: this.state.userDetails,
                  });
                }}>
                <View style={{ flex: 1, flexDirection: 'column' }}>
                  <Text maxFontSizeMultiplier={1} style={styles.listItemTitle}>
                    Feedback & Suggestion
                  </Text>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.listItemSubtitle}>
                    Feature suggestion or general feedback about the app
                  </Text>
                </View>
                <MaterialCommunityIcons
                  name="chevron-right"
                  style={{
                    fontSize: 24,
                    color: '#632dc2',
                    alignSelf: 'center',
                  }}
                />
              </TouchableOpacity>
              {/* DIVIDER */}
              <View style={styles.dividerLine} />

              {/* <TouchableOpacity
                style={styles.listViewStyle}
                // onPress={() => {
                //   this.props.navigation.navigate('HelpAndFaq');
                // }}
              >
                <View
                  maxFontSizeMultiplier={1}
                  style={{ flex: 1, flexDirection: 'column' }}>
                  <Text maxFontSizeMultiplier={1} style={styles.listItemTitle}>
                    Our Socials
                  </Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.listItemSubtitle}>
                      Follow us on our socials & Cheer us on{' '}
                    </Text>
                    <MaterialCommunityIcons
                      name="heart"
                      style={{
                        fontSize: 18,
                        color: '#c62828',
                      }}
                    />
                  </View>
                </View>
                <MaterialCommunityIcons
                  name="chevron-right"
                  style={{
                    fontSize: 24,
                    color: '#632dc2',
                    alignSelf: 'center',
                  }}
                />
              </TouchableOpacity> */}
              {/* DIVIDER */}
              {/* <View style={styles.dividerLine} /> */}
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
  },
  contentUsableStyle: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
  },
  contentUsableStyle2: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
    marginBottom: 60,
  },
  headlineText: {
    fontFamily: 'PoppinsMedium',
    color: '#131e40',
    fontSize: 18,
    textTransform: 'capitalize',
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  listViewStyle: {
    marginVertical: 2,
    flexDirection: 'row',
  },
  listItemTitle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
  },
  listItemSubtitle: {
    fontFamily: 'Poppins-ExtraLight',
    alignItems: 'center',
  },
  dividerLine: {
    marginVertical: 12,
    marginHorizontal: 12,
    height: 0,
    borderStyle: 'solid',
    borderWidth: 0.7,
    borderColor: '#d8dadf',
  },
});
