import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Modal,
  Linking,
  ToastAndroid,
} from 'react-native';
import { CustomHeader } from '../../components/CustomHeader';
import {
  Container,
  Title,
  Button,
  Content,
  Picker,
  Textarea,
  Form,
  Radio,
} from 'native-base';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { AuthService } from '../../../Services/Authentication/auth.service';

export default class AppIssue extends Component {
  constructor() {
    super();
    this.state = {
      selectedCritical: false,
      selectedMedium: false,
      selectedLow: false,
      issueDescription: '',
      userDetails: '',
      modalVisible: false,
    };
  }

  componentDidMount() {
    // let userId = AuthService.getCurrentUserUserId();
    // console.log(this.props.route.params.userDetails);
    this.setState({
      userDetails: this.props.route.params.userDetails,
    });
  }

  criticalRadioClicked = () => {
    this.setState({
      selectedCritical: true,
      selectedMedium: false,
      selectedLow: false,
    });
  };

  mediumRadioClicked = () => {
    this.setState({
      selectedCritical: false,
      selectedMedium: true,
      selectedLow: false,
    });
  };

  lowRadioClicked = () => {
    this.setState({
      selectedCritical: false,
      selectedMedium: false,
      selectedLow: true,
    });
  };

  checkFormValidity() {
    let message1 = 'Please select the Severity before submitting the issue';
    let message2 = 'Write detailed description of the issue before submitting.';
    if (!this.state.issueDescription) {
      this.showToastWithGravity(message2);
    } else if (
      !(
        this.state.selectedCritical ||
        this.state.selectedMedium ||
        this.state.selectedLow
      )
    ) {
      this.showToastWithGravity(message1);
    } else {
      this.setState({
        modalVisible: true,
      });
    }
  }

  sendOnWhatsApp() {
    let severity = '';
    if (this.state.selectedCritical) {
      severity = 'Critical';
    } else if (this.state.selectedMedium) {
      severity = 'Medium';
    } else {
      severity = 'Low';
    }
    let userId = AuthService.getCurrentUserUserId();
    let userIdTrimmed = userId.substring(userId.length - 4);
    let message =
      'Name: ' +
      this.state.userDetails.name +
      '\n' +
      'CustomerId: ' +
      'xxxxx' +
      userIdTrimmed +
      '\n' +
      'Severity: ' +
      severity +
      '\n' +
      'Issue: ' +
      this.state.issueDescription;
    let url = 'whatsapp://send?text=' + message + '&phone=91' + '7204630078';
    Linking.openURL(url)
      .then(data => {
        // console.log('WhatsApp Opened');
        this.setState({
          modalVisible: false,
          selectedCritical: false,
          selectedMedium: false,
          selectedLow: false,
          issueDescription: '',
        });
        this.props.navigation.navigate('Home');
      })
      .catch(() => {
        alert('Make sure Whatsapp installed on your device');
      });
  }

  showToastWithGravity = message => {
    ToastAndroid.showWithGravity(
      message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
    );
  };

  render() {
    return (
      <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
        {CustomHeader('Help & Support', true, this.props)}
        <Content contentContainerStyle={styles.contentStyle}>
          <View style={styles.contentUsableStyle}>
            <Text maxFontSizeMultiplier={1} style={styles.headlineText}>
              Connect with us for support
            </Text>
            <View style={styles.contentUsableStyle2}>
              <Text maxFontSizeMultiplier={1} style={styles.subHeadingText}>
                * You are using a Beta version of the app. {'\n'} Hence you may
                find issues in the app that need to be taken care of. {'\n'}*
                The issues can be categorised as :{'\n'}
                {'\n'}'CRITICAL' - cannot use the app at all,{'\n'}'MEDIUM' -
                App is usable but issue to be taken care of as soon as possible,
                {'\n'}'LOW' - Most of the features are usable but there are
                there are minor issues.
              </Text>
              <Text
                maxFontSizeMultiplier={1}
                style={{ marginTop: 8, ...styles.subHeadingText }}>
                ** The app is in BETA. The issue will be communicated through
                WhatsApp. We are working towards giving you the best app
                experience.
              </Text>
            </View>
          </View>
          <View style={styles.contentUsableStyle2}>
            <View>
              <Text
                maxFontSizeMultiplier={1}
                style={{ flex: 1, ...styles.headlineText }}>
                Severity :{' '}
              </Text>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-evenly',
                }}>
                <TouchableOpacity
                  style={styles.categoryViewStyle}
                  onPress={() => this.criticalRadioClicked()}>
                  <Text maxFontSizeMultiplier={1}>Critical</Text>
                  <Radio
                    // note
                    onPress={() => this.criticalRadioClicked()}
                    color={'#b71c1c'}
                    selectedColor={'#b71c1c'}
                    style={{ flex: 1, marginLeft: 16 }}
                    selected={this.state.selectedCritical}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.categoryViewStyle}
                  onPress={() => this.mediumRadioClicked()}>
                  <Text maxFontSizeMultiplier={1}>Medium</Text>
                  <Radio
                    // note
                    onPress={() => this.mediumRadioClicked()}
                    color={'#ef6c00'}
                    selectedColor={'#ef6c00'}
                    style={{ flex: 1, marginLeft: 16 }}
                    selected={this.state.selectedMedium}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.categoryViewStyle}
                  onPress={() => this.lowRadioClicked()}>
                  <Text maxFontSizeMultiplier={1}>Low</Text>
                  <Radio
                    // note
                    onPress={() => this.lowRadioClicked()}
                    color={'#455a64'}
                    selectedColor={'#455a64'}
                    style={{ flex: 1, marginLeft: 16 }}
                    selected={this.state.selectedLow}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ marginTop: 16 }}>
              <Text
                maxFontSizeMultiplier={1}
                style={{ flex: 1, ...styles.headlineText }}>
                Issue :{' '}
              </Text>
              <Textarea
                // note
                maxFontSizeMultiplier={1}
                rowSpan={5}
                bordered
                placeholder="Describe your issue here"
                mode="dropdown"
                style={{ marginTop: 16 }}
                onChangeText={text => this.setState({ issueDescription: text })}
              />
            </View>
            <View style={styles.pauseButtonView}>
              <Button
                rounded
                style={styles.continueButton}
                onPress={() => {
                  this.checkFormValidity();
                }}>
                <Text
                  maxFontSizeMultiplier={1}
                  style={styles.continueButtonText}>
                  Submit Issue
                </Text>
              </Button>
            </View>
          </View>
        </Content>
        {/* Modal to connect to WhatsApp  */}
        <Modal
          animationType="slide"
          transparent={true}
          // presentationStyle="fullScreen"
          statusBarTranslucent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            // Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text maxFontSizeMultiplier={1} style={styles.modalText}>
                Submit request through WhatsApp ?
              </Text>
              <View
                style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                <Button
                  style={{ marginHorizontal: 10 }}
                  transparent
                  onPress={() => {
                    this.setState({ modalVisible: false });
                  }}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.textStyleCancel}>
                    Cancel
                  </Text>
                </Button>
                <Button
                  style={{ marginHorizontal: 10 }}
                  transparent
                  onPress={() => {
                    this.sendOnWhatsApp();
                  }}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.textStyleConnect}>
                    Submit
                  </Text>
                </Button>
              </View>
            </View>
          </View>
        </Modal>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
  },
  contentUsableStyle: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
  },
  contentUsableStyle2: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
  },
  headlineText: {
    fontFamily: 'PoppinsMedium',
    color: '#131e40',
    fontSize: 18,
    textTransform: 'capitalize',
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  listViewStyle: {
    marginVertical: 2,
    flexDirection: 'row',
  },
  listItemTitle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
  },
  listItemSubtitle: {
    fontFamily: 'Poppins-ExtraLight',
  },
  questionTextStyle: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
  },
  answerTextStyle: {
    fontFamily: 'Poppins-Light',
    fontSize: 14,
  },
  dividerLine: {
    marginVertical: 12,
    marginHorizontal: 12,
    height: 0,
    borderStyle: 'solid',
    borderWidth: 0.7,
    borderColor: '#d8dadf',
  },
  categoryViewStyle: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 10,
  },
  pauseButtonView: {
    flex: 1,
    marginTop: 20,
    alignItems: 'center',
  },
  continueButton: {
    backgroundColor: '#fc2250',
    marginBottom: 40,
    alignSelf: 'center',
  },
  continueButtonText: {
    fontFamily: 'Poppins-Regular',
    color: '#fff',
    paddingLeft: 50,
    paddingRight: 50,
  },
  //Modal
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
    backgroundColor: 'rgba(0,0,0,0.7)',
  },
  modalView: {
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 25,
    paddingBottom: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  },
  modalText: {
    fontFamily: 'Poppins-Regular',
    marginBottom: 15,
    textAlign: 'center',
  },
  textStyleConnect: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#fc2250',
  },
  textStyleCancel: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#fc2250',
  },
  // Modal END
});
