import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Modal,
  Alert,
  ToastAndroid,
  TextInput,
  ScrollView,
} from 'react-native';
import { CustomHeader } from '../../components/CustomHeader';
import { Container, Title, Button, Content } from 'native-base';
import * as Animatable from 'react-native-animatable';
import * as Constants from '../../../Services/AppConstants/app.consts';
import auth from '@react-native-firebase/auth';
import GLOBAL from '../../components/Global.js';
import RBSheet from 'react-native-raw-bottom-sheet';
import PhoneAuthNavigator from '../../navigation/PhoneNumberAuth.js';
import { UserService } from '../../../Services/User/user.service';
import { AuthService } from '../../../Services/Authentication/auth.service';
import { ApplicationConstants } from '../../../Services/ApplicationConstants/application.service';
import UserContext from '../../components/userContext.js';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';

export default class UserAccount extends Component {
  static contextType = UserContext;
  #userService;
  constructor() {
    super();
    this.#userService = new UserService();
  }
  state = {
    userDetails: '',
    userName: '',
    gender: 'male',
    userLoggedIn: false,
    modalVisible: false,
    nameChangeModal: false,
    changedName: '',
    newName: false,
    errorNameEntry: '',
  };

  logOut() {
    auth()
      .signOut()
      .then(() =>
        GLOBAL.screen.setState({
          loggedIn: false,
          subscribed: false,
        }),
      );
    // this.props.navigation.navigate('ParentStackNavigator', {
    //   screen: 'HomeStackNavigatorUnreg',
    //   params: {
    //     screen: 'Footer',
    //     params: {
    //       screen: 'Home',
    //     },
    //   },
    // });
    this.showToastWithGravity('You have been logged out successfully.');
    this.setState({
      modalVisible: false,
    });
  }

  showToastWithGravity = message => {
    ToastAndroid.showWithGravity(
      message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
    );
  };

  componentDidMount() {
    let userId = AuthService.getCurrentUserUserId();
    const user = this.context;
    if (user) {
      this.setState({
        userDetails: user,
        userName: user.name,
      });
    } else {
      this.#userService.getUserDetails(userId).then(documentSnapshot => {
        if (documentSnapshot.data()) {
          this.setState({
            userDetails: documentSnapshot.data(),
            userName: documentSnapshot.data().name,
          });
        }
      });
    }
  }
  updateName = () => {
    let newName = this.state.changedName.trim();
    if (newName.length > 1) {
      let userId = AuthService.getCurrentUserUserId();
      this.#userService.updateUserName(userId, newName, success =>
        console.log(success),
      );
      this.setState({
        userName: newName,
        newName: true,
        nameChangeModal: false,
      });
      this.showToastWithGravity(
        'Your name has been successfully changed. It will reflect when you open the app the next time',
      );
    } else {
      this.setState({
        errorNameEntry: 'Name is not valid',
      });
    }
  };

  render() {
    return (
      <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
        {CustomHeader('My Profile', false, this.props)}
        {GLOBAL.screen.state.loggedIn ? (
          <View style={{ flex: 1 }}>
            <View style={styles.contentTopStyle}>
              <View style={styles.contentUsableStyle}>
                <View>
                  {this.state.gender.toLowerCase() === 'male' ? (
                    <Animatable.Image
                      animation="bounceIn"
                      useNativeDriver={true}
                      style={styles.SummaryFoodImage}
                      source={require('../../assets/Images/UserProfileMale.png')}
                    />
                  ) : (
                    <Animatable.Image
                      animation="bounceIn"
                      style={styles.SummaryFoodImage}
                      source={require('../../assets/Images/UserProfileFemale.png')}
                    />
                  )}
                </View>
                <View style={styles.profileDetails}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Title maxFontSizeMultiplier={1} style={styles.userName}>
                      {this.state.userName}
                    </Title>
                    <TouchableOpacity
                      onPress={() => this.setState({ nameChangeModal: true })}>
                      <MaterialCommunityIcons
                        name="pencil-outline"
                        color="#fff"
                        style={{ fontSize: 18 }}
                      />
                    </TouchableOpacity>
                  </View>
                  {this.state.newName ? (
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.userNameChanged}>
                      (Your name has successfully changed.{'\n'}Change will be
                      reflected throughout the app on app restart.)
                    </Text>
                  ) : null}
                  <View style={styles.userEmailView}>
                    <MaterialCommunityIcons name="email-outline" color="#fff" />
                    <Text maxFontSizeMultiplier={1} style={styles.userEmail}>
                      {this.state.userDetails.email}
                    </Text>
                  </View>
                  <View style={styles.userPhoneView}>
                    <MaterialCommunityIcons name="phone" color="#fff" />
                    <Text maxFontSizeMultiplier={1} style={styles.userPhone}>
                      {Constants.REGION_CODE} -{' '}
                      {this.state.userDetails.mobileNumber}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            <Animatable.View
              style={styles.contentBottomStyle}
              animation="fadeInUp"
              duration={500}
              useNativeDriver={true}>
              <ScrollView
                style={styles.contentUsableStyle2}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}>
                {/* ListItem 1 */}
                <TouchableOpacity
                  style={styles.listViewStyle}
                  onPress={() => {
                    this.props.navigation.navigate('ManageAddress');
                  }}>
                  <MaterialCommunityIcons
                    name="map-marker-outline"
                    style={{
                      color: '#8c8c8c',
                      fontSize: 24,
                      marginRight: 12,
                    }}
                  />
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'column',
                      justifyContent: 'center',
                    }}>
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.listItemTitle}>
                      Manage Address
                    </Text>
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.listItemSubtitle}>
                      Home and Office Address
                    </Text>
                  </View>
                  <MaterialCommunityIcons
                    name="chevron-right"
                    style={{
                      fontSize: 24,
                      color: '#8c8c8c',
                      alignSelf: 'center',
                    }}
                  />
                </TouchableOpacity>
                {/* DIVIDER */}
                <View style={styles.dividerLine} />
                {/* ListItem 2 */}
                <TouchableOpacity
                  style={styles.listViewStyle}
                  onPress={() => {
                    this.props.navigation.navigate('HelpAndFAQNavigator', {
                      screen: 'HelpAndFaq',
                      params: { userDetails: this.state.userDetails },
                    });
                  }}>
                  <MaterialCommunityIcons
                    name="help-circle-outline"
                    style={{
                      color: '#8c8c8c',
                      fontSize: 24,
                      marginRight: 12,
                    }}
                  />
                  <View style={{ flex: 1, flexDirection: 'column' }}>
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.listItemTitle}>
                      Help & FAQs
                    </Text>
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.listItemSubtitle}>
                      FAQ & Report an Issue
                    </Text>
                  </View>
                  <MaterialCommunityIcons
                    name="chevron-right"
                    style={{
                      fontSize: 24,
                      color: '#8c8c8c',
                      alignSelf: 'center',
                    }}
                  />
                </TouchableOpacity>
                <View style={styles.dividerLine} />
                {/* ListItem 3 */}
                <TouchableOpacity
                  style={styles.listViewStyle}
                  onPress={() => {
                    this.props.navigation.navigate('OurSocials');
                  }}>
                  <MaterialCommunityIcons
                    name="account-heart-outline"
                    style={{
                      color: '#8c8c8c',
                      fontSize: 24,
                      marginRight: 12,
                    }}
                  />
                  <View style={{ flex: 1, flexDirection: 'column' }}>
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.listItemTitle}>
                      Our Socials
                    </Text>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                      }}>
                      <Text
                        maxFontSizeMultiplier={1}
                        style={styles.listItemSubtitle}>
                        Follow us on our socials & Cheer us on{' '}
                      </Text>
                      <MaterialCommunityIcons
                        name="heart"
                        style={{
                          fontSize: 18,
                          color: '#c62828',
                        }}
                      />
                    </View>
                  </View>
                  <MaterialCommunityIcons
                    name="chevron-right"
                    style={{
                      fontSize: 24,
                      color: '#8c8c8c',
                      alignSelf: 'center',
                    }}
                  />
                </TouchableOpacity>
                {/* DIVIDER */}
                <View style={styles.dividerLine} />
                {/* LOGOUT */}
                <View style={styles.logOutView}>
                  <Button
                    transparent
                    style={styles.listViewStyle}
                    onPress={() => {
                      // this.logOut();
                      this.setState({
                        modalVisible: true,
                      });
                    }}>
                    <AntDesign
                      name="logout"
                      style={{
                        color: '#fc2250',
                        fontSize: 24,
                        marginRight: 12,
                      }}
                    />
                    <Text maxFontSizeMultiplier={1} style={styles.logOutTitle}>
                      Log Out
                    </Text>
                  </Button>
                </View>
              </ScrollView>
            </Animatable.View>
          </View>
        ) : (
          <View style={styles.contentBottomStyle}>
            <View style={styles.notLoggedInView}>
              <Text maxFontSizeMultiplier={1} style={styles.notLoggedInText}>
                Log in to see your Profile Details
              </Text>
              <Button
                rounded
                style={styles.logInButtonStyle}
                onPress={() => {
                  this.RBSheet.open();
                }}>
                <Text maxFontSizeMultiplier={1} style={styles.logInButtonText}>
                  Log In
                </Text>
              </Button>
            </View>
          </View>
        )}
        <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          closeOnPressMask={false}
          onClose={args => {
            if (args === true) {
              // console.log('loggedIn user');
              GLOBAL.screen.setState({
                loggedIn: true,
              });
            }
          }}
          height={450}
          openDuration={250}
          customStyles={{
            container: {
              borderTopLeftRadius: 20,
              borderTopRightRadius: 20,
              padding: 30,
            },
          }}>
          <TouchableOpacity
            style={styles.closeButtonStyle}
            onPress={() => {
              this.RBSheet.close();
            }}>
            <MaterialCommunityIcons
              name="close"
              style={{ fontSize: 25, color: 'black' }}
            />
          </TouchableOpacity>
          <PhoneAuthNavigator prop={this} />
        </RBSheet>
        <Modal
          animationType="slide"
          transparent={true}
          // presentationStyle="fullScreen"
          statusBarTranslucent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            // Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text maxFontSizeMultiplier={1} style={styles.modalText}>
                Are you sure you want to logout ?
              </Text>
              <View
                style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                <Button
                  style={{ marginHorizontal: 10 }}
                  transparent
                  onPress={() => {
                    this.setState({ modalVisible: false });
                  }}>
                  <Text maxFontSizeMultiplier={1} style={styles.textStyle}>
                    CANCEL
                  </Text>
                </Button>
                <Button
                  style={{ marginHorizontal: 10 }}
                  transparent
                  onPress={() => {
                    this.logOut();
                  }}>
                  <Text maxFontSizeMultiplier={1} style={styles.textStyle}>
                    LOGOUT
                  </Text>
                </Button>
              </View>
            </View>
          </View>
        </Modal>
        <Modal
          animationType="slide"
          transparent={true}
          // presentationStyle="fullScreen"
          statusBarTranslucent={true}
          visible={this.state.nameChangeModal}
          onRequestClose={() => {
            // Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.centeredView}>
            <View style={{ ...styles.modalView, width: '80%' }}>
              <TextInput
                maxFontSizeMultiplier={1}
                autoFocus
                defaultValue={this.state.userDetails.name}
                style={styles.modalText}
                onChangeText={text => this.setState({ changedName: text })}>
                {/* {this.state.userDetails.name} */}
              </TextInput>
              {this.state.errorNameEntry ? (
                <Text
                  maxFontSizeMultiplier={1}
                  style={{ color: 'red', marginTop: 12, textAlign: 'center' }}>
                  {this.state.errorNameEntry}
                </Text>
              ) : null}
              <View
                style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                <Button
                  style={{ marginHorizontal: 10 }}
                  transparent
                  onPress={() => {
                    this.setState({ nameChangeModal: false });
                  }}>
                  <Text maxFontSizeMultiplier={1} style={styles.textStyle}>
                    Cancel
                  </Text>
                </Button>
                <Button
                  style={{ marginHorizontal: 10 }}
                  transparent
                  onPress={() => this.updateName()}>
                  <Text maxFontSizeMultiplier={1} style={styles.textStyle}>
                    Save
                  </Text>
                </Button>
              </View>
            </View>
          </View>
        </Modal>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentTopStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#632dc2',
    // flexGrow: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentBottomStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 3,
  },
  contentUsableStyle: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    // flex: 1,
    marginBottom: 30,
  },
  contentUsableStyle2: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
    // marginBottom: 60,
  },
  // userImageView: {
  //   padding: 10,
  //   height: 80,
  //   width: 80,
  //   backgroundColor: '#fff',
  // },
  SummaryFoodImage: {
    height: 100,
    width: 100,
    borderRadius: 50,
    alignSelf: 'center',
  },
  profileDetails: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  userName: {
    flex: 1,
    fontFamily: 'Poppins-Medium',
    textTransform: 'capitalize',
    color: '#fff',
    fontSize: 18,
    textAlign: 'center',
  },
  userNameChanged: {
    textAlign: 'center',
    fontFamily: 'Poppins-ExtraLight',
    color: '#fff',
    marginLeft: 10,
    fontStyle: 'italic',
  },
  userEmail: {
    fontFamily: 'Poppins-ExtraLight',
    color: '#fff',
    marginLeft: 10,
  },
  userEmailView: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  userPhoneView: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 4,
  },
  userPhone: {
    fontFamily: 'Poppins-ExtraLight',
    color: '#fff',
    marginLeft: 10,
  },
  listViewStyle: {
    marginVertical: 8,
    flexDirection: 'row',
  },
  listItemTitle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
  },
  listItemSubtitle: {
    fontFamily: 'Poppins-ExtraLight',
  },
  dividerLine: {
    marginVertical: 4,
    marginHorizontal: 12,
    height: 0,
    borderStyle: 'solid',
    borderWidth: 0.7,
    borderColor: '#d8dadf',
  },
  logOutTitle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    color: '#fc2250',
  },
  logOutView: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    marginLeft: 16,
    marginRight: 16,
    marginBottom: 20,
  },
  notLoggedInText: {
    fontFamily: 'Poppins-Medium',
    fontSize: 18,
    textAlign: 'center',
  },
  notLoggedInView: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
    marginBottom: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logInButtonStyle: {
    marginTop: 60,
    backgroundColor: '#fc2250',
    alignSelf: 'center',
    marginBottom: 20,
  },
  logInButtonText: {
    paddingLeft: 50,
    paddingRight: 50,
    color: '#fff',
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
  },
  //Modal
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
    backgroundColor: 'rgba(0,0,0,0.7)',
  },
  modalView: {
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 25,
    paddingBottom: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  },
  textStyle: {
    fontFamily: 'Poppins-Medium',
    color: '#fc2250',
    // textAlign: 'center',
  },
  modalText: {
    fontFamily: 'Poppins-Regular',
    marginBottom: 15,
    textAlign: 'center',
  },
  // Modal END
});
