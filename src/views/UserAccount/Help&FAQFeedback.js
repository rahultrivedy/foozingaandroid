import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Modal,
  Linking,
  ToastAndroid,
} from 'react-native';
import { CustomHeader } from '../../components/CustomHeader';
import {
  Container,
  Title,
  Button,
  Content,
  Picker,
  Textarea,
  Form,
  Radio,
} from 'native-base';
import moment from 'moment';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { AuthService } from '../../../Services/Authentication/auth.service';
import { FeedbackService } from '../../../Services/User/feedback.service';

export default class FeedbackSuggestion extends Component {
  #feedbackService;
  constructor() {
    super();
    this.state = {
      selectedFeedback: false,
      selectedSuggestion: false,
      feedbackDescription: '1. \n\n2. \n\n3. ',
      userDetails: '',
    };
    this.#feedbackService = new FeedbackService();
  }

  componentDidMount() {
    this.setState({
      userDetails: this.props.route.params.userDetails,
    });
  }

  feedbackRadioClicked = () => {
    this.setState({
      selectedFeedback: true,
      selectedSuggestion: false,
    });
  };

  suggestionRadioClicked = () => {
    this.setState({
      selectedFeedback: false,
      selectedSuggestion: true,
    });
  };

  checkFormValidity() {
    let userId = AuthService.getCurrentUserUserId();
    var feedbackData = {
      name: this.state.userDetails.name,
      phone: this.state.userDetails.mobileNumber,
      userId: userId,
      feedback: this.state.feedbackDescription,
      dateOfSubmission: moment()
        .startOf('day')
        .format('DD/MM/YYYY'),
    };

    let message1 = 'Please select the Category before submitting';
    let message2 = 'Write detailed feedback before submitting.';
    if (!this.state.feedbackDescription) {
      this.showToastWithGravity(message2);
    } else if (
      !(this.state.selectedFeedback || this.state.selectedSuggestion)
    ) {
      this.showToastWithGravity(message1);
    } else {
      if (this.state.selectedFeedback) {
        this.#feedbackService.addFeedback(
          userId,
          feedbackData,
          resultFeedBack => console.log(resultFeedBack),
          this.setState({
            selectedFeedback: '',
            selectedSuggestion: '',
            feedbackDescription: '',
            feedbackSent: true,
            suggestionSent: false,
          }),
        );
      } else {
        this.#feedbackService.addSuggestion(
          userId,
          feedbackData,
          resultFeedBack => console.log(resultFeedBack),
          this.setState({
            selectedFeedback: false,
            selectedSuggestion: false,
            feedbackDescription: '',
            suggestionSent: true,
            feedbackSent: false,
          }),
        );
      }
    }
  }

  showToastWithGravity = message => {
    ToastAndroid.showWithGravity(
      message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
    );
  };

  render() {
    return (
      <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
        {CustomHeader('Feedback & Suggestion', true, this.props)}
        <Content contentContainerStyle={styles.contentStyle}>
          <View style={styles.contentUsableStyle}>
            <Text maxFontSizeMultiplier={1} style={styles.headlineText}>
              Connect with us for support
            </Text>
            <View style={styles.contentUsableStyle2}>
              <Text maxFontSizeMultiplier={1} style={styles.subHeadingText}>
                * You are using a Beta version of the app. {'\n'} {'\n'}
                We would love to get a feedback from you. {'\n'}* The feedback
                can be app specific or the service as a whole. {'\n'}* You can
                can provide suggestions on improving our service. {'\n'}* We
                might not be able to implement it immediately but will surely
                keep it in mind in the future as we go forward. {'\n'}* Thanks !
              </Text>
            </View>
          </View>
          <View style={styles.contentUsableStyle2}>
            <View>
              {this.state.suggestionSent || this.state.feedbackSent ? (
                <View style={styles.successViewStyle}>
                  <Text maxFontSizeMultiplier={1} style={styles.successStyle}>
                    Thanks for taking the time. {'\n'}Your Feedback/Suggestion
                    has been submitted successfully.
                  </Text>
                </View>
              ) : null}
              <Text
                maxFontSizeMultiplier={1}
                style={{ flex: 1, ...styles.headlineText }}>
                Category :{' '}
              </Text>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-evenly',
                }}>
                <TouchableOpacity
                  style={styles.categoryViewStyle}
                  onPress={() => this.feedbackRadioClicked()}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.categoryTextStyle}>
                    Feedback
                  </Text>
                  <Radio
                    // note
                    onPress={() => this.feedbackRadioClicked()}
                    color={'#455a64'}
                    selectedColor={'#00c853'}
                    style={{ flex: 1, marginLeft: 16 }}
                    selected={this.state.selectedFeedback}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.categoryViewStyle}
                  onPress={() => this.suggestionRadioClicked()}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.categoryTextStyle}>
                    Suggestion
                  </Text>
                  <Radio
                    // note
                    onPress={() => this.suggestionRadioClicked()}
                    color={'#455a64'}
                    selectedColor={'#00c853'}
                    style={{ flex: 1, marginLeft: 16 }}
                    selected={this.state.selectedSuggestion}
                  />
                </TouchableOpacity>
              </View>
            </View>
            {this.state.selectedFeedback || this.state.selectedSuggestion ? (
              <View style={{ marginTop: 16 }}>
                {this.state.selectedSuggestion ? (
                  <Text
                    maxFontSizeMultiplier={1}
                    style={{ flex: 1, ...styles.headlineText }}>
                    Suggestion :{' '}
                  </Text>
                ) : (
                  <Text
                    maxFontSizeMultiplier={1}
                    style={{ flex: 1, ...styles.headlineText }}>
                    Feedback :{' '}
                  </Text>
                )}
                <Textarea
                  // note
                  maxFontSizeMultiplier={1}
                  rowSpan={5}
                  bordered
                  placeholder="Details Here"
                  mode="dropdown"
                  value={this.state.feedbackDescription}
                  style={{ marginTop: 16 }}
                  onChangeText={text =>
                    this.setState({ feedbackDescription: text })
                  }
                />
              </View>
            ) : null}
          </View>
          <View style={styles.pauseButtonView}>
            <Button
              rounded
              style={styles.continueButton}
              onPress={() => {
                this.checkFormValidity();
              }}>
              <Text maxFontSizeMultiplier={1} style={styles.continueButtonText}>
                Submit Issue
              </Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
  },
  contentUsableStyle: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
  },
  contentUsableStyle2: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
  },
  headlineText: {
    fontFamily: 'Poppins-Medium',
    color: '#131e40',
    fontSize: 18,
    textTransform: 'capitalize',
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  listViewStyle: {
    marginVertical: 2,
    flexDirection: 'row',
  },
  listItemTitle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
  },
  listItemSubtitle: {
    fontFamily: 'Poppins-ExtraLight',
  },
  questionTextStyle: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
  },
  answerTextStyle: {
    fontFamily: 'Poppins-Light',
    fontSize: 14,
  },
  dividerLine: {
    marginVertical: 12,
    marginHorizontal: 12,
    height: 0,
    borderStyle: 'solid',
    borderWidth: 0.7,
    borderColor: '#d8dadf',
  },
  categoryViewStyle: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 10,
  },
  categoryTextStyle: {
    fontFamily: 'Poppins-Regular',
  },
  pauseButtonView: {
    flex: 1,
    marginTop: 20,
    alignItems: 'center',
  },
  continueButton: {
    backgroundColor: '#fc2250',
    marginBottom: 40,
    alignSelf: 'center',
  },
  continueButtonText: {
    fontFamily: 'Poppins-Regular',
    color: '#fff',
    paddingLeft: 50,
    paddingRight: 50,
  },
  //Modal
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
    backgroundColor: 'rgba(0,0,0,0.7)',
  },
  modalView: {
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 25,
    paddingBottom: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  },
  modalText: {
    fontFamily: 'Poppins-Regular',
    marginBottom: 15,
    textAlign: 'center',
  },
  textStyleConnect: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#fc2250',
  },
  textStyleCancel: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#fc2250',
  },
  successViewStyle: {
    padding: 12,
    marginTop: 8,
    marginBottom: 8,
    marginHorizontal: 8,
    borderColor: '#fc2250',
    borderRadius: 6,
    borderWidth: 0.5,
  },
  successStyle: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#fc2250',
  },
  // Modal END
});
