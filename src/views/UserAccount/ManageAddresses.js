import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { CustomHeader } from '../../components/CustomHeader';
import { Container, Title, Button, Content } from 'native-base';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import * as Constants from '../../../Services/AppConstants/app.consts';
import { UserService } from '../../../Services/User/user.service';
import { AuthService } from '../../../Services/Authentication/auth.service';

export default class ManageAddress extends Component {
  #userService;
  constructor(props) {
    super(props);
    this.#userService = new UserService();
  }
  state = {
    userName: '',
    homeAddress: '',
    officeAddress: '',
    addressLoading: false,
  };

  componentDidMount() {
    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.setState({ addressLoading: true });
      let userId = AuthService.getCurrentUserUserId();
      this.#userService.getUserDetails(userId).then(documentSnapshot => {
        if (Object.keys(documentSnapshot.data().address.home).length !== 0) {
          this.setState({
            userName: documentSnapshot.data().name,
            homeAddress: documentSnapshot.data().address.home,
          });
        }
        if (Object.keys(documentSnapshot.data().address.office).length !== 0) {
          this.setState({
            userName: documentSnapshot.data().name,
            officeAddress: documentSnapshot.data().address.office,
          });
        }
      });
      this.setState({ addressLoading: false });
    });
  }

  requestHomeAddressChange = () => {
    let homeAddressAvailable = false;
    if (this.state.homeAddress) {
      homeAddressAvailable = true;
    }
    this.props.navigation.navigate('AddressNavigator', {
      screen: 'AddressHomeMap',
      params: {
        deliveryLocation: Constants.ADDRESS_HOME_SCREEN,
        fromScreen: 'manageAddress',
        userName: this.state.userName,
        addressAvailable: homeAddressAvailable,
      },
    });
  };
  requestOfficeAddressChange = () => {
    let officeAddressAvailable = false;
    if (this.state.officeAddress) {
      officeAddressAvailable = true;
    }
    this.props.navigation.navigate('AddressNavigator', {
      screen: 'AddressHomeMap',
      params: {
        deliveryLocation: Constants.ADDRESS_OFFICE_SCREEN,
        fromScreen: 'manageAddress',
        userName: this.state.userName,
        addressAvailable: officeAddressAvailable,
      },
    });
  };

  render() {
    return (
      <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
        {CustomHeader('Manage Addresses', true, this.props)}
        <Content contentContainerStyle={styles.contentStyle}>
          <View style={styles.contentUsableStyle}>
            <View style={styles.contentUsableStyle2}>
              {/* ListItem 1 */}
              <TouchableOpacity
                style={styles.listViewStyle}
                onPress={() => this.requestHomeAddressChange()}>
                <MaterialCommunityIcons
                  name="home-outline"
                  style={{
                    color: '#8c8c8c',
                    fontSize: 24,
                    marginRight: 12,
                  }}
                />
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                  }}>
                  <Text maxFontSizeMultiplier={1} style={styles.listItemTitle}>
                    Home Address
                  </Text>
                  {this.state.homeAddress ? (
                    <View>
                      <Text
                        maxFontSizeMultiplier={1}
                        style={styles.addressInCard}>
                        {this.state.homeAddress.addressHouseNo},{' '}
                        {this.state.homeAddress.addressBuildingName} {'\n'}{' '}
                        {this.state.homeAddress.addressLandmark}
                      </Text>

                      {/* <TouchableOpacity onPress={() => this.addOfficeAddress()}>
                        <Text style={styles.changeAddressButton}>Change</Text>
                      </TouchableOpacity> */}
                    </View>
                  ) : (
                    <Text maxFontSizeMultiplier={1}>Add your home address</Text>
                  )}
                </View>
                <MaterialCommunityIcons
                  name="chevron-right"
                  style={{
                    fontSize: 24,
                    color: '#8c8c8c',
                    alignSelf: 'center',
                  }}
                />
              </TouchableOpacity>
              {/* DIVIDER */}
              <View style={styles.dividerLine} />
              {/* ListItem 2 */}
              <TouchableOpacity
                style={styles.listViewStyle}
                onPress={() => this.requestOfficeAddressChange()}>
                <MaterialCommunityIcons
                  name="office-building"
                  style={{
                    color: '#8c8c8c',
                    fontSize: 24,
                    marginRight: 12,
                  }}
                />
                <View style={{ flex: 1, flexDirection: 'column' }}>
                  <Text maxFontSizeMultiplier={1} style={styles.listItemTitle}>
                    Office Address
                  </Text>
                  {this.state.officeAddress ? (
                    <View>
                      <View style={{ flex: 1 }}>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.addressInCard}>
                          {this.state.officeAddress.addressHouseNo},{' '}
                          {this.state.officeAddress.addressBuildingName} {'\n'}{' '}
                          {this.state.officeAddress.addressLandmark}
                        </Text>
                      </View>
                      {/* <TouchableOpacity onPress={() => this.addOfficeAddress()}>
                        <Text style={styles.changeAddressButton}>Change</Text>
                      </TouchableOpacity> */}
                    </View>
                  ) : (
                    <Text maxFontSizeMultiplier={1}>
                      Add your office address
                    </Text>
                  )}
                </View>
                <MaterialCommunityIcons
                  name="chevron-right"
                  style={{
                    fontSize: 24,
                    color: '#8c8c8c',
                    alignSelf: 'center',
                  }}
                />
              </TouchableOpacity>
              {/* DIVIDER */}
              <View style={styles.dividerLine} />
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
  },
  contentUsableStyle: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
  },
  headlineText: {
    fontFamily: 'PoppinsMedium',
    color: '#131e40',
    fontSize: 18,
    textTransform: 'capitalize',
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  contentUsableStyle2: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
    marginBottom: 60,
  },
  addressInCard: {
    color: '#656c82',
    fontFamily: 'Poppins-Regular',
    marginTop: 10,
  },
  listViewStyle: {
    marginVertical: 5,
    flexDirection: 'row',
  },
  listItemTitle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
  },
  listItemSubtitle: {
    fontFamily: 'Poppins-ExtraLight',
  },
  dividerLine: {
    marginVertical: 12,
    marginHorizontal: 12,
    height: 0,
    borderStyle: 'solid',
    borderWidth: 0.7,
    borderColor: '#d8dadf',
  },
});
