import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Modal,
  Linking,
  ToastAndroid,
} from 'react-native';
import { CustomHeader } from '../../components/CustomHeader';
import {
  Container,
  Title,
  Button,
  Content,
  Picker,
  Textarea,
  Form,
  Radio,
} from 'native-base';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { AuthService } from '../../../Services/Authentication/auth.service';

export default class HelpAndSupport extends Component {
  #applicationConstants;
  constructor() {
    super();
    this.state = {
      selectedAccount: false,
      selectedSubscription: false,
      selectedPayment: false,
      issueDescription: '',
      userDetails: '',
      modalVisible: false,
    };
  }

  componentDidMount() {
    // let userId = AuthService.getCurrentUserUserId();
    // console.log(this.props.route.params.userDetails);
    this.setState({
      userDetails: this.props.route.params.userDetails,
    });
  }

  accountRadioClicked = () => {
    this.setState({
      selectedAccount: true,
      selectedSubscription: false,
      selectedPayment: false,
    });
  };

  subscriptionRadioClicked = () => {
    this.setState({
      selectedAccount: false,
      selectedSubscription: true,
      selectedPayment: false,
    });
  };

  paymentRadioClicked = () => {
    this.setState({
      selectedAccount: false,
      selectedSubscription: false,
      selectedPayment: true,
    });
  };

  checkFormValidity() {
    // console.log(this.state.issueDescription);
    // console.log(this.state.selectedAccount);
    // console.log(this.state.selectedSubscription);
    let message1 = 'Please select the category before submitting the issue';
    let message2 = 'Write detailed description of the issue before submitting.';
    if (!this.state.issueDescription) {
      this.showToastWithGravity(message2);
    } else if (
      !(this.state.selectedAccount || this.state.selectedSubscription)
    ) {
      this.showToastWithGravity(message1);
    } else {
      this.setState({
        modalVisible: true,
      });
    }
  }

  sendOnWhatsApp() {
    let category = '';
    if (this.state.selectedAccount) {
      category = 'Account';
    } else if (this.state.selectedSubscription) {
      category = 'Subscription';
    } else {
      category = 'Payment';
    }
    let userId = AuthService.getCurrentUserUserId();
    let userIdTrimmed = userId.substring(userId.length - 4);
    let message =
      'Name: ' +
      this.state.userDetails.name +
      '\n' +
      'CustomerId: ' +
      'xxxxx' +
      userIdTrimmed +
      '\n' +
      'Category: ' +
      category +
      '\n' +
      'Issue: ' +
      this.state.issueDescription;
    let url = 'whatsapp://send?text=' + message + '&phone=91' + '7204630078';
    Linking.openURL(url)
      .then(data => {
        // console.log('WhatsApp Opened');
        this.setState({
          modalVisible: false,
          selectedAccount: false,
          selectedSubscription: false,
          issueDescription: '',
        });
        this.props.navigation.navigate('Home');
      })
      .catch(() => {
        alert('Make sure Whatsapp installed on your device');
      });
  }

  showToastWithGravity = message => {
    ToastAndroid.showWithGravity(
      message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
    );
  };

  render() {
    // console.log(this.state.selectedAccount);
    // console.log(this.state.selectedSubscription);
    return (
      <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
        {CustomHeader('Help & Support', true, this.props)}
        <Content contentContainerStyle={styles.contentStyle}>
          <View style={styles.contentUsableStyle}>
            <Text maxFontSizeMultiplier={1} style={styles.headlineText}>
              Connect with us for support
            </Text>
            <View style={styles.contentUsableStyle2}>
              <Text maxFontSizeMultiplier={1} style={styles.subHeadingText}>
                * If you are having problems with your account or subscription,
                you can connect with us through here. {'\n'}* Select the
                category and log your issue. {'\n'}* Your issue will be attended
                to on priority basis and solved ASAP depending on the severity
                of the issue.
              </Text>
              <Text
                maxFontSizeMultiplier={1}
                style={{ marginTop: 8, ...styles.subHeadingText }}>
                ** The app is in BETA. The issue will be communicated through
                WhatsApp. We are working towards giving you the best app
                experience.
              </Text>
            </View>
          </View>
          <View style={styles.contentUsableStyle2}>
            <View>
              <Text
                maxFontSizeMultiplier={1}
                style={{ flex: 1, ...styles.headlineText }}>
                Category :{' '}
              </Text>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <TouchableOpacity
                  style={styles.categoryViewStyle}
                  onPress={() => this.accountRadioClicked()}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.categoryTextStyle}>
                    Account
                  </Text>
                  <Radio
                    // note
                    onPress={() => this.accountRadioClicked()}
                    color={'grey'}
                    selectedColor={'#5cb85c'}
                    style={{ marginLeft: 8 }}
                    selected={this.state.selectedAccount}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.categoryViewStyle}
                  onPress={() => this.subscriptionRadioClicked()}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.categoryTextStyle}>
                    Subscription
                  </Text>
                  <Radio
                    // note
                    onPress={() => this.subscriptionRadioClicked()}
                    color={'grey'}
                    selectedColor={'#5cb85c'}
                    style={{ marginLeft: 8 }}
                    selected={this.state.selectedSubscription}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.categoryViewStyle}
                  onPress={() => this.paymentRadioClicked()}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.categoryTextStyle}>
                    Payment
                  </Text>
                  <Radio
                    // note
                    onPress={() => this.paymentRadioClicked()}
                    color={'grey'}
                    selectedColor={'#5cb85c'}
                    style={{ marginLeft: 8 }}
                    selected={this.state.selectedPayment}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ marginTop: 16 }}>
              <Text
                maxFontSizeMultiplier={1}
                style={{ flex: 1, ...styles.headlineText }}>
                Issue :{' '}
              </Text>
              <Textarea
                // note
                maxFontSizeMultiplier={1}
                rowSpan={5}
                bordered
                placeholder="Describe your issue here"
                mode="dropdown"
                style={{ marginTop: 16 }}
                onChangeText={text => this.setState({ issueDescription: text })}
              />
            </View>
            <View style={styles.pauseButtonView}>
              <Button
                rounded
                style={styles.continueButton}
                onPress={() => {
                  this.checkFormValidity();
                }}>
                <Text
                  maxFontSizeMultiplier={1}
                  style={styles.continueButtonText}>
                  Submit Issue
                </Text>
              </Button>
            </View>
          </View>
        </Content>
        {/* Modal to connect to WhatsApp  */}
        <Modal
          animationType="slide"
          transparent={true}
          // presentationStyle="fullScreen"
          statusBarTranslucent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            // Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text maxFontSizeMultiplier={1} style={styles.modalText}>
                Submit request through WhatsApp ?
              </Text>
              <View
                style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                <Button
                  style={{ marginHorizontal: 10 }}
                  transparent
                  onPress={() => {
                    this.setState({ modalVisible: false });
                  }}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.textStyleCancel}>
                    Cancel
                  </Text>
                </Button>
                <Button
                  style={{ marginHorizontal: 10 }}
                  transparent
                  onPress={() => {
                    this.sendOnWhatsApp();
                  }}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.textStyleConnect}>
                    Submit
                  </Text>
                </Button>
              </View>
            </View>
          </View>
        </Modal>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
  },
  contentUsableStyle: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
  },
  contentUsableStyle2: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
  },
  headlineText: {
    fontFamily: 'Poppins-Medium',
    color: '#131e40',
    fontSize: 18,
    textTransform: 'capitalize',
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  listViewStyle: {
    marginVertical: 2,
    flexDirection: 'row',
  },
  listItemTitle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
  },
  listItemSubtitle: {
    fontFamily: 'Poppins-ExtraLight',
  },
  questionTextStyle: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
  },
  answerTextStyle: {
    fontFamily: 'Poppins-Light',
    fontSize: 14,
  },
  dividerLine: {
    marginVertical: 12,
    marginHorizontal: 12,
    height: 0,
    borderStyle: 'solid',
    borderWidth: 0.7,
    borderColor: '#d8dadf',
  },
  categoryViewStyle: {
    // flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 10,
    // backgroundColor: 'yellow',
    marginLeft: 12,
    // borderRadius: 6,
    // borderWidth: 0.5,
    // padding: 4,
    // borderColor: '#fc2250',
  },
  categoryTextStyle: {
    fontFamily: 'Poppins-Regular',
    textAlign: 'center',
  },
  pauseButtonView: {
    flex: 1,
    marginTop: 20,
    alignItems: 'center',
  },
  continueButton: {
    backgroundColor: '#fc2250',
    marginBottom: 40,
    alignSelf: 'center',
  },
  continueButtonText: {
    fontFamily: 'Poppins-Regular',
    color: '#fff',
    paddingLeft: 50,
    paddingRight: 50,
  },
  //Modal
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
    backgroundColor: 'rgba(0,0,0,0.7)',
  },
  modalView: {
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 25,
    paddingBottom: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  },
  modalText: {
    fontFamily: 'Poppins-Regular',
    marginBottom: 15,
    textAlign: 'center',
  },
  textStyleConnect: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#fc2250',
  },
  textStyleCancel: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#fc2250',
  },
  // Modal END
});
