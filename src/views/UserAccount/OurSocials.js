import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Linking,
} from 'react-native';
import { CustomHeader } from '../../components/CustomHeader';
import { Container, Title, Button, Content } from 'native-base';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';

export default class OurSocials extends Component {
  constructor() {
    super();
  }
  state = {};

  componentDidMount() {}

  onPressFacebook = () => {
    Linking.canOpenURL('fb://page/104779601282876').then(result => {
      if (result) {
        Linking.openURL('fb://page/104779601282876').catch(err =>
          console.error('An error occurred', err),
        );
      } else {
        Linking.openURL('https://www.facebook.com/foozingareal').catch(err =>
          console.error('An error occurred', err),
        );
      }
    });
  };
  onPressLinkedIn = () => {
    Linking.openURL(
      'https://www.linkedin.com/company/foozinga/?originalSubdomain=in',
    ).catch(err => console.error('An error occurred', err));
  };

  onPressInstagram = () => {
    Linking.canOpenURL('instagram://user?username=foozinga4homemadefood').then(
      result => {
        if (result) {
          Linking.openURL(
            'instagram://user?username=foozinga4homemadefood',
          ).catch(err => console.error('An error occurred', err));
        } else {
          Linking.openURL(
            'https://instagram.com/foozinga4homemadefood?igshid=1anj1o7tpg0s0',
          ).catch(err => console.error('An error occurred', err));
        }
      },
    );
  };

  render() {
    return (
      <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
        {CustomHeader('Our Socials', true, this.props)}
        <Content contentContainerStyle={styles.contentStyle}>
          <View style={styles.contentUsableStyle}>
            <View style={styles.contentUsableStyle2}>
              {/* ListItem 1 */}
              <TouchableOpacity
                style={styles.listViewStyle}
                onPress={() => {
                  this.onPressFacebook();
                }}>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                  }}>
                  <MaterialCommunityIcons
                    name="facebook"
                    style={{
                      fontSize: 24,
                      color: '#fff',
                      alignSelf: 'center',
                      padding: 12,
                      backgroundColor: '#4267B2',
                      borderRadius: 10,
                      marginBottom: 16,
                    }}
                  />
                  <Text maxFontSizeMultiplier={1} style={styles.listItemTitle}>
                    facebook
                  </Text>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.listItemSubtitle}>
                    Connect with us on our official facebook page
                  </Text>
                </View>
                <MaterialCommunityIcons
                  name="chevron-right"
                  style={{
                    fontSize: 24,
                    color: '#632dc2',
                    alignSelf: 'center',
                  }}
                />
              </TouchableOpacity>
              {/* DIVIDER */}
              <View style={styles.dividerLine} />
              {/* ListItem 2 */}
              <TouchableOpacity
                style={styles.listViewStyle}
                onPress={() => {
                  this.onPressLinkedIn();
                }}>
                <View style={{ flex: 1, flexDirection: 'column' }}>
                  <MaterialCommunityIcons
                    name="linkedin"
                    style={{
                      fontSize: 24,
                      color: '#fff',
                      alignSelf: 'center',
                      padding: 12,
                      backgroundColor: '#0077b5',
                      borderRadius: 10,
                      marginBottom: 16,
                    }}
                  />
                  <Text maxFontSizeMultiplier={1} style={styles.listItemTitle}>
                    linkedIn
                  </Text>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.listItemSubtitle}>
                    Spread our word among your connections on linkedIn
                  </Text>
                </View>
                <MaterialCommunityIcons
                  name="chevron-right"
                  style={{
                    fontSize: 24,
                    color: '#632dc2',
                    alignSelf: 'center',
                  }}
                />
              </TouchableOpacity>
              {/* DIVIDER */}
              <View style={styles.dividerLine} />

              <TouchableOpacity
                style={styles.listViewStyle}
                onPress={() => {
                  this.onPressInstagram();
                }}>
                <View style={{ flex: 1, flexDirection: 'column' }}>
                  <MaterialCommunityIcons
                    name="instagram"
                    style={{
                      fontSize: 24,
                      color: '#833AB4',
                      alignSelf: 'center',
                      // padding: 12,
                      // backgroundColor: '#4267B2',
                      // borderRadius: 10,
                      marginBottom: 16,
                      fontSize: 40,
                    }}
                  />
                  <Text maxFontSizeMultiplier={1} style={styles.listItemTitle}>
                    Instagram
                  </Text>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.listItemSubtitle}>
                    Follow us and show us some love on Instagram
                  </Text>
                </View>
                <MaterialCommunityIcons
                  name="chevron-right"
                  style={{
                    fontSize: 24,
                    color: '#632dc2',
                    alignSelf: 'center',
                  }}
                />
              </TouchableOpacity>
              {/* DIVIDER */}
              <View style={styles.dividerLine} />
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
  },
  contentUsableStyle: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
  },
  contentUsableStyle2: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
    marginBottom: 60,
  },
  headlineText: {
    fontFamily: 'PoppinsMedium',
    color: '#131e40',
    fontSize: 18,
    textTransform: 'capitalize',
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  listViewStyle: {
    marginVertical: 2,
    flexDirection: 'row',
  },
  listItemTitle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    textAlign: 'center',
    marginBottom: 12,
  },
  listItemSubtitle: {
    fontFamily: 'Poppins-ExtraLight',
  },
  dividerLine: {
    marginVertical: 12,
    marginHorizontal: 12,
    height: 0,
    borderStyle: 'solid',
    borderWidth: 0.7,
    borderColor: '#d8dadf',
  },
});
