import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import LottieView from 'lottie-react-native';
import { CustomHeader } from '../components/CustomHeader';
import { Container, Content, Button, Text } from 'native-base';
// import { NavigationActions } from 'react-navigation';
import GLOBAL from '../components/Global.js';

export default class paymentSuccess extends Component {
  constructor(props) {
    super(props);
    this.state = {
      amountPayed: 1579,
      subscriptionStartDate: '15 January 2021',
      subscriptionEndDate: '22 January 2021',
      transactionNumber: '825478923',
    };
  }
  componentDidMount() {
    if (this.props) {
      if (this.props.route.params) {
        this.setState({
          amountPayed: this.props.route.params.totalAmount,
          subscriptionStartDate: this.props.route.params.subStartDate,
          subscriptionEndDate: this.props.route.params.subEndDate,
          transactionNumber: this.props.route.params.transactionNumber,
          futureSubscriptionApplied: this.props.route.params
            .futureSubscriptionApplied,
        });
      }
    }
  }

  continueButtonPress = () => {
    GLOBAL.screen.setState({
      subscribed: true,
    });
    if (this.state.futureSubscriptionApplied) {
      this.props.navigation.navigate('Footer', {
        screen: 'Home',
        params: { userFutureSub: true },
      });
    }
  };
  render() {
    return (
      // <StyleProvider style={getTheme(material)}>
      <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
        {CustomHeader('Payment Successful', false, this.props)}
        <Content contentContainerStyle={styles.contentStyle}>
          <View style={styles.contentUsableStyle}>
            <View style={styles.paymentSuccessfulView}>
              <Text maxFontSizeMultiplier={1} style={styles.headlineText}>
                Your payment of {'\u20B9'} {this.state.amountPayed} completed
                successfully
              </Text>
              <LottieView
                style={{ width: 100, height: 100, flex: 1 }}
                source={require('../assets/LottieAnimation/paymentSuccess.json')}
                autoPlay
                loop
              />
            </View>
          </View>
          <View style={styles.contentUsableStyle}>
            <Text maxFontSizeMultiplier={1} style={styles.headlineText}>
              Welcome to the Foozinga family
            </Text>
            <Text maxFontSizeMultiplier={1} style={styles.informationText}>
              Your Subscription starts from{' '}
              <Text maxFontSizeMultiplier={1} style={styles.infoTextDate}>
                {this.state.subscriptionStartDate}
              </Text>{' '}
              and ends on{' '}
              <Text maxFontSizeMultiplier={1} style={styles.infoTextDate}>
                {this.state.subscriptionEndDate}
              </Text>
              .
            </Text>
            <Text maxFontSizeMultiplier={1} style={styles.transactionText}>
              Your Order Id is :{' '}
              <Text
                maxFontSizeMultiplier={1}
                selectable={true}
                style={styles.transactionNoText}>
                #{this.state.transactionNumber}
              </Text>
            </Text>
          </View>
          <Button
            rounded
            style={styles.continueButton}
            onPress={() => this.continueButtonPress()}>
            <Text maxFontSizeMultiplier={1} style={styles.continueButtonText}>
              Take me Home
            </Text>
          </Button>
        </Content>
      </Container>
      // </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  contentStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
  },
  contentUsableStyle: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
  },
  paymentSuccessfulView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headlineText: {
    fontFamily: 'Poppins-Medium',
    color: '#131e40',
    fontSize: 20,
    textAlign: 'center',

    // textTransform: 'capitalize',
  },
  informationText: {
    marginTop: 24,
    fontFamily: 'Poppins-Regular',
    color: '#131e40',
    fontSize: 18,
    textAlign: 'center',
  },
  infoTextDate: {
    color: '#632dc2',
    fontSize: 20,
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  transactionText: {
    marginTop: 24,
    fontFamily: 'Poppins-Regular',
    textAlign: 'center',
    color: '#131e40',
    fontSize: 16,
  },
  transactionNoText: {
    fontFamily: 'Poppins-Regular',
    textAlign: 'center',
    color: '#1565c0',
    fontSize: 16,
  },
  continueButton: {
    backgroundColor: '#fc2250',
    marginBottom: 40,
    alignSelf: 'center',
    marginTop: 30,
  },
  continueButtonText: {
    paddingLeft: 50,
    paddingRight: 50,
    color: '#fff',
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
  },
});
