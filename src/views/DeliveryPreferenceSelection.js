import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import { CustomHeader } from '../components/CustomHeader';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  Container,
  Content,
  Title,
  Button,
  Radio,
  CheckBox,
} from 'native-base';
import * as Constants from '../../Services/AppConstants/app.consts';
import { UserService } from '../../Services/User/user.service';
import { AuthService } from '../../Services/Authentication/auth.service';

export default class DeliveryPreferenceSelection extends Component {
  #userService;
  constructor(props) {
    super(props);
    this.#userService = new UserService();
    // We need homeAddress & officeAddress from firebase - n set to respective states
    this.state = {
      homeAddress: '',
      officeAddress: '',
      lunch: false, // set programmatically in ComponentDidMount - depends on subscriptionFrequencyOne - already done
      dinner: false, // set programatically
      subscriptionFrequencyOne: true, // true/false - one time a day / 2 times a day
      lunchSelectedHome: false,
      lunchSelectedOffice: false,
      lunchSelected: '',
      dinnerSelectedHome: false,
      dinnerSelectedOffice: false,
      dinnerSelected: '',
      // userDetails: this.props.route.params.userDetails,
    };
  }
  componentDidMount() {
    // console.log(this.props.route.params.userDetails);
    // console.log(this.props.route.params);
    // console.log(this.props.route.params.userDetails.address.home);
    // console.log(
    //   Object.keys(this.props.route.params.userDetails.address.home).length ===
    //     0,
    // );
    // console.log(
    //   Object.keys(this.props.route.params.userDetails.address.office).length ===
    //     0,
    // );

    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.setState({ addressLoading: true });
      let userId = AuthService.getCurrentUserUserId();
      this.#userService.getUserDetails(userId).then(documentSnapshot => {
        if (Object.keys(documentSnapshot.data().address.home).length !== 0) {
          this.setState({
            homeAddress: documentSnapshot.data().address.home,
          });
        }
        if (Object.keys(documentSnapshot.data().address.office).length !== 0) {
          this.setState({
            officeAddress: documentSnapshot.data().address.office,
          });
        }
      });
      this.setState({ addressLoading: false });
    });
    if (!(this.props.route.params.subscriptionFrequency === 'oneTimeADay')) {
      this.setState({
        lunch: true,
        dinner: true,
        subscriptionFrequencyOne: false,
      });
    }
    // this.setState({
    //   homeAddress: this.props.route.params.userDetails.address.home,
    //   officeAddress: this.props.route.params.userDetails.address.office,
    // });
  }

  onPressLunchSelectedHome() {
    this.setState({
      lunchSelectedHome: true,
      lunchSelectedOffice: false,
      lunchSelected: 'home',
    });
  }
  onPressLunchSelectedOffice() {
    this.setState({
      lunchSelectedHome: false,
      lunchSelectedOffice: true,
      lunchSelected: 'office',
    });
  }
  onPressDinnerSelectedHome() {
    this.setState({
      dinnerSelectedHome: true,
      dinnerSelectedOffice: false,
      dinnerSelected: 'home',
    });
  }

  onPressDinnerSelectedOffice() {
    this.setState({
      dinnerSelectedHome: false,
      dinnerSelectedOffice: true,
      dinnerSelected: 'office',
    });
  }

  onPressLunchCheck() {
    this.setState({
      lunch: true,
      dinner: false,
      dinnerSelectedHome: false,
      dinnerSelectedOffice: false,
      dinnerSelected: '',
    });
  }

  onPressDinnerCheck() {
    this.setState({
      dinner: true,
      lunch: false,
      lunchSelectedHome: false,
      lunchSelectedOffice: false,
      lunchSelected: '',
    });
  }

  addOfficeAddress = () => {
    this.props.navigation.navigate('AddressNavigator', {
      screen: 'AddressHomeMap',
      params: { deliveryLocation: Constants.ADDRESS_OFFICE_SCREEN },
    });
  };
  addHomeAddress = () => {
    this.props.navigation.navigate('AddressNavigator', {
      screen: 'AddressHomeMap',
      params: { deliveryLocation: Constants.ADDRESS_HOME_SCREEN },
    });
  };
  onNextButtonPress = () => {
    console.log(this.props.route.params.userDetails);
    let foodFrequencyPerDay = '';
    if (this.state.lunchSelected && this.state.dinnerSelected) {
      foodFrequencyPerDay = 'lunch & dinner';
    } else {
      if (this.state.dinnerSelected) {
        foodFrequencyPerDay = 'dinner';
      } else if (this.state.lunchSelected) {
        foodFrequencyPerDay = 'lunch';
      }
    }
    this.props.navigation.navigate('SubscriptionSummary', {
      foodFrequencyPerDay: foodFrequencyPerDay,
      selectedDinnerLocation: this.state.dinnerSelected,
      selectedLunchLocation: this.state.lunchSelected,
      homeAddress: this.state.homeAddress,
      officeAddress: this.state.officeAddress,
      selectedPlan: this.props.route.params.selectedPlan,
      selectedDiet: this.props.route.params.selectedDiet,
      selectedEthnicity: this.props.route.params.selectedEthnicity,
      selectedNumberOfTiffins: this.props.route.params.selectedNumberOfTiffins,
      startDate: this.props.route.params.startDate,
      userDetails: this.props.route.params.userDetails,
      allergies: this.props.route.params.allergies,
    });
  };
  renderSubmitButton = () => {
    if (this.state.lunch && this.state.dinner) {
      if (this.state.lunchSelected && this.state.dinnerSelected) {
        return (
          <Button
            rounded
            style={styles.continueButton}
            onPress={() => this.onNextButtonPress()}>
            <Text maxFontSizeMultiplier={1} style={styles.continueButtonText}>
              NEXT
            </Text>
          </Button>
        );
      } else {
        return (
          <Button rounded style={styles.continueButtonDisabled}>
            <Text maxFontSizeMultiplier={1} style={styles.continueButtonText}>
              NEXT
            </Text>
          </Button>
        );
      }
    } else {
      if (this.state.lunch || this.state.dinner) {
        if (this.state.lunchSelected || this.state.dinnerSelected) {
          return (
            <Button
              rounded
              style={styles.continueButton}
              onPress={() => this.onNextButtonPress()}>
              <Text maxFontSizeMultiplier={1} style={styles.continueButtonText}>
                NEXT
              </Text>
            </Button>
          );
        } else {
          return (
            <Button rounded style={styles.continueButtonDisabled}>
              <Text maxFontSizeMultiplier={1} style={styles.continueButtonText}>
                NEXT
              </Text>
            </Button>
          );
        }
      }
    }
  };

  render() {
    // console.log(this.props.route.params.userDetails);
    // console.log(this.state.homeAddress);
    // console.log(this.state.officeAddress);
    return (
      <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
        {CustomHeader('Delivery Preference', true, this.props)}
        <Content contentContainerStyle={styles.contentStyle}>
          <View style={styles.contentUsableStyle}>
            <View>
              <Title maxFontSizeMultiplier={1} style={styles.headlineText}>
                Select Delivery Location
              </Title>
              <Text maxFontSizeMultiplier={1} style={styles.subHeadingText}>
                This selection will remain same for your entire subscription
                period.
              </Text>
            </View>
            <View style={{ flexDirection: 'row', marginTop: 20 }}>
              {/* Home Add address card */}
              <View style={styles.addressCardStyle}>
                <View style={{ flexDirection: 'row' }}>
                  <MaterialCommunityIcons
                    name="home-outline"
                    color="#9e9e9e"
                    size={24}
                  />
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.headingInsideCard}>
                    Home
                  </Text>
                </View>
                <View style={{ flex: 1 }}>
                  {this.state.homeAddress ? (
                    <View>
                      <View style={{ flex: 1 }}>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.addressInCard}>
                          {this.state.homeAddress.addressHouseNo},{' '}
                          {this.state.homeAddress.addressBuildingName}
                        </Text>
                      </View>
                      <TouchableOpacity onPress={() => this.addHomeAddress()}>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.changeAddressButton}>
                          Change
                        </Text>
                      </TouchableOpacity>
                    </View>
                  ) : (
                    <TouchableOpacity
                      style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        flex: 1,
                        marginTop: 40,
                      }}
                      onPress={() => this.addHomeAddress()}>
                      <Text
                        maxFontSizeMultiplier={1}
                        style={styles.addAddressButton}>
                        + Add Address
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
              {/* Office Add address card */}
              <View style={styles.addressCardStyle}>
                <View style={{ flexDirection: 'row' }}>
                  <MaterialCommunityIcons
                    name="office-building"
                    color="#9e9e9e"
                    size={24}
                  />
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.headingInsideCard}>
                    Office
                  </Text>
                </View>
                <View style={{ flex: 1 }}>
                  {this.state.officeAddress ? (
                    <View>
                      <View style={{ flex: 1 }}>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.addressInCard}>
                          {this.state.officeAddress.addressHouseNo},{' '}
                          {this.state.officeAddress.addressBuildingName}
                        </Text>
                      </View>
                      <TouchableOpacity onPress={() => this.addOfficeAddress()}>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.changeAddressButton}>
                          Change
                        </Text>
                      </TouchableOpacity>
                    </View>
                  ) : (
                    <TouchableOpacity
                      style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        flex: 1,
                        marginTop: 40,
                      }}
                      onPress={() => this.addOfficeAddress()}>
                      <Text
                        maxFontSizeMultiplier={1}
                        style={styles.addAddressButton}>
                        + Add Address
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </View>
            {/* Choose Lunch or Dinner for subscriptionFrequencyOne = 1 */}
            {this.state.subscriptionFrequencyOne ? (
              <View style={{ marginTop: 20 }}>
                <Title
                  maxFontSizeMultiplier={1}
                  style={styles.lunchDinnerHeading}>
                  Choose your meal preference
                </Title>
                <View style={{ flexDirection: 'row' }}>
                  <TouchableOpacity
                    onPress={() => this.onPressLunchCheck()}
                    style={styles.mealPreferenceCheckView}>
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.mealPreferenceCheckText}>
                      Lunch
                    </Text>
                    <CheckBox
                      onPress={() => this.onPressLunchCheck()}
                      color={'#fc2250'}
                      checked={this.state.lunch}
                      // key={index}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this.onPressDinnerCheck()}
                    style={styles.mealPreferenceCheckView}>
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.mealPreferenceCheckText}>
                      Dinner
                    </Text>
                    <CheckBox
                      onPress={() => this.onPressDinnerCheck()}
                      color={'#fc2250'}
                      checked={this.state.dinner}
                      // key={index}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            ) : null}
            {/* ****** FOR LUNCH ******* */}
            {this.state.lunch ? (
              <View style={{ marginTop: 30 }}>
                <View>
                  <Title
                    maxFontSizeMultiplier={1}
                    style={styles.lunchDinnerHeading}>
                    For Lunch
                  </Title>
                  <View style={{ flex: 1, flexDirection: 'row' }}>
                    {/* Home for Lunch */}
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                      {this.state.homeAddress ? (
                        <TouchableOpacity
                          onPress={() => this.onPressLunchSelectedHome()}
                          style={styles.homeOfficeGridViewEnabled}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.homeOfficeGridViewTextEnabled}>
                            Home
                          </Text>
                          <Radio
                            onPress={() => this.onPressLunchSelectedHome()}
                            color={'#b7bac4'}
                            selectedColor={'#fc2250'}
                            selected={this.state.lunchSelectedHome}
                            // key={index}
                          />
                        </TouchableOpacity>
                      ) : (
                        <View style={styles.homeOfficeGridViewDisabled}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.homeOfficeGridViewTextDisabled}>
                            No Home Address
                          </Text>
                        </View>
                      )}
                    </View>
                    {/* Office for Lunch */}
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                      {this.state.officeAddress ? (
                        <TouchableOpacity
                          onPress={() => this.onPressLunchSelectedOffice()}
                          style={styles.homeOfficeGridViewEnabled}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.homeOfficeGridViewTextEnabled}>
                            Office
                          </Text>
                          <Radio
                            onPress={() => this.onPressLunchSelectedOffice()}
                            color={'#b7bac4'}
                            selectedColor={'#fc2250'}
                            selected={this.state.lunchSelectedOffice}
                            // key={index}
                          />
                        </TouchableOpacity>
                      ) : (
                        <View style={styles.homeOfficeGridViewDisabled}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.homeOfficeGridViewTextDisabled}>
                            No Office Address
                          </Text>
                        </View>
                      )}
                    </View>
                  </View>
                </View>
              </View>
            ) : null}
            {/* ****** FOR DINNER ****** */}
            {this.state.dinner ? (
              <View style={{ marginTop: 30 }}>
                <View>
                  <Title
                    maxFontSizeMultiplier={1}
                    style={styles.lunchDinnerHeading}>
                    For Dinner
                  </Title>
                  <View style={{ flex: 1, flexDirection: 'row' }}>
                    {/* Home for Dinner */}
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                      {this.state.homeAddress ? (
                        <TouchableOpacity
                          onPress={() => this.onPressDinnerSelectedHome()}
                          style={styles.homeOfficeGridViewEnabled}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.homeOfficeGridViewTextEnabled}>
                            Home
                          </Text>
                          <Radio
                            onPress={() => this.onPressDinnerSelectedHome()}
                            color={'#b7bac4'}
                            selectedColor={'#fc2250'}
                            selected={this.state.dinnerSelectedHome}
                            // key={index}
                          />
                        </TouchableOpacity>
                      ) : (
                        <View style={styles.homeOfficeGridViewDisabled}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.homeOfficeGridViewTextDisabled}>
                            No Home Address
                          </Text>
                        </View>
                      )}
                    </View>
                    {/* Office for Dinner */}
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                      {this.state.officeAddress ? (
                        <TouchableOpacity
                          onPress={() => this.onPressDinnerSelectedOffice()}
                          style={styles.homeOfficeGridViewEnabled}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.homeOfficeGridViewTextEnabled}>
                            Office
                          </Text>
                          <Radio
                            onPress={() => this.onPressDinnerSelectedOffice()}
                            color={'#b7bac4'}
                            selectedColor={'#fc2250'}
                            selected={this.state.dinnerSelectedOffice}
                            // key={index}
                          />
                        </TouchableOpacity>
                      ) : (
                        <View style={styles.homeOfficeGridViewDisabled}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.homeOfficeGridViewTextDisabled}>
                            No Office Address
                          </Text>
                        </View>
                      )}
                    </View>
                  </View>
                </View>
              </View>
            ) : null}
          </View>
          {/* SUBMIT BUTTON */}
          <View>{this.renderSubmitButton()}</View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
  },
  contentUsableStyle: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
  },
  headlineText: {
    fontFamily: 'Poppins-Medium',
    color: '#2a3452',
    fontSize: 18,
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  addressCardStyle: {
    flex: 1,
    paddingHorizontal: 10,
    borderRadius: 10,
    paddingVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: '#f3e5f5',
    marginHorizontal: 5,
  },
  headingInsideCard: {
    flex: 1,
    fontFamily: 'Poppins-Medium',
    color: '#2a3452',
    fontSize: 18,
    marginLeft: 10,
  },
  addressInCard: {
    color: '#656c82',
    fontFamily: 'Poppins-Regular',
    marginTop: 10,
  },
  addAddressButton: {
    color: '#fc2250',
    fontFamily: 'Poppins-Medium',
  },
  changeAddressButton: {
    color: '#fc2250',
    fontFamily: 'Poppins-Regular',
  },
  lunchDinnerHeading: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    color: '#632dc2',
  },
  mealPreferenceCheckView: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#fafafb',
    padding: 20,
    margin: 10,
    borderRadius: 10,
  },
  mealPreferenceCheckText: {
    flex: 1,
    color: '#fc2250',
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
  },
  homeOfficeGridViewEnabled: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#fafafb',
    padding: 20,
    margin: 10,
    borderRadius: 10,
    elevation: 2,
  },
  homeOfficeGridViewDisabled: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#fafafb',
    padding: 10,
    margin: 10,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  homeOfficeGridViewTextEnabled: {
    flex: 1,
    color: '#fc2250',
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
  },
  homeOfficeGridViewTextDisabled: {
    flex: 1,
    color: '#656c82',
    fontFamily: 'Poppins-Medium',
  },
  continueButton: {
    backgroundColor: '#fc2250',
    marginBottom: 40,
    alignSelf: 'center',
    marginTop: 30,
  },
  continueButtonDisabled: {
    backgroundColor: '#d8dadf',
    marginBottom: 40,
    alignSelf: 'center',
    marginTop: 30,
  },
  continueButtonText: {
    paddingLeft: 50,
    paddingRight: 50,
    color: '#fff',
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
  },
});
