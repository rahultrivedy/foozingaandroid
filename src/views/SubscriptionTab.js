import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Linking,
  TouchableOpacity,
  ToastAndroid,
  Modal,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import { CustomHeader } from '../components/CustomHeader';
import { Container, Content, CheckBox, Button } from 'native-base';
import * as Animatable from 'react-native-animatable';
import Calendar from '../components/DatePicker.js';
import { UserService } from '../../Services/User/user.service';
import { AuthService } from '../../Services/Authentication/auth.service';
import { ApplicationConstants } from '../../Services/ApplicationConstants/application.service';
import UserContext from '../components/userContext.js';
import moment from 'moment';
import GLOBAL from '../components/Global.js';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export default class RegisteredUserSubscription extends Component {
  #userService;
  #applicationConstants;
  static contextType = UserContext;
  constructor() {
    super();
    this.#userService = new UserService();
    this.#applicationConstants = new ApplicationConstants();
    // GLOBAL.screen = this;
  }
  state = {
    dateIssued: '',
    customerId: '',
    userName: '',
    foodEthnicity: '',
    foodDiet: '',
    foodSubscription: '',
    lunch: false,
    dinner: false,
    // foodFrequencyPerDay: '', // Lunch/Dinner/Lunch & Dinner
    subscriptionDateStart: '',
    subscriptionDateEnd: '',
    subscriptionDateEndUpdated: '',
    numberOfTiffins: '',
    status: 'active', // Active, Suspended, Paused
    daysRemaining: 0,
    userLoggedIn: false,
    startDate: '',
    endDate: '',
    checkLunch: false,
    checkDinner: false,
    errorIncompleteFormMessage: '',
    modalVisible: false,
    modalFutureSubVisible: false,
    modalPauseDatesVisible: false,
    userDetails: '',
    userSubscriptionDetails: '',
    futureSubscription: '',
    cardDataLoading: false,
    activeSubId: '',
    pauseDatesArray: [],
  };
  componentDidMount() {
    let userId = AuthService.getCurrentUserUserId();
    const user = this.context;
    // console.log(user);
    // this.focusListener = this.props.navigation.addListener('focus', () => {
    // });
    // if (Object.keys(user).length > 0) {
    //   if (GLOBAL.screen.state.subscribed) {
    //     if (!user.activeSubscriptionID) {
    //       this.#userService.getUserDetails(userId).then(documentSnapshot => {
    //         if (documentSnapshot.data()) {
    //           activeSubId = documentSnapshot.data().activeSubscriptionID;
    //         }
    //       });
    //     } else {
    //       activeSubId = user.activeSubscriptionID;
    //     }
    //   }
    //   this.setState({
    //     userDetails: user,
    //     customerId: userId,
    //   });
    // } else if (GLOBAL.screen.state.loggedIn) {
    //   this.#userService.getUserDetails(userId).then(documentSnapshot => {
    //     if (documentSnapshot.data()) {
    //       this.setState({
    //         userDetails: documentSnapshot.data(),
    //       });
    //     }
    //   });
    // }
    if (GLOBAL.screen.state.subscribed) {
      this.setState({
        cardDataLoading: true,
        userDetails: user,
        customerId: userId,
      });
      // console.log('activeSub');
      // console.log(activeSubId);
      if (user.futureSubscriptionID) {
        this.#userService
          .getSubscriptionDetails(userId, user.futureSubscriptionID)
          .then(documentSnapshotFutureSubscription => {
            this.setState({
              futureSubscription: documentSnapshotFutureSubscription.data(),
              futureSubscriptionStartDate: moment(
                documentSnapshotFutureSubscription.data().subscriptionStartDate,
                'DD/MM/YYYY',
              ).format('MMM DD, YYYY'),
              futureSubscriptionEndDate: moment(
                documentSnapshotFutureSubscription.data().subscriptionEndDate,
                'DD/MM/YYYY',
              ).format('MMM DD, YYYY'),
            });
          });
      }
      this.#userService
        .getSubscriptionDetails(userId, user.activeSubscriptionID)
        .then(documentSnapshotSubscription => {
          this.setState({
            userSubscriptionDetails: documentSnapshotSubscription.data(),
            subscriptionDateStart: moment(
              documentSnapshotSubscription.data().subscriptionStartDate,
              'DD/MM/YYYY',
            ).format('MMM DD, YYYY'),
            subscriptionDateEnd: moment(
              documentSnapshotSubscription.data().subscriptionEndDate,
              'DD/MM/YYYY',
            ).format('MMM DD, YYYY'),
            subscriptionDateEndUpdated: moment(
              documentSnapshotSubscription.data().subscriptionEndDateUpdated,
              'DD/MM/YYYY',
            ).format('MMM DD, YYYY'),
            dateIssued: moment(
              documentSnapshotSubscription.data().subscriptionCreationDate,
              'DD/MM/YYYY',
            ).format('MMM DD, YYYY'),
            cardDataLoading: false,
          });
          if (
            documentSnapshotSubscription
              .data()
              .subscriptionFrequency.toLowerCase() === 'lunch & dinner'
          ) {
            this.setState({
              lunch: true,
              dinner: true,
            });
          } else if (
            documentSnapshotSubscription
              .data()
              .subscriptionFrequency.toLowerCase() === 'lunch'
          ) {
            this.setState({
              lunch: true,
            });
          } else if (
            documentSnapshotSubscription
              .data()
              .subscriptionFrequency.toLowerCase() === 'dinner'
          ) {
            this.setState({
              dinner: true,
            });
          }
          // Array of non-empty Pause dates
          var pauseDatesArray = [];
          for (
            var i = 0;
            i < documentSnapshotSubscription.data().pauseDates.length;
            i++
          ) {
            if (documentSnapshotSubscription.data().pauseDates[i].date) {
              pauseDatesArray.push(
                documentSnapshotSubscription.data().pauseDates[i],
              );
            }
          }
          this.setState({
            pauseDatesArray: pauseDatesArray,
          });
        });
    } else {
      this.setState({
        userDetails: user,
        customerId: userId,
      });
    }
  }

  checkFormValidity() {
    let message1 =
      'If pausing for 1 day, select the start day and the end date as the same date.';
    let message2 = 'Select meal to pause (lunch / dinner / lunch & dinner).';
    let message3 = 'End Date cannot be before Start Date.';
    if (!(this.state.startDate && this.state.endDate)) {
      this.showToastWithGravity(message1);
    } else if (!(this.state.checkLunch || this.state.checkDinner)) {
      this.showToastWithGravity(message2);
    } else if (this.state.endDate < this.state.startDate) {
      this.showToastWithGravity(message3);
    } else {
      this.setState({
        modalVisible: true,
      });
    }
  }

  /* Send pause request through WhatsApp */

  sendOnWhatsApp() {
    let pauseFoodFrequency = '';
    if (this.state.checkDinner && this.state.checkLunch) {
      pauseFoodFrequency = 'Lunch and Dinner';
    } else if (this.state.checkDinner) {
      pauseFoodFrequency = 'Dinner';
    } else if (this.state.checkLunch) {
      pauseFoodFrequency = 'Lunch';
    }
    let message =
      'Name: ' +
      this.state.userDetails.name +
      '\n' +
      'CustomerId: ' +
      this.state.customerId +
      '\n' +
      'Start Date: ' +
      this.state.startDate +
      '\n' +
      'End Date: ' +
      this.state.endDate +
      '\n' +
      pauseFoodFrequency;
    let url = 'whatsapp://send?text=' + message + '&phone=91' + '7204630078';
    Linking.openURL(url)
      .then(data => {
        // console.log('WhatsApp Opened');
        this.setState({
          modalVisible: false,
          checkDinner: false,
          checkLunch: false,
          startDate: '',
          endDate: '',
        });
        this.props.navigation.navigate('Home');
      })
      .catch(() => {
        alert('Make sure Whatsapp installed on your device');
      });
  }

  onPressLunchRadio() {
    this.setState({
      checkLunch: !this.state.checkLunch,
    });
  }

  onPressDinnerRadio() {
    this.setState({
      checkDinner: !this.state.checkDinner,
    });
  }
  showToastWithGravity = message => {
    ToastAndroid.showWithGravity(
      message,
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
    );
  };

  /* Calculate subscription days left  */
  daysRemaining() {
    let dateEnd = moment(
      this.state.userSubscriptionDetails.subscriptionEndDateUpdated,
      'DD/MM/YYYY',
    );
    let dateStart = moment(
      this.state.userSubscriptionDetails.subscriptionStartDate,
      'DD/MM/YYYY',
    );
    let todaysDate = moment().startOf('day');
    if (dateStart.isAfter(todaysDate, 'day')) {
      let diff = dateEnd.diff(dateStart, 'days');
      // console.log('Date Diff frm start date: ', diff);
      let date = dateStart;
      let numberOfDays = 0;
      for (var i = 0; i <= diff; i++) {
        date = date.add(1, 'days');
        if (date.format('dddd').toLowerCase() !== 'sunday') {
          numberOfDays = numberOfDays + 1;
        }
      }
      // console.log('Number of Days', numberOfDays);
      return numberOfDays;
    } else {
      let diff = dateEnd.diff(todaysDate, 'days', true);
      // console.log('Date Diff frm TODAY: ', diff);
      let date = todaysDate;
      let numberOfDays = 0;
      for (var i = 0; i <= diff; i++) {
        date = date.add(1, 'days');
        if (date.format('dddd').toLowerCase() !== 'sunday') {
          numberOfDays = numberOfDays + 1;
        }
      }
      // console.log('Number of Days', numberOfDays);
      return numberOfDays;
    }
  }

  /* Show pause dates */

  showPauseDates = () => {
    if (this.state.userSubscriptionDetails) {
      if (this.state.userSubscriptionDetails.pauseDates) {
        return (
          <View style={{ flexDirection: 'row' }}>
            <FlatList
              data={this.state.pauseDatesArray}
              renderItem={({ item }) => (
                <View style={{ flexDirection: 'row' }}>
                  <Text
                    style={{
                      flex: 1,
                      textAlign: 'center',
                      ...styles.pauseDateLHS,
                    }}
                    maxFontSizeMultiplier={1}>
                    {item.date}
                  </Text>
                  <Text
                    style={{
                      flex: 1,
                      textAlign: 'center',
                      ...styles.pauseDateRHS,
                    }}
                    maxFontSizeMultiplier={1}>
                    {item.meal}
                  </Text>
                </View>
              )}
            />
          </View>
        );
      }
    }
  };

  render() {
    // console.log('SUB DETAILS');
    // console.log(this.state.userSubscriptionDetails);
    return (
      <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
        {CustomHeader('My Subscription', false, this.props)}
        {GLOBAL.screen.state.subscribed ? (
          <Content
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={styles.contentStyle}>
            <View style={styles.contentUsableStyle}>
              {!this.state.cardDataLoading ? (
                <View>
                  <Animatable.View
                    animation="bounceIn"
                    useNativeDriver={true}
                    style={styles.subscriptionCard}>
                    <View style={styles.cardHeaderView}>
                      <Text
                        maxFontSizeMultiplier={1}
                        numberOfLines={1}
                        style={{
                          ...styles.foozingerText,
                          transform: [{ rotate: '-15deg' }],
                        }}>
                        Foozinger
                      </Text>
                      <Text
                        maxFontSizeMultiplier={1}
                        numberOfLines={2}
                        style={styles.cardName}>
                        {this.state.userDetails.name}
                      </Text>
                      <View style={{ flexDirection: 'column', flex: 1 }}>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.issueDateCard}>
                          Issued on:
                        </Text>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.issueDateCard}>
                          {this.state.dateIssued}
                        </Text>
                      </View>
                    </View>
                    <View style={styles.cardContentSection}>
                      <View style={{ flexDirection: 'row' }}>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.cardEthnicityTitle}>
                          Food Ethnicity :{' '}
                        </Text>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.cardEthnicity}>
                          {
                            this.state.userSubscriptionDetails
                              .subscriptionEthnicity
                          }{' '}
                          /{' '}
                          {this.state.userSubscriptionDetails.subscriptionDiet}
                        </Text>
                      </View>
                      <View style={{ flexDirection: 'row' }}>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.cardEthnicityTitle}>
                          Subscription Plan :{' '}
                        </Text>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.cardEthnicity}>
                          {this.state.userSubscriptionDetails.subscriptionPlan}{' '}
                          /{' '}
                          {
                            this.state.userSubscriptionDetails
                              .subscriptionFrequency
                          }
                        </Text>
                      </View>
                      <View style={{ flexDirection: 'row' }}>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.cardEthnicityTitle}>
                          No. of Tiffins :{' '}
                        </Text>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.cardEthnicity}>
                          {this.state.userSubscriptionDetails.quantity}
                        </Text>
                      </View>
                      <View style={{ flexDirection: 'row' }}>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.cardEthnicityTitle}>
                          Start Date :{' '}
                        </Text>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.cardEthnicity}>
                          {this.state.subscriptionDateStart} -{' '}
                          {
                            this.state.userSubscriptionDetails
                              .subscriptionStartMeal
                          }
                        </Text>
                      </View>
                      <View style={{ flexDirection: 'row' }}>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.cardEthnicityTitle}>
                          End Date :{' '}
                        </Text>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.cardEthnicity}>
                          {this.state.subscriptionDateEnd} -{' '}
                          {
                            this.state.userSubscriptionDetails
                              .subscriptionEndMeal
                          }
                        </Text>
                      </View>
                      {this.state.pauseDatesArray.length > 0 ? (
                        <View style={{ flexDirection: 'row' }}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.cardEthnicityTitle}>
                            Pauses :{' '}
                          </Text>
                          <View style={{ flex: 1 }}>
                            <View style={{ flexDirection: 'row' }}>
                              <Text
                                maxFontSizeMultiplier={1}
                                style={{ flex: 1, textAlign: 'left' }}>
                                {this.state.pauseDatesArray.length}
                              </Text>
                              <TouchableOpacity
                                onPress={() => {
                                  this.setState({
                                    modalPauseDatesVisible: true,
                                  });
                                }}>
                                <Text
                                  style={{
                                    color: '#2196f3',
                                    fontFamily: 'Poppins-Regular',
                                  }}>
                                  See Dates
                                </Text>
                              </TouchableOpacity>
                            </View>
                          </View>
                        </View>
                      ) : null}
                      {this.state.pauseDatesArray.length > 0 ? (
                        <View style={{ flexDirection: 'row' }}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.cardEthnicityTitle}>
                            Updated End Date :{' '}
                          </Text>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.cardEthnicity}>
                            {this.state.subscriptionDateEndUpdated} -{' '}
                            {
                              this.state.userSubscriptionDetails
                                .subscriptionEndMealUpdated
                            }
                          </Text>
                        </View>
                      ) : null}
                      <View style={{ flexDirection: 'row' }}>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.cardEthnicityTitle}>
                          Days Remaining (excluding Sundays) :{' '}
                        </Text>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.cardEthnicity}>
                          {'\n'} {this.daysRemaining()}
                        </Text>
                      </View>
                    </View>
                  </Animatable.View>
                  {/* FUTURE SUB */}
                  {this.state.futureSubscription ? (
                    <View>
                      <View>
                        {/* DIVIDER */}
                        <View style={styles.dividerLine} />
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.futureSubscriptionTitle}>
                          Renewed Subscription
                        </Text>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                          <View
                            style={{
                              flex: 1,
                              marginTop: 16,
                              flexDirection: 'row',
                            }}>
                            <Text
                              maxFontSizeMultiplier={1}
                              style={styles.cardEthnicityTitle}>
                              Start Date :
                            </Text>
                            <Text
                              maxFontSizeMultiplier={1}
                              style={styles.cardEthnicity}>
                              {this.state.futureSubscriptionStartDate}
                            </Text>
                          </View>
                          <View
                            style={{
                              flex: 1,
                              marginTop: 16,
                              flexDirection: 'row',
                            }}>
                            <Text
                              maxFontSizeMultiplier={1}
                              style={styles.cardEthnicityTitle}>
                              End Date :
                            </Text>
                            <Text
                              maxFontSizeMultiplier={1}
                              style={styles.cardEthnicity}>
                              {this.state.futureSubscriptionEndDate}
                            </Text>
                          </View>
                        </View>
                      </View>
                      <TouchableOpacity
                        onPress={() =>
                          this.setState({ modalFutureSubVisible: true })
                        }>
                        <Text style={styles.futureSubDetailsText}>
                          more details
                        </Text>
                      </TouchableOpacity>
                      {/* DIVIDER */}
                      <View style={styles.dividerLine} />
                    </View>
                  ) : null}
                </View>
              ) : (
                <View style={styles.emptyCardView}>
                  <ActivityIndicator small color="#fc2250" />
                </View>
              )}
              <View style={styles.editSubscription}>
                <View>
                  <Text maxFontSizeMultiplier={1} style={styles.headlineText}>
                    Pause Subscription
                  </Text>
                  <Text maxFontSizeMultiplier={1} style={styles.subHeadingText}>
                    * You can pause your subscription for future dates up to a
                    week. {'\n'}* Your subscription end date will be moved
                    forward once request is accepted. {'\n'}* Accepted Request
                    time : Before 8am for Lunch and Before 4 p.m for dinner.{' '}
                    {'\n'}* If pausing for 1 day, select the start day and the
                    end date as the same date.
                  </Text>
                  <Text maxFontSizeMultiplier={1} style={styles.subHeadingText}>
                    ** The app is in BETA. The pause request will be taken
                    through the app and transferred to whatsApp. We are working
                    towards giving you the best app experience.
                  </Text>
                </View>
                <View style={styles.pauseSubscription}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                      <Text
                        maxFontSizeMultiplier={1}
                        style={styles.calendarTitle}>
                        Start Date
                      </Text>
                      <Calendar
                        notifyChange={date =>
                          this.setState({ startDate: date })
                        }
                      />
                    </View>
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                      <Text
                        maxFontSizeMultiplier={1}
                        style={styles.calendarTitle}>
                        End Date
                      </Text>
                      <Calendar
                        minDate={0}
                        notifyChange={date => this.setState({ endDate: date })}
                      />
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginTop: 16 }}>
                    {this.state.lunch ? (
                      <TouchableOpacity
                        onPress={() => this.onPressLunchRadio()}
                        style={styles.homeOfficeGridViewEnabled}>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.homeOfficeGridViewTextEnabled}>
                          Lunch
                        </Text>
                        <CheckBox
                          color={'#fc2250'}
                          checked={this.state.checkLunch}
                          onPress={() => this.onPressLunchRadio()}
                          // key={index}
                        />
                      </TouchableOpacity>
                    ) : null}
                    {this.state.dinner ? (
                      <TouchableOpacity
                        onPress={() => this.onPressDinnerRadio()}
                        style={styles.homeOfficeGridViewEnabled}>
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.homeOfficeGridViewTextEnabled}>
                          Dinner
                        </Text>
                        <CheckBox
                          color={'#fc2250'}
                          checked={this.state.checkDinner}
                          onPress={() => this.onPressDinnerRadio()}
                          // key={index}
                        />
                      </TouchableOpacity>
                    ) : null}
                  </View>
                  <View style={styles.pauseButtonView}>
                    <Button
                      rounded
                      style={styles.continueButton}
                      onPress={() => {
                        this.checkFormValidity();
                      }}>
                      <Text
                        maxFontSizeMultiplier={1}
                        style={styles.continueButtonText}>
                        Request Pause
                      </Text>
                    </Button>
                  </View>
                </View>
              </View>
            </View>
          </Content>
        ) : (
          <Content
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={styles.contentStyle}>
            <View style={styles.contentUsableStyle}>
              <Animatable.View
                animation="bounceIn"
                useNativeDriver={true}
                style={styles.subscriptionCard}>
                <View style={styles.cardHeaderView}>
                  <Text
                    maxFontSizeMultiplier={1}
                    numberOfLines={1}
                    style={{
                      ...styles.foozingerText,
                      transform: [{ rotate: '-15deg' }],
                    }}>
                    Foozinger
                  </Text>
                  {GLOBAL.screen.state.loggedIn ? (
                    <Text
                      maxFontSizeMultiplier={1}
                      numberOfLines={2}
                      style={styles.cardName}>
                      {this.state.userDetails.name}
                    </Text>
                  ) : (
                    <Text
                      maxFontSizeMultiplier={1}
                      numberOfLines={2}
                      style={styles.cardName}>
                      Hey Foozinger
                    </Text>
                  )}
                  <View style={{ flexDirection: 'column', flex: 1 }}>
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.issueDateCard}>
                      Issued on:
                    </Text>
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.issueDateCard}>
                      {this.state.dateIssued}
                    </Text>
                  </View>
                </View>
                <View style={styles.cardContentSection}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.cardSampleHeading}>
                    Subscription Card
                  </Text>
                  <Text maxFontSizeMultiplier={1} style={styles.cardSampleText}>
                    Your subscription details{'\n'}will show up here
                  </Text>
                </View>
              </Animatable.View>
              <View style={styles.informationTextView}>
                <View style={styles.howItWorksView}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.informationTextTitle}>
                    How it works:{' '}
                  </Text>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.informationText}>
                    Choose your preferred Food Ethnicity
                  </Text>
                  <MaterialCommunityIcons
                    name="arrow-down"
                    style={{
                      color: '#632dc2',
                      fontSize: 32,
                    }}
                  />
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.informationText}>
                    Select weekly or monthly subscription
                  </Text>
                  <MaterialCommunityIcons
                    name="arrow-down"
                    style={{
                      color: '#632dc2',
                      fontSize: 32,
                    }}
                  />
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.informationText}>
                    Select Lunch / Dinner / Lunch & Dinner
                  </Text>
                  <MaterialCommunityIcons
                    name="arrow-down"
                    style={{
                      color: '#632dc2',
                      fontSize: 32,
                    }}
                  />
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.informationText}>
                    Subscribe
                  </Text>
                  <MaterialCommunityIcons
                    name="arrow-down"
                    style={{
                      color: '#632dc2',
                      fontSize: 32,
                    }}
                  />
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.informationTextHighlight}>
                    Sit back and enjoy home made food{'\n'}delivered to you at
                    your Home or Office
                  </Text>
                </View>
                <View style={{ marginTop: 12 }}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.informationTextTitle2}>
                    Special Features:
                  </Text>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.informationTextSubtitle}>
                    PAUSE {'  '}- {'  '}
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.informationText}>
                      Going out. No issues. Pause your meals any day, giving
                      prior notice
                    </Text>
                  </Text>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.informationTextSubtitle}>
                    Cancellation {'  '}- {'  '}
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.informationText}>
                      Cancel any time. No questions asked.
                    </Text>
                  </Text>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.informationTextSubtitle}>
                    Request for cook change {'  '}- {'  '}
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.informationText}>
                      Don't love the food. We'll find you a different cook.
                    </Text>
                  </Text>
                </View>
              </View>
            </View>
          </Content>
        )}
        {/* Modal to connect to WhatsApp  */}
        <Modal
          animationType="slide"
          transparent={true}
          // presentationStyle="fullScreen"
          statusBarTranslucent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setState({
              modalVisible: false,
            });
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text maxFontSizeMultiplier={1} style={styles.modalText}>
                Submit request through WhatsApp ?
              </Text>
              <View
                style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                <Button
                  style={{ marginHorizontal: 10 }}
                  transparent
                  onPress={() => {
                    this.setState({ modalVisible: false });
                  }}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.textStyleCancel}>
                    Cancel
                  </Text>
                </Button>
                <Button
                  style={{ marginHorizontal: 10 }}
                  transparent
                  onPress={() => {
                    this.sendOnWhatsApp();
                  }}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.textStyleConnect}>
                    Submit
                  </Text>
                </Button>
              </View>
            </View>
          </View>
        </Modal>

        {/* Modal to show future Sub Details */}
        <Modal
          animationType="slide"
          transparent={true}
          // presentationStyle="fullScreen"
          statusBarTranslucent={true}
          visible={this.state.modalFutureSubVisible}
          onRequestClose={() => {
            this.setState({
              modalFutureSubVisible: false,
            });
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <View style={styles.cardContentSection}>
                <View style={{ flexDirection: 'row' }}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.cardEthnicityTitle}>
                    Food Ethnicity :{' '}
                  </Text>
                  <Text maxFontSizeMultiplier={1} style={styles.cardEthnicity}>
                    {this.state.futureSubscription.subscriptionEthnicity} /{' '}
                    {this.state.futureSubscription.subscriptionDiet}
                  </Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.cardEthnicityTitle}>
                    Subscription Plan :{' '}
                  </Text>
                  <Text maxFontSizeMultiplier={1} style={styles.cardEthnicity}>
                    {this.state.futureSubscription.subscriptionPlan} /{' '}
                    {this.state.futureSubscription.subscriptionFrequency}
                  </Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.cardEthnicityTitle}>
                    No. of Tiffins :{' '}
                  </Text>
                  <Text maxFontSizeMultiplier={1} style={styles.cardEthnicity}>
                    {this.state.futureSubscription.quantity}
                  </Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.cardEthnicityTitle}>
                    Start Date :{' '}
                  </Text>
                  <Text maxFontSizeMultiplier={1} style={styles.cardEthnicity}>
                    {this.state.futureSubscriptionStartDate} -{' '}
                    {this.state.futureSubscription.subscriptionStartMeal}
                  </Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.cardEthnicityTitle}>
                    End Date :{' '}
                  </Text>
                  <Text maxFontSizeMultiplier={1} style={styles.cardEthnicity}>
                    {this.state.futureSubscriptionEndDate} -{' '}
                    {this.state.futureSubscription.subscriptionEndMeal}
                  </Text>
                </View>
              </View>
              {/* </View> */}
              <View
                style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                <Button
                  style={{ marginHorizontal: 10 }}
                  transparent
                  onPress={() => {
                    this.setState({ modalFutureSubVisible: false });
                  }}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.textStyleCancel}>
                    Back
                  </Text>
                </Button>
              </View>
            </View>
          </View>
        </Modal>

        {/* Modal to show Pause Dates */}
        <Modal
          animationType="slide"
          transparent={true}
          // presentationStyle="fullScreen"
          statusBarTranslucent={true}
          visible={this.state.modalPauseDatesVisible}
          onRequestClose={() => {
            this.setState({
              modalPauseDatesVisible: false,
            });
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <View
                style={{
                  backgroundColor: '#632dc2',
                  marginBottom: 24,
                  padding: 8,
                  borderRadius: 5,
                }}>
                <Text
                  maxFontSizeMultiplier={1}
                  style={{
                    textAlign: 'center',
                    fontSize: 18,
                    color: '#fff',
                  }}>
                  {' '}
                  Pause Dates
                </Text>
              </View>
              <View style={styles.cardContentSection}>
                {this.showPauseDates()}
              </View>
              {/* </View> */}
              <View
                style={{
                  marginTop: 16,
                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                }}>
                <Button
                  style={{ marginHorizontal: 10 }}
                  transparent
                  onPress={() => {
                    this.setState({ modalPauseDatesVisible: false });
                  }}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.textStyleCancel}>
                    Back
                  </Text>
                </Button>
              </View>
            </View>
          </View>
        </Modal>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
  },
  contentUsableStyle: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
  },
  headlineText: {
    fontFamily: 'Poppins-Medium',
    color: '#2a3452',
    fontSize: 14,
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
    fontSize: 12,
  },
  emptyCardView: {
    height: '40%',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 0.5,
    borderRadius: 10,
    borderColor: '#CFD8DC',
  },
  subscriptionCard: {
    backgroundColor: '#CFD8DC',
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 20,
    elevation: 5,
  },
  cardHeaderView: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 0,
  },
  foozingerText: {
    fontFamily: 'Bella-Sweety',
    fontSize: 42,
    color: '#fc2250',
    flex: 1,
    bottom: 10,
    // right: 5,
    // backgroundColor: 'green',
    // textAlign: 'left',
  },
  issueDateCard: {
    fontFamily: 'Poppins-Regular',
    color: '#283593',
    textAlign: 'right',
    fontSize: 10,
  },
  cardContentSection: {
    marginTop: 0,
    padding: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: '80%',
    alignSelf: 'center',
  },
  cardName: {
    fontFamily: 'Poppins-Medium',
    color: '#283593',
    fontSize: 14,
    // flex: 1,
    textAlign: 'center',
    textTransform: 'capitalize',
    marginBottom: 20,
    width: 120,
  },
  cardEmail: {
    fontFamily: 'Poppins-Light',
    color: '#000',
    fontSize: 12,
  },
  cardEthnicityTitle: {
    fontFamily: 'Poppins-Light',
    color: '#000',
    fontSize: 12,
    // textAlign: 'center',
    flex: 1,
  },
  cardEthnicity: {
    fontFamily: 'Poppins-Medium',
    color: '#000',
    fontSize: 12,
    textTransform: 'capitalize',
    flex: 1,
  },
  // CARD SECTION OVER
  editSubscription: {
    marginTop: 20,
  },
  pauseSubscription: {
    marginTop: 20,
  },
  calendarTitle: {
    textAlign: 'left',
    marginRight: 20,
    fontFamily: 'Poppins-Medium',
    fontSize: 14,
    color: '#fc2250',
    paddingVertical: 10,
  },
  pauseButtonView: {
    flex: 1,
    marginTop: 20,
    alignItems: 'center',
  },
  continueButton: {
    backgroundColor: '#fc2250',
    marginBottom: 40,
    alignSelf: 'center',
  },
  continueButtonText: {
    fontFamily: 'Poppins-Regular',
    color: '#fff',
    paddingLeft: 50,
    paddingRight: 50,
  },
  homeOfficeGridViewEnabled: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
    borderRadius: 10,
    width: 100,
    justifyContent: 'flex-start',
  },
  homeOfficeGridViewTextEnabled: {
    // flex: 1,
    // color: '#632dc2',
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    marginRight: 10,
    textAlign: 'center',
  },
  //Modal
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
    backgroundColor: 'rgba(0,0,0,0.7)',
  },
  modalView: {
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 25,
    paddingBottom: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  },
  modalText: {
    fontFamily: 'Poppins-Regular',
    marginBottom: 15,
    textAlign: 'center',
  },
  textStyleConnect: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#fc2250',
  },
  textStyleCancel: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#fc2250',
  },
  // Modal END

  // NOT SUBSCRIBED USER
  cardSampleHeading: {
    fontFamily: 'Poppins-Medium',
    fontSize: 18,
    color: '#fc2250',
  },
  cardSampleText: {
    textAlign: 'center',
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#283593',
  },
  informationTextView: {
    // backgroundColor: '#632dc2',
    marginTop: 24,
    marginBottom: 12,
  },
  howItWorksView: {
    marginHorizontal: 8,
    marginTop: 12,
    alignItems: 'center',
    borderRadius: 10,
    borderWidth: 0.5,
    borderColor: '#632dc2',
    paddingVertical: 8,
  },
  informationTextTitle: {
    paddingHorizontal: 8,
    paddingVertical: 4,
    fontFamily: 'Poppins-Medium',
    color: '#2a3452',
    fontSize: 18,
    fontStyle: 'italic',
    marginBottom: 12,
  },
  informationTextTitle2: {
    fontFamily: 'Poppins-Medium',
    color: '#2a3452',
    fontSize: 18,
    marginBottom: 8,
    fontStyle: 'italic',
  },
  informationTextSubtitle: {
    fontFamily: 'Poppins-Medium',
    color: '#2a3452',
    fontSize: 16,
    marginTop: 8,
    // fontWeight: 'bold',
  },
  informationText: {
    fontFamily: 'Poppins-Regular',
    color: '#2a3452',
    fontSize: 14,
  },
  informationTextHighlight: {
    textAlign: 'center',
    fontFamily: 'Poppins-Medium',
    color: '#283593',
    fontSize: 14,
  },
  futureSubscriptionTitle: {
    textAlign: 'center',
    fontFamily: 'Poppins-Medium',
    marginTop: 12,
    color: '#632dc2',
  },
  dividerLine: {
    marginVertical: 4,
    marginHorizontal: 12,
    height: 0,
    borderStyle: 'solid',
    borderWidth: 0.7,
    borderColor: '#d8dadf',
  },
  futureSubDetailsText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#fc2250',
    textAlign: 'right',
    marginRight: 16,
    marginTop: 12,
  },
  pauseDateLHS: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    color: '#2a3452',
  },
  pauseDateRHS: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    color: '#2a3452',
    textTransform: 'capitalize',
  },
});
