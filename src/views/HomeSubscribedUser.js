import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  FlatList,
  ScrollView,
  Image,
  ImageBackground,
  Modal,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import LottieView from 'lottie-react-native';
import { CustomHeader } from '../components/CustomHeader';
import {
  Container,
  Content,
  Button,
  Text,
  Card,
  Title,
  Tab,
  Tabs,
  Subtitle,
  Icon,
  StyleProvider,
} from 'native-base';
import Carousel from 'react-native-snap-carousel';
import LinearGradient from 'react-native-linear-gradient';
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';
import { UserService } from '../../Services/User/user.service';
import { NewUserService } from '../../Services/User/newUser.service';
import { AuthService } from '../../Services/Authentication/auth.service';
import { ApplicationConstants } from '../../Services/ApplicationConstants/application.service';
import moment from 'moment';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import UserContext from '../components/userContext.js';
import { clearThemeCache } from 'native-base-shoutem-theme';
import GLOBAL from '../components/Global.js';
import * as Animatable from 'react-native-animatable';
import * as Constants from '../../Services/AppConstants/app.consts';
import * as Quotes from '../assets/quotes.json';
import * as FunnyQuotes from '../assets/funnyQuotes.json';
// import storage from '@react-native-firebase/storage';

export default class HomeSubscribedUser extends Component {
  #userService;
  #applicationConstants;
  #newUserService;
  pricing = {};
  dietEthnicity = {};
  static contextType = UserContext;
  constructor(props) {
    super(props);
    this.#userService = new UserService();
    this.#newUserService = new NewUserService();
    this.#applicationConstants = new ApplicationConstants();
    this.state = {
      userDetails: '',
      jsonAdd: '',
      activeIndex: 0,
      cookMenuDetails: '',
      renewSubActive: false,
      renewSubActiveMessage: false,
      backgroundSourceImage: '',
      greetingTime: 'Morning',
      randomQuoteNumber: null,
      carouselItems: [],
      modalVisible: false,
      quotesJson: Quotes,
      quotesJsonQuote: '',
      quotesJsonAuthor: '',
      loading: true,
      error: '',
    };
  }

  lunchJsonAddress = (documentSnapshotCook, day) => {
    switch (day) {
      case 'Monday':
        return documentSnapshotCook.data().Monday.Lunch;
      case 'Tuesday':
        return documentSnapshotCook.data().Tuesday.Lunch;
      case 'Wednesday':
        return documentSnapshotCook.data().Wednesday.Lunch;
      case 'Thursday':
        return documentSnapshotCook.data().Thursday.Lunch;
      case 'Friday':
        return documentSnapshotCook.data().Friday.Lunch;
      case 'Saturday':
        return documentSnapshotCook.data().Saturday.Lunch;
      case 'Sunday':
        return '';
      default:
        break;
    }
  };

  dinnerJsonAddress = (documentSnapshotCook, day) => {
    switch (day) {
      case 'Monday':
        return documentSnapshotCook.data().Monday.Dinner;
      case 'Tuesday':
        return documentSnapshotCook.data().Tuesday.Dinner;
      case 'Wednesday':
        return documentSnapshotCook.data().Wednesday.Dinner;
      case 'Thursday':
        return documentSnapshotCook.data().Thursday.Dinner;
      case 'Friday':
        return documentSnapshotCook.data().Friday.Dinner;
      case 'Saturday':
        return documentSnapshotCook.data().Saturday.Dinner;
      case 'Sunday':
        return '';
      default:
        break;
    }
  };
  componentDidMount() {
    clearThemeCache();
    const user = this.context;
    let userId = AuthService.getCurrentUserUserId();
    let randomQuoteNumber = Math.floor(Math.random() * 5400 + 1);
    let randomQuoteNumberFunny = Math.floor(Math.random() * 100 + 1);
    let now = moment().format('HH');
    let todaysDay = moment()
      .startOf('day')
      .format('dddd');
    if (
      todaysDay.toLowerCase() === 'monday' ||
      todaysDay.toLowerCase() === 'friday' ||
      todaysDay.toLowerCase() === 'sunday'
    ) {
      this.setState({
        quotesJsonQuote: FunnyQuotes[randomQuoteNumberFunny].quoteText,
        quotesJsonAuthor: FunnyQuotes[randomQuoteNumberFunny].quoteAuthor,
      });
    } else {
      this.setState({
        // quotesJson: Quotes[randomQuoteNumber],
        quotesJsonQuote: Quotes[randomQuoteNumber].quoteText,
        quotesJsonAuthor: Quotes[randomQuoteNumber].quoteAuthor,
      });
    }
    this.setState({
      randomQuoteNumber: randomQuoteNumber,
      randomQuoteNumberFunny: randomQuoteNumberFunny,
    });
    if (now >= 16) {
      this.setState({
        backgroundSourceImage: require('../assets/Images/greetingSunset.jpg'),
        greetingTime: 'Evening',
      });
    } else {
      this.setState({
        backgroundSourceImage: require('../assets/Images/greetingSunrise.jpg'),
        greetingTime: 'Morning',
      });
    }
    this.focusListener = this.props.navigation.addListener('focus', () => {
      if (user) {
        if (this.props.route) {
          if (this.props.route.params) {
            if (this.props.route.params.userFutureSub) {
              this.setState({
                renewSubActive: false,
                renewSubActiveMessage: true,
              });
            }
          }
        }
      }
      if (now >= 16) {
        this.setState({
          backgroundSourceImage: require('../assets/Images/greetingSunset.jpg'),
          greetingTime: 'Evening',
        });
      } else {
        this.setState({
          backgroundSourceImage: require('../assets/Images/greetingSunrise.jpg'),
          greetingTime: 'Morning',
        });
      }
    });

    if (user) {
      // console.log(user);
      this.setState({ loading: true });
      const endDate = moment(user.endDate, 'DD/MM/YYYY');
      let todaysDate = moment().startOf('day');
      let dateDiff = endDate.diff(todaysDate, 'days');
      // console.log('DATE DIFF : ', dateDiff);
      this.setState({
        userDetails: user,
      });
      if (!user.futureSubscriptionID) {
        if (dateDiff <= 3) {
          this.setState({ renewSubActive: true, dateDiff: dateDiff });
          this.#applicationConstants
            .getApplicationConstants()
            .then(querySnapshot => {
              querySnapshot.forEach(documentSnapshot => {
                switch (documentSnapshot.id) {
                  case Constants.PRICE_DETAILS:
                    this.pricing = documentSnapshot.data();
                    break;
                  case Constants.ETHNICITY_DIET:
                    this.dietEthnicity = documentSnapshot.data();
                    this.setState({
                      dietEth: documentSnapshot.data(),
                    });
                    break;
                  default:
                    // console.log('Document Id does not match.');
                    break;
                }
              });
            })
            .catch(error => {
              console.log(error);
            });
          this.#userService
            .getSubscriptionDetails(userId, user.activeSubscriptionID)
            .then(documentSnapshotSubscription => {
              this.setState({
                userSubscriptionDetails: documentSnapshotSubscription.data(),
              });
            });
        }
      }
    }
    this.setState({
      jsonAdd: 'documentSnapshotCook.data().Friday.Lunch',
    });
    const todaysDate = moment().startOf('day');
    // const todaysDay = todaysDate.startOf('day').format('dddd');
    const tomorrowsDay = todaysDate
      .clone()
      .add(1, 'days')
      .format('dddd');
    if (todaysDay.toLowerCase() === 'sunday') {
      this.setState({
        carouselItems: [
          {
            date: todaysDate.format('Do-MMM-YYYY'),
            title: 'Today',
          },
        ],
      });
      this.setState({ loading: false });
    } else {
      if (user.cook) {
        this.#newUserService.deleteNewUserFromList(userId);
        this.#applicationConstants
          .getCookDetails(user.cook)
          .then(querySnapshot => {
            querySnapshot.forEach(documentSnapshotCook => {
              console.log(documentSnapshotCook);
              this.setState({
                cookMenuDetails: documentSnapshotCook.data(),
                carouselItems: [
                  {
                    date: todaysDate.format('Do-MMM-YYYY'),
                    title: 'Today',
                    lunch: this.lunchJsonAddress(
                      documentSnapshotCook,
                      todaysDay,
                    ),
                    dinner: this.dinnerJsonAddress(
                      documentSnapshotCook,
                      todaysDay,
                    ),
                  },
                  {
                    date: todaysDate
                      .clone()
                      .add(1, 'days')
                      .format('Do-MMM-YYYY'),
                    title: 'Tomorrow',
                    lunch: this.lunchJsonAddress(
                      documentSnapshotCook,
                      tomorrowsDay,
                    ),
                    dinner: this.dinnerJsonAddress(
                      documentSnapshotCook,
                      tomorrowsDay,
                    ),
                  },
                ],
              });
            });
            this.setState({ loading: false });
          })
          .catch(error => {
            console.log(error);
            this.setState({
              loading: false,
              errorText:
                'Could not retrieve information. \n Please close the app and try later.',
            });
          });
        // this.setState({ loading: false });
      } else {
        this.setState({ loading: false });
      }
    }
  }

  _renderItem({ item, index }) {
    const todaysDate = moment();
    const todaysDay = todaysDate.format('dddd');
    if (todaysDay.toLowerCase() === 'sunday') {
      return (
        <LinearGradient
          colors={['rgba(219,0,93,0.8)', 'rgba(167,0,255,0.8)']}
          start={{ x: -1, y: 0 }}
          end={{ x: 1, y: -1 }}
          style={styles.linearGradientCarousal}>
          <View style={styles.carousalCardDateSection}>
            <Text
              maxFontSizeMultiplier={1}
              style={{
                fontFamily: 'Poppins-Medium',
                color: '#fff',
                fontSize: 12,
              }}>
              {item.title}
              <Text
                maxFontSizeMultiplier={1}
                style={{
                  fontFamily: 'Poppins',
                  color: '#fff',
                  fontSize: 10,
                  position: 'absolute',
                }}>
                {'   '}
                {item.date}{' '}
              </Text>
            </Text>
          </View>
          <View style={styles.tabViewStyleSunday}>
            <Text
              maxFontSizeMultiplier={1}
              style={{ textAlign: 'center', color: '#fff' }}>
              We are off on Sundays. {'\n'}Your menu for next week will be
              updated on Monday.
            </Text>
          </View>
        </LinearGradient>
      );
    } else {
      return (
        <LinearGradient
          colors={['rgba(219,0,93,0.8)', 'rgba(167,0,255,0.8)']}
          start={{ x: -0.5, y: 0.5 }}
          end={{ x: 0.5, y: -0.5 }}
          style={styles.linearGradientCarousal}>
          <ImageBackground
            source={require('../assets/Images/plate-fork-spoon.png')}
            resizeMode="contain"
            style={{
              height: 200,
              width: 150,
              position: 'absolute',
              bottom: -10,
              right: 0,
            }}
            imageStyle={{ borderRadius: 0, flex: 1, opacity: 0.7 }}
          />
          <View style={styles.carousalCardDateSection}>
            <Text
              maxFontSizeMultiplier={1}
              style={{
                fontFamily: 'Poppins-Medium',
                color: '#000',
                fontSize: 14,
              }}>
              {item.title}
              <Text
                maxFontSizeMultiplier={1}
                style={{
                  fontFamily: 'Poppins',
                  color: '#000',
                  fontSize: 12,
                  position: 'absolute',
                }}>
                {'   '}
                {item.date}{' '}
              </Text>
            </Text>
          </View>
          <View style={styles.tabViewStyle}>
            <Tabs
              tabBarUnderlineStyle={{
                backgroundColor: '#f7e400',
                height: 2,
              }}>
              <Tab
                style={{ backgroundColor: 'transparent' }}
                heading="Lunch"
                activeTabStyle={{
                  backgroundColor: 'rgb(0,0,0,0.1)',
                }}
                activeTextStyle={{
                  fontFamily: 'Poppins-Medium',
                  color: '#fff',
                  fontSize: 18,
                }}
                key="tab1"
                textStyle={{
                  fontFamily: 'Poppins-Light',
                  color: '#bdbdbd',
                  fontSize: 14,
                }}>
                {item.lunch ? (
                  <View style={styles.flatlistItems}>
                    <FlatList
                      data={item.lunch}
                      renderItem={({ item }) => (
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.flatlistTextStyle}>
                          {item}{' '}
                        </Text>
                      )}
                      keyExtractor={item => item}
                    />
                  </View>
                ) : (
                  <Text
                    maxFontSizeMultiplier={1}
                    style={{
                      textAlign: 'center',
                      color: '#fff',
                      marginTop: 32,
                    }}>
                    We are off on Sundays
                  </Text>
                )}
              </Tab>
              <Tab
                style={{ backgroundColor: 'transparent' }}
                heading="Dinner"
                activeTextStyle={{
                  fontFamily: 'Poppins-Medium',
                  color: '#fff',
                  fontSize: 18,
                }}
                key="tab2"
                textStyle={{
                  fontFamily: 'Poppins-Light',
                  color: '#bdbdbd',
                  fontSize: 14,
                }}>
                {/*  */}
                {item.dinner ? (
                  <View style={styles.flatlistItems}>
                    <FlatList
                      data={item.dinner}
                      renderItem={({ item }) => (
                        <Text
                          maxFontSizeMultiplier={1}
                          style={styles.flatlistTextStyle}>
                          {item}{' '}
                        </Text>
                      )}
                      keyExtractor={item => item.id}
                      // listKey={item}
                    />
                  </View>
                ) : (
                  <Text
                    maxFontSizeMultiplier={1}
                    style={{
                      textAlign: 'center',
                      color: '#fff',
                      marginTop: 32,
                    }}>
                    We are off on Sundays
                  </Text>
                )}
              </Tab>
            </Tabs>
          </View>
        </LinearGradient>
      );
    }
  }

  renewSubscribeAPlan = () => {
    const startDateRenew = moment(
      this.state.userSubscriptionDetails.subscriptionEndDateUpdated,
      'DD/MM/YYYY',
    ).add(1, 'days');
    this.props.navigation.push('SubscribeAPlanNavigator', {
      screen: 'SubscribeAPlan',
      params: {
        ethnicity: this.state.userSubscriptionDetails.subscriptionEthnicity,
        pricing: this.pricing,
        dietEthnicity: this.dietEthnicity,
        userDetails: this.state.userDetails,
        startDate: startDateRenew,
        selectedNumberOfTiffins: this.state.userSubscriptionDetails.quantity,
        selectedDiet: this.state.userSubscriptionDetails.subscriptionDiet,
      },
    });
  };

  render() {
    const windowWidth = Dimensions.get('window').width;
    const windowHeight = Dimensions.get('window').height;
    // console.log(this.state.userDetails);
    console.log(this.state.cookMenuDetails);
    return (
      <StyleProvider style={getTheme(material)}>
        <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
          {CustomHeader(null, null, this.props, this.state.userDetails.name)}
          <Content contentContainerStyle={styles.contentStyle}>
            <View>
              <ImageBackground
                source={this.state.backgroundSourceImage}
                style={{ marginLeft: 16, marginRight: 16, marginTop: 30 }}
                imageStyle={{ borderRadius: 10, resizeMode: 'cover' }}>
                <View style={styles.updateCardContainer}>
                  <View style={styles.updateCardStyle}>
                    <View style={styles.updateCardImageViewStyle}>
                      <Text
                        maxFontSizeMultiplier={1}
                        style={styles.headlineText}>
                        Good {this.state.greetingTime}
                      </Text>
                      {this.state.randomQuoteNumber ? (
                        <View>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={{
                              // textAlign: 'center',
                              ...styles.subHeadingText,
                            }}>
                            {this.state.quotesJsonQuote}
                          </Text>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={{
                              fontStyle: 'italic',
                              textAlign: 'right',
                              marginTop: -10,
                              ...styles.subHeadingText,
                            }}>
                            {'\n'}- {this.state.quotesJsonAuthor}
                          </Text>
                        </View>
                      ) : null}
                    </View>
                  </View>
                </View>
              </ImageBackground>
            </View>
            {this.state.cookMenuDetails && !this.state.renewSubActive ? (
              <TouchableOpacity
                style={styles.cookInfoView}
                onPress={() => {
                  this.props.navigation.navigate('CookProfilePage', {
                    cookMenuDetails: this.state.cookMenuDetails,
                  });
                }}>
                <Image
                  style={styles.cookProfileImage}
                  source={{
                    uri: this.state.cookMenuDetails.photoLink,
                  }}
                />
                <View>
                  <Text
                    style={{
                      marginHorizontal: 8,
                      ...styles.menuCarousalTitleText,
                    }}
                    maxFontSizeMultiplier={1}>
                    {this.state.cookMenuDetails.name}{' '}
                    <Text maxFontSizeMultiplier={1}>
                      is your home chef{'\n'}for the week.
                    </Text>
                  </Text>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={{
                      marginHorizontal: 8,
                      fontFamily: 'Poppins-Light',
                      fontSize: 14,
                    }}>
                    Tap to see your home chef details
                  </Text>
                </View>
              </TouchableOpacity>
            ) : null}
            <View>
              {/* Renew Subscription */}
              {this.state.renewSubActive && !this.state.loading ? (
                <Animatable.View
                  animation="slideInLeft"
                  useNativeDriver={true}
                  style={styles.renewSubscriptionView}>
                  <TouchableOpacity
                    style={styles.referralContentRenew}
                    onPress={() => {
                      this.renewSubscribeAPlan();
                    }}>
                    {this.state.dateDiff > 0 ? (
                      <View>
                        <View style={{ flexDirection: 'row' }}>
                          <View>
                            <Text
                              maxFontSizeMultiplier={1}
                              style={styles.subHeadingTextRenew}>
                              Your Subscription ends in the next{' '}
                              <Text
                                style={{
                                  // color: '#311b92',
                                  fontFamily: 'Poppins-Medium',
                                  fontSize: 18,
                                }}>
                                {this.state.dateDiff} days.
                              </Text>
                            </Text>
                            <Text
                              maxFontSizeMultiplier={1}
                              style={styles.gridTextActionRenew}>
                              Renew Subscription
                            </Text>
                          </View>
                        </View>
                      </View>
                    ) : (
                      <View>
                        <View style={{ flexDirection: 'row' }}>
                          <View>
                            <Text
                              maxFontSizeMultiplier={1}
                              style={styles.subHeadingTextRenew}>
                              Your Subscription ends{' '}
                              <Text
                                style={{
                                  color: '#311b92',
                                  fontFamily: 'Poppins-Medium',
                                  fontSize: 18,
                                }}>
                                Today.
                              </Text>
                            </Text>
                            <Text
                              maxFontSizeMultiplier={1}
                              style={styles.gridTextActionRenew}>
                              Renew Subscription
                            </Text>
                          </View>
                        </View>
                      </View>
                    )}
                  </TouchableOpacity>
                </Animatable.View>
              ) : null}
              {this.state.renewSubActiveMessage ? (
                <Animatable.View
                  animation="slideInLeft"
                  useNativeDriver={true}
                  style={styles.renewSubscriptionSuccessView}>
                  <View style={styles.referralContentRenewMessage}>
                    <View style={{ marginHorizontal: 12 }}>
                      <Text
                        maxFontSizeMultiplier={1}
                        style={styles.subHeadingTextRenewSuccess}>
                        Your Subscription has been renewed successfully.{'\n'}
                        It will show up in your Subscription Tab when you reopen
                        the app.
                      </Text>
                    </View>
                  </View>
                </Animatable.View>
              ) : null}
              {/* Food Menu */}
              <View style={styles.contentUsableStyle2}>
                <View style={styles.menuCarousal}>
                  <View style={{ flexDirection: 'row' }}>
                    <Title
                      maxFontSizeMultiplier={1}
                      style={styles.menuCarousalTitleText}>
                      {' '}
                      My Food Menu{' '}
                    </Title>
                    {this.state.cookMenuDetails ? (
                      <TouchableOpacity
                        // style={styles.continueButton}
                        onPress={() =>
                          this.props.navigation.navigate(
                            'SubscribedUserMenuPage',
                            {
                              cookMenuDetails: this.state.cookMenuDetails,
                            },
                          )
                        }>
                        <View style={{ flexDirection: 'row' }}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.continueButtonText}>
                            View All
                          </Text>
                          <MaterialCommunityIcons
                            name="arrow-right"
                            style={{ fontSize: 18, color: '#fc2250' }}
                          />
                        </View>
                      </TouchableOpacity>
                    ) : null}
                  </View>
                  {!this.state.loading ? (
                    <View>
                      {this.state.userDetails.cook ? (
                        <View
                          style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'center',
                          }}>
                          <Carousel
                            layout={'default'}
                            ref={ref => (this.carousel = ref)}
                            data={this.state.carouselItems}
                            sliderWidth={400}
                            itemWidth={280}
                            itemHeight={100}
                            renderItem={this._renderItem.bind(this)}
                            onSnapToItem={index =>
                              this.setState({ activeIndex: index })
                            }
                          />
                        </View>
                      ) : (
                        <View
                          style={[
                            styles.noCookViewStyle,
                            {
                              width: 0.8 * windowWidth,
                              height: 0.35 * windowHeight,
                            },
                          ]}>
                          <Text
                            maxFontSizeMultiplier={1}
                            style={styles.headlineText2}>
                            Your menu will show up here.{'\n'}Please wait for a
                            cook to be assigned to you.
                          </Text>
                        </View>
                      )}
                    </View>
                  ) : (
                    <View
                      style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        height: Dimensions.get('window').width / 1.4,
                      }}>
                      <ActivityIndicator small color="#fc2250" />
                      <Text maxFontSizeMultiplier={1} style={styles.loaderText}>
                        Retrieving the menu
                      </Text>
                    </View>
                  )}
                </View>
              </View>
            </View>
            <View
              style={{
                // flex: 1,
                flexDirection: 'row',
                backgroundColor: '#cfd8dc',
                marginTop: 8,
                marginHorizontal: 16,
                borderRadius: 6,
                // position: 'absolute',
                // bottom: 0,
              }}>
              <View style={styles.betaViewStyle}>
                <Text maxFontSizeMultiplier={1} style={styles.subHeadingText}>
                  ** App is in Beta **
                </Text>
              </View>
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginRight: 12,
                }}
                onPress={() => this.setState({ modalVisible: true })}>
                <MaterialCommunityIcons
                  name="information-outline"
                  style={{ fontSize: 18, color: '#fc2250' }}
                />
                <Text
                  maxFontSizeMultiplier={1}
                  style={{
                    // textAlign: 'center',
                    marginLeft: 5,
                    fontSize: 16,
                    color: '#fc2250',
                  }}>
                  more info
                </Text>
              </TouchableOpacity>
            </View>
          </Content>
          <Modal
            animationType="slide"
            // transparent={true}
            statusBarTranslucent={true}
            visible={this.state.modalVisible}
            // onBackButtonPress={() => this.setState({ modalVisible: false })}
            onRequestClose={() => {
              this.setState({ modalVisible: false });
            }}>
            <ScrollView contentContainerStyle={styles.centeredView}>
              <View style={styles.modalView}>
                <Text maxFontSizeMultiplier={1} style={styles.headlineText2}>
                  Dear Foozinger
                </Text>
                <Text maxFontSizeMultiplier={1} style={styles.modalText}>
                  Thank you for using the beta version of the app.
                </Text>
                <Text maxFontSizeMultiplier={1} style={styles.modalTextHeader}>
                  What does it mean ?
                </Text>
                <Text maxFontSizeMultiplier={1} style={styles.modalTextContent}>
                  As we move forward in our aspiration towards providing fresh
                  home made food to everyone living away from home, we want to
                  make it easier for our customers to use our service.
                </Text>
                <Text maxFontSizeMultiplier={1} style={styles.modalTextContent}>
                  The beta version of the app brings the essential features of
                  our service. We appreciate all your feedback and feature
                  requests after you have used the app.
                </Text>
                <Text maxFontSizeMultiplier={1} style={styles.modalTextContent}>
                  We are consistently working towards providing you with a
                  better app experience.
                </Text>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <MaterialCommunityIcons
                    name="heart"
                    style={{ fontSize: 18, color: '#b71c1c' }}
                  />
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.modalTextContentSign}>
                    Team Foozinga
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    margintTop: 24,
                    marginBottom: 24,
                  }}>
                  <TouchableOpacity
                    style={{
                      justifyContent: 'flex-end',
                    }}
                    onPress={() => {
                      this.setState({ modalVisible: false });
                    }}>
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.textStyleCancel}>
                      Back
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
          </Modal>
        </Container>
      </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  contentStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
    alignItems: 'stretch',
    justifyContent: 'space-between',
  },
  contentUsableStyle: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
  },
  contentUsableStyle2: {
    marginTop: 10,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
  },
  headlineText: {
    fontFamily: 'Poppins-Medium',
    color: '#212121',
    fontSize: 18,
    // textAlign: 'center',
    textTransform: 'capitalize',
    marginBottom: 8,
  },
  headlineTextRenew: {
    fontFamily: 'Poppins-Medium',
    color: '#000',
    fontSize: 18,
    // textAlign: 'center',
    // textTransform: 'capitalize',
  },
  referralContentRenew: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 18,
    marginBottom: 24,
  },
  referralContentRenewMessage: {
    alignItems: 'center',
    // justifyContent: 'center',
    marginTop: 18,
    marginBottom: 24,
  },
  gridTextActionRenew: {
    fontFamily: 'Poppins-Medium',
    color: '#fc2250',
    marginTop: 8,
    textAlign: 'right',
  },
  subHeadingTextRenew: {
    fontFamily: 'Poppins-Regular',
    color: '#131e40',
  },
  subHeadingTextRenewSuccess: {
    fontFamily: 'Poppins-Medium',
    color: '#131e40',
  },
  // renewButton: {
  //   backgroundColor: '#fc2250',
  //   padding: 10,
  // },
  renewButtonText: {
    color: '#fc2250',
    fontFamily: 'Poppins-Regular',
  },
  headlineText2: {
    fontFamily: 'Poppins-Regular',
    color: '#424242',
    fontSize: 18,
    textAlign: 'center',
  },
  informationText: {
    marginTop: 16,
    fontFamily: 'Poppins-Regular',
    color: '#131e40',
    fontSize: 18,
    textAlign: 'center',
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#424242',
    // textAlign: 'center',
  },
  appBetaText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
    textAlign: 'center',
  },
  continueButton: {
    marginBottom: 40,
    alignSelf: 'flex-end',
  },
  continueButtonText: {
    color: '#fc2250',
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    textTransform: 'capitalize',
    marginRight: 10,
  },
  menuCarousal: {
    marginTop: 20,
  },
  menuCarousalTitleText: {
    flex: 1,
    color: '#343434',
    fontSize: 16,
    fontFamily: 'Poppins-Medium',
  },
  linearGradientCarousal: {
    marginTop: 15,
    marginLeft: -50,
    paddingRight: 15,
    borderRadius: 10,
    height: Dimensions.get('window').width / 1.4,
    width: Dimensions.get('window').width / 1.4,
  },
  carousalCardDateSection: {
    backgroundColor: 'rgba(255,255,255,0.1)',
    width: 180,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 10,
    borderBottomEndRadius: 10,
  },
  tabViewStyleSunday: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    position: 'absolute',
    top: 60,
    flexDirection: 'row',
    marginLeft: 20,
    backgroundColor: 'rgba(1,1,1,0.1)',
  },
  tabViewStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    position: 'absolute',
    top: 60,
    flexDirection: 'row',
    marginLeft: 20,
  },
  tab1SubtitleStyle: {
    color: '#fff',
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
  },
  flatlistItems: {
    marginTop: 10,
  },
  flatlistTextStyle: {
    alignSelf: 'flex-start',
    marginTop: 5,
    color: '#fff',
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    // backgroundColor: 'rgba(0,0,0,0.1)',
  },
  noCookViewStyle: {
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#fc2250',
    borderRadius: 5,
    borderWidth: 0.5,
    marginTop: 12,
    // elevation: 0.5,
  },
  // UPDATE CARD ON TOP
  updateCardContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  updateCardStyle: {
    // borderColor: '#fc2250',
    // borderWidth: 1,
    // height: '95%',
    width: '97%',
    // height: 96,
    borderRadius: 6,
    backgroundColor: 'rgba(255,255,255,0.3)',
    padding: 20,
    marginVertical: 6,
    // elevation: 10,
  },
  updateCardImageViewStyle: {
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  updateCardImageStyle: {
    height: 60,
    width: 60,
  },
  updateActiveText: {
    marginLeft: 16,
    flex: 1,
    justifyContent: 'space-evenly',
  },
  betaViewStyle: {
    flex: 1,
    margin: 12,
  },
  //Modal
  centeredView: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
  modalView: {
    backgroundColor: '#fff',
    padding: 25,
  },
  modalText: {
    fontFamily: 'Poppins-Regular',
    marginTop: 12,
    marginBottom: 8,
    textAlign: 'center',
  },
  modalTextContent: {
    fontFamily: 'Poppins-Regular',
    marginBottom: 15,
  },
  modalTextContentSign: {
    fontFamily: 'Poppins-Regular',
    marginLeft: 12,
  },
  modalTextHeader: {
    fontFamily: 'Poppins-Medium',
    marginBottom: 15,
    color: '#ef6c00',
  },
  textStyleCancel: {
    fontFamily: 'Poppins-Regular',
    fontSize: 18,
    color: '#fc2250',
    textTransform: 'capitalize',
  },
  // Modal END
  renewSubscriptionView: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#c5cae9',
    marginTop: 18,
    marginHorizontal: 16,
    // marginBottom: 8,
    borderRadius: 6,
    borderWidth: 0.5,
    borderColor: '#f50057',
    elevation: 5,
  },
  renewSubscriptionSuccessView: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#b9f6ca',
    marginTop: 18,
    marginHorizontal: 16,
    // marginBottom: 8,
    borderRadius: 6,
    // borderWidth: 0.5,
    // borderColor: '#f50057',
    elevation: 5,
  },
  cookInfoView: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: 18,
    marginHorizontal: 16,
    borderRadius: 6,
    borderWidth: 0.5,
    borderColor: '#fff',
    elevation: 5,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
  },
  cookProfileImage: {
    marginLeft: 4,
    height: 60,
    width: 50,
    borderRadius: 5,
    resizeMode: 'cover',
  },
});
