import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  PermissionsAndroid,
  Alert,
  ScrollView,
  Linking,
  Modal,
} from 'react-native';
import { Container, Title, Button, Icon } from 'native-base';
import MapView, {
  PROVIDER_GOOGLE,
  AnimatedRegion,
  Polygon,
} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import Geocoder from 'react-native-geocoding';
Geocoder.init('AIzaSyBYAEY9EGT0in18UIVzM0tlSILfCW120cI');
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import GeoFencing from 'react-native-geo-fencing';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import LottieView from 'lottie-react-native';
import { ApplicationConstants } from '../../Services/ApplicationConstants/application.service';
import * as Constants from '../../Services/AppConstants/app.consts';

export default class ServiceAreaCheck extends Component {
  #applicationConstants;
  constructor() {
    super();
    this.#applicationConstants = new ApplicationConstants();
    global.AddressAutoCounter = 1;
  }
  state = {
    userName: '',
    customerId: '',
    initialPosition: null,
    finalPosition: null,
    coordinate: new AnimatedRegion({
      latitude: null,
      longitude: null,
    }),
    geoCodeAddress: '',
    geoCodeAddressAuto: null,
    geoCodeAddressLine1: '',
    geoCodeAddressLine1Auto: null,
    onRegionChangeCounter: 0,
    geoCodeLatitude: '',
    geoCodeLongitude: '',
    // inputStyle: 'styles.inputStyleFocused',
    inputStyleHouse: 'gray',
    inputStyleBuilding: 'gray',
    inputStyleLandmark: 'gray',
    addressLandmark: '',
    addressHouseNo: '',
    addressBuildingName: '',
    polygonArray: '',
    polygonGeoArray: '',
    // serviceArea: {
    //   polygon1: [
    //     { lat: 13.067044, lng: 77.620017 },
    //     { lat: 13.051994, lng: 77.579676 },
    //     { lat: 13.014031, lng: 77.602507 },
    //     { lat: 13.0341, lng: 77.650229 },
    //     { lat: 13.033097, lng: 77.660014 },
    //     { lat: 13.067044, lng: 77.620017 }, // last point has to be same as first point
    //   ],
    //   polygon2: [
    //     { lat: 12.93205, lng: 77.67184 },
    //     { lat: 12.896, lng: 77.6798 },
    //     { lat: 12.88457, lng: 77.66484 },
    //     { lat: 12.88141, lng: 77.63715 },
    //     { lat: 12.89699, lng: 77.61599 },
    //     { lat: 12.93477, lng: 77.62041 },
    //     { lat: 12.93205, lng: 77.67184 }, // last point has to be same as first point
    //   ],
    // },
    polygon: [
      [
        { lat: 12.93205, lng: 77.67184 },
        { lat: 12.896, lng: 77.6798 },
        { lat: 12.88457, lng: 77.66484 },
        { lat: 12.88141, lng: 77.63715 },
        { lat: 12.89699, lng: 77.61599 },
        { lat: 12.93477, lng: 77.62041 },
        { lat: 12.93205, lng: 77.67184 }, // last point has to be same as first point
      ],
      [
        { latitude: 13.067044, longitude: 77.620017 },
        { latitude: 13.051994, longitude: 77.579676 },
        { latitude: 13.014031, longitude: 77.602507 },
        { latitude: 13.0341, longitude: 77.650229 },
        { latitude: 13.033097, longitude: 77.660014 },
        { latitude: 13.067044, longitude: 77.620017 }, // last point has to be same as first point
      ],
    ],
    serviceAvailable: false,
    focusAutoComplete: false,
    modalVisible: false,
  };
  requestLocationPermission = async () => {
    //Calling the permission function
    const response = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );
    // console.log(response);
    if (response === 'granted') {
      // this.locateCurrentPosition();
      this.markerAnimateToCurrentPosition();
    }
  };

  componentDidMount() {
    this.requestLocationPermission();
    // GET SERVICE AREA FROM FIREBASE
    this.#applicationConstants
      .getServiceArea()
      .then(querySnapshot => {
        let polygonArray = [];
        let polygonGeoArray = [];
        querySnapshot.forEach(documentSnapshot => {
          // console.log(documentSnapshot.data());
          let arrayCoords = [];
          let arrayGeoCoords = [];
          for (var key in documentSnapshot.data()) {
            var coord = {};
            if (documentSnapshot.data().hasOwnProperty(key)) {
              var val = documentSnapshot.data()[key];
              coord.lat = val.latitude;
              coord.lng = val.longitude;
              arrayCoords.push(val);
              arrayGeoCoords.push(coord);
            }
          }
          // console.log(arrayCoords);
          polygonArray.push(arrayCoords);
          // console.log(arrayGeoCoords);
          polygonGeoArray.push(arrayGeoCoords);
        });
        // console.log('array of polygons');
        // console.log(polygonArray);
        // console.log(polygonGeoArray);
        this.setState({
          polygonArray: polygonArray,
          polygonGeoArray: polygonGeoArray,
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  checkIfServiceAvailable(region) {
    // console.log('SERVICE AVAILABLE');
    let point = {
      lat: region.latitude,
      lng: region.longitude,
    };
    if (this.state.polygonGeoArray) {
      let allRequests = [];
      let responseArray = [];
      for (var i = 0; i < this.state.polygonGeoArray.length; i++) {
        try {
          allRequests.push(
            GeoFencing.containsLocation(point, this.state.polygonGeoArray[i])
              .then(response => responseArray.push(response))
              .catch(error => console.log('response', error)),
          );
        } catch (e) {
          // handle error
          console.log(e);
        }
      }
      Promise.all(allRequests)
        .then(values => {
          // console.log('Values of PROMISES');
          // console.log(values);
          if (values.includes(1)) {
            this.setState({ serviceAvailable: true });
          } else {
            this.setState({ serviceAvailable: false });
          }
        })
        .catch(error => console.log(error));
    }
    // console.log(this.state.serviceArea);
    // GeoFencing.containsLocation(point, this.state.polygon[1])
    //   .then(() => this.setState({ serviceAvailable: true }))

    //   .catch(() =>
    //     GeoFencing.containsLocation(point, this.state.serviceArea.polygon2)
    //       .then(() => this.setState({ serviceAvailable: true }))
    //       .catch(() => this.setState({ serviceAvailable: false })),
    //   );
    // return;
  }

  locateCurrentPosition = () => {
    let region = {
      latitude: 12.97877,
      longitude: 77.57149,
      latitudeDelta: 0.19,
      longitudeDelta: 0.2,
    };
    this.setState({
      initialPosition: region,
      // finalPosition: region,
    });
  };

  backButton = () => {
    this.props.navigation.goBack();
  };

  markerAnimateToCurrentPosition = () => {
    Geolocation.getCurrentPosition(
      position => {
        let region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.09,
          longitudeDelta: 0.035,
        };
        this.setState({
          initialPosition: region,
          // finalPosition: region,
        });
        this._map.animateToRegion({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.005,
          longitudeDelta: 0.005,
        });
      },
      error => Alert.alert(error.message),
      { enableHighAccuracy: true, timeout: 10000, maximumAge: 1000 },
    );
  };

  zoomOut = () => {
    this._map.animateToRegion({
      latitude: 12.97877,
      longitude: 77.57149,
      latitudeDelta: 0.19,
      longitudeDelta: 0.2,
    });
  };

  onRegionChange = region => {
    this.checkIfServiceAvailable(region);
    if (global.AddressAutoCounter >= 1) {
      Geocoder.from(region.latitude, region.longitude)
        .then(json => {
          var addressComponent =
            json.results[json.results.length - 7].formatted_address;
          var addressLine1 =
            json.results[0].address_components[0].short_name +
            ', ' +
            json.results[0].address_components[1].short_name;
          // console.log(json);
          this.setState({
            geoCodeAddress: addressComponent,
            geoCodeAddressLine1: addressLine1,
          });
        })
        .catch(error => console.warn(error));
      this.setState({
        finalPosition: region,
      });
    }
    global.AddressAutoCounter = global.AddressAutoCounter + 1;
  };

  addressAutoCompleteLocation = addressComponent => {
    if (addressComponent.formatted_address) {
      let region = {
        latitude: addressComponent.geometry.location.lat,
        longitude: addressComponent.geometry.location.lng,
        latitudeDelta: 0.09,
        longitudeDelta: 0.035,
      };
      this.checkIfServiceAvailable(region);
      this.setState({
        geoCodeAddress: addressComponent.formatted_address,
        geoCodeAddressLine1: addressComponent.name,
      });
      if (this._map) {
        this._map.animateToRegion({
          latitude: addressComponent.geometry.location.lat,
          longitude: addressComponent.geometry.location.lng,
          latitudeDelta: 0.0005,
          longitudeDelta: 0.005,
        });
      }
    }
  };

  sendOnWhatsApp() {
    let message =
      'Name: ' +
      this.state.userName +
      '\n' +
      'Address: ' +
      '\n' +
      'Subscription Required: ';
    let url = 'whatsapp://send?text=' + message + '&phone=91' + '7204630078';
    Linking.openURL(url)
      .then(data => {
        // console.log('WhatsApp Opened');
        this.setState({
          modalVisible: false,
          checkDinner: false,
          checkLunch: false,
          startDate: '',
          endDate: '',
        });
        this.props.navigation.goBack();
      })
      .catch(() => {
        alert('Make sure Whatsapp installed on your device');
      });
  }

  renderDescription(rowData) {
    const title = rowData.structured_formatting.main_text;
    const address = rowData.structured_formatting.secondary_text;
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <Icon type="MaterialCommunityIcons" name="map-marker-outline" />
        <View style={{ marginLeft: 10 }}>
          <Text maxFontSizeMultiplier={1} style={styles.rowTitle}>
            {title}
          </Text>
          <Text maxFontSizeMultiplier={1} style={styles.rowAddress}>
            {address}
          </Text>
        </View>
      </View>
    );
  }

  onSubmitButtonPress = () => {
    this.props.navigation.goBack();
  };

  render() {
    // console.log('POLYGON ARRAY RENDER');
    // console.log(this.state.polygonArray);
    return (
      <Container style={{ flex: 1, backgroundColor: 'rgba(255,255,255, 0.1)' }}>
        {/* {CustomHeader('Add Home Address')} */}
        <View style={styles.contentMapStyle}>
          <View style={styles.mapContainer}>
            <MapView
              style={styles.map}
              // customMapStyle={mapStyle}
              ref={map => (this._map = map)}
              showsUserLocation={false}
              provider={PROVIDER_GOOGLE}
              loadingEnabled
              initialRegion={this.state.initialPosition}
              onRegionChangeComplete={this.onRegionChange}>
              {/* {this.createPolygon()} */}
              {this.state.polygonArray ? (
                <View>
                  {this.state.polygonArray.map(coordinates => (
                    <Polygon
                      coordinates={coordinates}
                      fillColor="rgba(100, 200, 200, 0.2)"
                      strokeColor="rgba(100, 200, 200, 0.8)"
                    />
                  ))}
                </View>
              ) : null}
              {/* <Polygon
                coordinates={this.state.polygon1}
                fillColor="rgba(100, 200, 200, 0.2)"
                strokeColor="rgba(100, 200, 200, 0.8)"
              /> */}
            </MapView>
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity
                style={styles.backButton}
                onPress={() => this.backButton()}>
                <MaterialCommunityIcons
                  name="arrow-left-circle"
                  style={{ color: '#000', fontSize: 32 }}
                />
              </TouchableOpacity>
              <GooglePlacesAutocomplete
                ref={ref => (this.autoComplete = ref)}
                maxFontSizeMultiplier={1.2}
                placeholder="Search a different address"
                minLength={3} // minimum length of text to search
                autoFocus={false}
                // returnKeyType={'search'} // Can be left out for default return key
                listViewDisplayed={true} // true/false/undefined
                fetchDetails={true}
                enableHighAccuracyLocation
                enablePoweredByContainer={false}
                multiline={true}
                numberOfLines={3}
                currentLocation={false}
                styles={{
                  textInputContainer: {
                    backgroundColor: 'rgba(0,0,0,0)',
                    borderBottomWidth: 0,
                    height: 60,
                    marginLeft: 32,
                    // marginRight: 16,
                  },
                  textInput: {
                    borderWidth: 1,
                    borderColor: '#2a3452',
                    marginLeft: 16,
                    marginRight: 16,
                    height: 50,
                    color: '#5d5d5d',
                    fontSize: 16,
                  },
                  row: {
                    height: 70,
                    padding: 12,
                    backgroundColor: 'rgba(255,255,255,0.9)',
                  },
                  listView: {
                    zIndex: 16, //To popover the component outwards
                    position: 'absolute',
                    top: 60,
                  },
                }}
                onPress={(data, details = null) => {
                  this.autoComplete.setAddressText('');
                  this.addressAutoCompleteLocation(details);
                }}
                query={{
                  key: 'AIzaSyBYAEY9EGT0in18UIVzM0tlSILfCW120cI',
                  language: 'en',
                  components: 'country:in',
                  location: '12.970689, 77.584924',
                  radius: '20000',
                  // strictbounds: 'true',
                }}
                nearbyPlacesAPI="GooglePlacesSearch" //GoogleReverseGeocoding / GooglePlacesSearch
                debounce={300}
                renderRow={rowData => {
                  return <View>{this.renderDescription(rowData)}</View>;
                }}
              />
            </View>

            {this.state.initialPosition ? (
              <View style={styles.markerFixed}>
                <Image
                  style={styles.marker}
                  source={require('../assets/Images/location-pin-3.png')}
                />
              </View>
            ) : null}
            <TouchableOpacity
              style={styles.currentLocationIcon}
              onPress={() => this.markerAnimateToCurrentPosition()}>
              <Icon
                type="MaterialIcons"
                name="my-location"
                style={{ color: '#424242', fontSize: 28 }}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.zoomOutIcon}
              onPress={() => this.zoomOut()}>
              <Icon
                type="MaterialIcons"
                name="zoom-out-map"
                style={{ color: '#424242', fontSize: 28 }}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.contentAddressStyle}>
          <ScrollView contentContainerStyle={styles.addressCardView}>
            <View style={styles.addressInputStyle}>
              <View style={styles.addressLineStyle}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <MaterialCommunityIcons
                    name="map-marker-radius"
                    style={{
                      color: '#fc2250',
                      fontSize: 20,
                      marginRight: 12,
                      marginBottom: 5,
                    }}
                  />
                  <Title maxFontSizeMultiplier={1} style={styles.addressLine1}>
                    {this.state.geoCodeAddressLine1}
                  </Title>
                </View>
                <Text
                  maxFontSizeMultiplier={1}
                  numberOfLines={2}
                  style={styles.addressLine2}>
                  {this.state.geoCodeAddress}
                </Text>
              </View>
            </View>
            {!this.state.serviceAvailable ? (
              <View style={styles.serviceNotAvailableView}>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <LottieView
                    style={{ width: 100, height: 100, flex: 1 }}
                    source={require('../assets/LottieAnimation/workingHardLottie.json')}
                    autoPlay
                    loop
                  />
                  <View style={{ flex: 2, flexDirection: 'column' }}>
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.serviceNotAvailableText}>
                      We are not available at this location yet.
                    </Text>
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.serviceNotAvailableText2}>
                      We are working round the clock to reach your location as
                      soon as possible.
                    </Text>
                  </View>
                </View>
                <View style={{ flexDirection: 'column' }}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.serviceNotAvailableText3}>
                    Connect with us on our official whatsApp & {'\n'}nudge us to
                    move to your location faster.
                  </Text>
                </View>
                <Button
                  transparent
                  rounded
                  style={styles.whatsAppButton}
                  onPress={() => this.setState({ modalVisible: true })}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.whatsAppButtonText}>
                    Connect
                  </Text>
                  <MaterialCommunityIcons
                    name="whatsapp"
                    style={{ color: 'green', fontSize: 32, marginLeft: 10 }}
                  />
                </Button>
                <Button
                  rounded
                  style={styles.serviceNotAvailableContinueButton}
                  onPress={() => this.onSubmitButtonPress()}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.continueButtonText}>
                    Back
                  </Text>
                </Button>
              </View>
            ) : (
              <View style={styles.serviceAvailableView}>
                <View style={{ flexDirection: 'row' }}>
                  <LottieView
                    style={{ width: 100, height: 100, flex: 1 }}
                    source={require('../assets/LottieAnimation/deliveryLottieAnimation.json')}
                    autoPlay
                    loop
                  />
                  <View style={{ flex: 2, flexDirection: 'column' }}>
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.serviceAvailableText}>
                      Yayy!! We are available at your location.
                    </Text>
                    <Text
                      maxFontSizeMultiplier={1}
                      style={styles.serviceAvailableText2}>
                      Subscribe to to enjoy delicious, healthy Homemade meals.
                    </Text>
                  </View>
                </View>
                <Button
                  rounded
                  style={styles.continueButton}
                  onPress={() => this.onSubmitButtonPress()}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.continueButtonText}>
                    Go Back & Subscribe
                  </Text>
                </Button>
              </View>
            )}
          </ScrollView>
        </View>
        <Modal
          animationType="slide"
          transparent={true}
          statusBarTranslucent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            // Alert.alert('Modal has been closed.');
          }}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text maxFontSizeMultiplier={1} style={styles.modalText}>
                Clicking Connect will take you to our official WhatsApp
              </Text>
              <View
                style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                <Button
                  style={{ marginHorizontal: 10 }}
                  transparent
                  onPress={() => {
                    this.setState({ modalVisible: false });
                  }}>
                  <Text style={styles.textStyleCancel}>Cancel</Text>
                </Button>
                <Button
                  style={{ marginHorizontal: 10 }}
                  transparent
                  onPress={() => {
                    this.sendOnWhatsApp();
                  }}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.textStyleConnect}>
                    Connect
                  </Text>
                </Button>
              </View>
            </View>
          </View>
        </Modal>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentMapStyle: {
    flex: 4,
  },
  mapContainer: {
    // ...StyleSheet.absoluteFillObject,
    flex: 1,
  },
  map: {
    // height: '100%',
    ...StyleSheet.absoluteFillObject,
    // flex: 1,
  },
  contentAddressStyle: {
    fontFamily: 'Poppins-Regular',
    backgroundColor: '#f5f5f5',
    flex: 4,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderWidth: 1,
    borderColor: '#424242',
  },
  headlineText: {
    fontFamily: 'Poppins-Medium',
    color: '#2a3452',
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  markerFixed: {
    left: '50%',
    marginLeft: -24,
    marginTop: -48,
    position: 'absolute',
    top: '50%',
  },
  marker: {
    height: 48,
    width: 48,
  },
  backButton: {
    top: 0,
    left: 0,
    position: 'absolute',
    marginVertical: 10,
    backgroundColor: 'rgba(255,255,255,0.2)',
    borderRadius: 100,
    padding: 5,
  },
  currentLocationIcon: {
    bottom: 0,
    right: 0,
    position: 'absolute',
    margin: 20,
    backgroundColor: 'rgba(255,255,255,1)',
    borderRadius: 100,
    padding: 5,
    borderColor: 'green',
    borderWidth: 1,
  },
  zoomOutIcon: {
    bottom: 0,
    left: 0,
    position: 'absolute',
    margin: 20,
    backgroundColor: 'rgba(255,255,255,1)',
    borderRadius: 100,
    padding: 5,
    borderColor: 'green',
    borderWidth: 1,
  },
  addressCardView: {
    marginTop: 12,
    marginHorizontal: 8,
  },
  addressInputStyle: {
    flexDirection: 'row',
    marginTop: 10,
  },
  addressLine1: {
    fontFamily: 'Poppins-SemiBold',
    color: '#2a3452',
    fontSize: 18,
    width: '85%',
  },
  addressLine2: {
    fontFamily: 'Poppins-Light',
    color: '#37474f',
    fontSize: 13,
    width: '85%',
    textAlign: 'left',
    marginTop: 5,
    // marginLeft: 20,
  },
  addressLineStyle: {
    flex: 4,
  },
  addressChangeButtonView: {
    flex: 1,
  },
  changeButtonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5f5f5',
    borderRadius: 10,
    borderColor: '#eeeeee',
    borderWidth: 1,
    width: 80,
    height: 30,
  },
  addressButtonText: {
    fontFamily: 'Poppins-Medium',
    fontSize: 12,
    color: '#fc2250',
    textAlign: 'center',
  },
  serviceNotAvailableView: {
    padding: 5,
    alignItems: 'center',
    marginHorizontal: 10,
    flex: 1,
    backgroundColor: '#f5f5f5',
  },
  serviceNotAvailableText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: 'red',
    marginVertical: 5,
    // width: '85%',
  },
  serviceNotAvailableText2: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    marginVertical: 5,
    width: '85%',
  },
  serviceNotAvailableText3: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    marginTop: 5,
    width: '85%',
  },
  serviceAvailableView: {
    padding: 5,
    alignItems: 'center',
    marginHorizontal: 10,
    flex: 1,
  },
  serviceAvailableText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#00c853',
    marginVertical: 5,
    width: '85%',
  },
  serviceAvailableText2: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#000',
    marginVertical: 5,
    width: '85%',
  },
  whatsAppButton: {
    width: 200,
    borderWidth: 1,
    borderColor: 'green',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    padding: 10,
    marginTop: 15,
  },
  whatsAppButtonText: {
    textAlign: 'center',
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: 'green',
  },
  serviceNotAvailableContinueButton: {
    marginTop: 10,
    marginBottom: 20,
    alignSelf: 'center',
    width: 200,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fc2250',
    padding: 10,
  },
  continueButton: {
    marginBottom: 20,
    alignSelf: 'center',
    width: 200,
    marginTop: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fc2250',
    padding: 10,
  },
  continueButtonText: {
    textAlign: 'center',
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#fff',
  },
  //Modal
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
  modalView: {
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 25,
    paddingBottom: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
    width: '80%',
  },
  modalText: {
    fontFamily: 'Poppins-Regular',
    marginBottom: 15,
    textAlign: 'center',
  },
  textStyleConnect: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#fc2250',
  },
  textStyleCancel: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#fc2250',
  },
  // Modal END
});
