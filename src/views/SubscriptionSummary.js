import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import { CustomHeader } from '../components/CustomHeader';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Container, Content, Button, Text } from 'native-base';
import RazorpayCheckout from 'react-native-razorpay';
import * as Constants from '../../Services/AppConstants/app.consts';
import moment from 'moment';
import { UserService } from '../../Services/User/user.service';
import { NewUserService } from '../../Services/User/newUser.service';
import { NewRegistrationService } from '../../Services/User/newRegistration.service';
import { AuthService } from '../../Services/Authentication/auth.service';
import { ApplicationConstants } from '../../Services/ApplicationConstants/application.service';
import {
  posCreateOrder,
  postVerifyPayment,
} from '../../Services/razorpay-order.service';

export default class SubscriptionSummary extends Component {
  #userService;
  #newUserService;
  #newRegistrationService;
  #applicationConstants;
  #authService;
  constructor(props) {
    super(props);
    this.state = {
      lunchAddress: '',
      dinnerAddress: '',
      homeAddress: '',
      officeAddress: '',
      selectedLunchLocation: '', // Added only when user selects lunch in prev screen
      selectedDinnerLocation: '', // Added only when user selects dinner in prev screen
      foodFrequencyPerDay: '', // Lunch/Dinner/Lunch & Dinner
      subscriptionEthnicity: '',
      subscriptionPlan: '',
      subscriptionDays: '',
      diet: '', // Veg or Non-Veg
      subscriptionDateStart: '', // Format to display - Nov 05, 2020
      subscriptionStartMeal: '',
      subscriptionDateEnd: '', // Format to display - Nov 05, 2020
      subscriptionDateEndDB: '', // Format to save to fireBase - 05/11/2020
      subscriptionEndMeal: '',
      subscriptionPrice: 0,
      quantity: 1,
      taxPrice: 'Inclusive of all Taxes',
      couponValid: '',
      discountCode: '',
      discountPercent: 0,
      discountValue: 0,
      mealsAdded: 0,
      transactionNumber: '',
    };
    this.#userService = new UserService();
    this.#applicationConstants = new ApplicationConstants();
    this.#newUserService = new NewUserService();
    this.#authService = new AuthService();
    this.#newRegistrationService = new NewRegistrationService();
  }
  componentDidMount() {
    if (
      this.props.route.params.foodFrequencyPerDay.toLowerCase() === 'lunch' ||
      this.props.route.params.foodFrequencyPerDay.toLowerCase() === 'dinner'
    ) {
      this.setState({
        subscriptionStartMeal: this.props.route.params.foodFrequencyPerDay,
        subscriptionEndMeal: this.props.route.params.foodFrequencyPerDay,
      });
    } else if (
      this.props.route.params.foodFrequencyPerDay.toLowerCase() ===
      'lunch & dinner'
    ) {
      this.setState({
        subscriptionStartMeal: 'lunch',
        subscriptionEndMeal: 'dinner',
      });
    }
    const todaysDate = moment(this.props.route.params.startDate, 'DD-MM-YYYY');
    if (
      todaysDate
        .clone()
        .add(this.props.route.params.selectedPlan.days - 1, 'days')
        .format('dddd')
        .toLowerCase() === 'sunday'
    ) {
      this.setState({
        subscriptionDateEnd: todaysDate
          .clone()
          .add(this.props.route.params.selectedPlan.days - 2, 'days')
          .format('MMM DD, YYYY'),
        subscriptionDateEndDB: todaysDate
          .clone()
          .add(this.props.route.params.selectedPlan.days - 2, 'days')
          .format('DD/MM/YYYY'),
      });
    } else {
      this.setState({
        subscriptionDateEnd: todaysDate
          .clone()
          .add(this.props.route.params.selectedPlan.days - 1, 'days')
          .format('MMM DD, YYYY'),
        subscriptionDateEndDB: todaysDate
          .clone()
          .add(this.props.route.params.selectedPlan.days - 1, 'days')
          .format('DD/MM/YYYY'),
      });
    }

    this.setState({
      foodFrequencyPerDay: this.props.route.params.foodFrequencyPerDay,
      selectedDinnerLocation: this.props.route.params.selectedDinnerLocation,
      selectedLunchLocation: this.props.route.params.selectedLunchLocation,
      homeAddress: this.props.route.params.homeAddress,
      officeAddress: this.props.route.params.officeAddress,
      subscriptionPlan: this.props.route.params.selectedPlan.title,
      subscriptionEthnicity: this.props.route.params.selectedEthnicity,
      subscriptionDays: this.props.route.params.selectedPlan.days,
      diet: this.props.route.params.selectedDiet,
      subscriptionPrice: this.props.route.params.selectedPlan.price,
      quantity: this.props.route.params.selectedNumberOfTiffins,
      subscriptionDateStart: moment(
        this.props.route.params.startDate,
        'DD-MM-YYYY',
      ).format('MMM DD, YYYY'),
    });
    if (
      this.props.route.params.selectedDinnerLocation.toLowerCase() === 'home'
    ) {
      this.setState({
        dinnerAddress:
          this.props.route.params.homeAddress.addressHouseNo +
          ', ' +
          this.props.route.params.homeAddress.addressBuildingName,
      });
    } else if (
      this.props.route.params.selectedDinnerLocation.toLowerCase() === 'office'
    ) {
      this.setState({
        dinnerAddress:
          this.props.route.params.officeAddress.addressHouseNo +
          ', ' +
          this.props.route.params.officeAddress.addressBuildingName,
      });
    }

    if (
      this.props.route.params.selectedLunchLocation.toLowerCase() === 'home'
    ) {
      this.setState({
        lunchAddress:
          this.props.route.params.homeAddress.addressHouseNo +
          ', ' +
          this.props.route.params.homeAddress.addressBuildingName,
      });
    } else if (
      this.props.route.params.selectedLunchLocation.toLowerCase() === 'office'
    ) {
      this.setState({
        lunchAddress:
          this.props.route.params.officeAddress.addressHouseNo +
          ', ' +
          this.props.route.params.officeAddress.addressBuildingName,
      });
    }
  }

  /* When Pay button is pressed - Amount>0, Amount=0 */

  onPaymentPressed() {
    this.setState({
      paymentLoading: true,
    });
    var mealInformation = [];
    if (
      this.state.selectedDinnerLocation.toLowerCase() ===
      Constants.ADDRESS_HOME_SCREEN.toLowerCase()
    ) {
      mealInformation.push({
        mealType: Constants.MEALTYPE_DINNER,
        addresstype: Constants.ADDRESS_HOME_SCREEN,
      });
    }
    if (
      this.state.selectedDinnerLocation.toLowerCase() ===
      Constants.ADDRESS_OFFICE_SCREEN.toLowerCase()
    ) {
      mealInformation.push({
        mealType: Constants.MEALTYPE_DINNER,
        addresstype: Constants.ADDRESS_OFFICE_SCREEN,
      });
    }

    if (
      this.state.selectedLunchLocation.toLowerCase() ===
      Constants.ADDRESS_HOME_SCREEN.toLowerCase()
    ) {
      mealInformation.push({
        mealType: Constants.MEALTYPE_LUNCH,
        addresstype: Constants.ADDRESS_HOME_SCREEN,
      });
    }

    if (
      this.state.selectedLunchLocation.toLowerCase() ===
      Constants.ADDRESS_OFFICE_SCREEN.toLowerCase()
    ) {
      mealInformation.push({
        mealType: Constants.MEALTYPE_LUNCH,
        addresstype: Constants.ADDRESS_OFFICE_SCREEN,
      });
    }

    let totalPrice =
      this.state.subscriptionPrice * this.state.quantity -
      this.state.discountValue;

    var payment = {
      amount: this.props.route.params.selectedPlan.price,
      totalAmount: totalPrice,
      discount: this.state.discountValue,
      discountCoupon: this.state.discountCode,
      gstValue: this.state.taxPrice,
      paymentDate: moment(new Date()).format('DD/MM/YYYY [\n]hh:mm a'),
      paymentStatus: Constants.PAYMENT_SUCCESS_STATUS,
    };

    var pauseDates = [
      { date: '', meal: '' },
      { date: '', meal: '' },
      { date: '', meal: '' },
    ];

    var subscriptionData = {
      subscriptionEthnicity: this.state.subscriptionEthnicity,
      subscriptionDiet: this.state.diet,
      subscriptionPlan: this.props.route.params.selectedPlan.title,
      subscriptionFrequency: this.props.route.params.foodFrequencyPerDay,
      mealInformation: mealInformation,
      pauseDates: pauseDates,
      payment: payment,
      subscriptionStartDate: moment(
        this.props.route.params.startDate,
        'DD-MM-YYYY',
      ).format('DD/MM/YYYY'),
      subscriptionStartMeal: this.state.subscriptionStartMeal,
      subscriptionEndDate: this.state.subscriptionDateEndDB,
      subscriptionEndDateUpdated: this.state.subscriptionDateEndDB,
      subscriptionEndMeal: this.state.subscriptionEndMeal,
      subscriptionEndMealUpdated: this.state.subscriptionEndMeal,
      subscriptionDays: this.props.route.params.selectedPlan.days,
      subscriptionCreationDate: moment(new Date()).format(
        'DD/MM/YYYY [\n]hh:mm a',
      ),
      subscriptionStatus: Constants.SUBSCRIPTION_ACTIVE_STATUS,
      quantity: this.props.route.params.selectedNumberOfTiffins,
      allergies: this.props.route.params.allergies,
    };

    let userId = AuthService.getCurrentUserUserId();

    if (totalPrice > 0) {
      this.#userService.createSubscription(
        userId,
        subscriptionData,
        result => {
          this.#authService.getAuthToken((resultToken, error) => {
            if (error) {
              console.error(error);
              return;
            }
            posCreateOrder(totalPrice, userId, result.id, resultToken)
              .then(response => {
                this.setState({
                  paymentLoading: false,
                });
                if (response.isSuccess) {
                  this.setState({ transactionNumber: response.data.id });
                  // console.log(response);
                  this.callRazorPay(
                    totalPrice,
                    userId,
                    result.id,
                    response.data.id,
                  );
                }
              })
              .catch(errorApi => {
                this.setState({
                  paymentLoading: false,
                });
                console.log(errorApi);
              });
          });
        },
        error => {
          this.setState({
            paymentLoading: false,
          });
          console.log(error);
        },
      );
    } else {
      this.#userService.createSubscription(
        userId,
        subscriptionData,
        result => {
          let futureSubscriptionApplied = false;
          // console.log(result);
          if (!this.props.route.params.userDetails.activeSubscriptionID) {
            this.#userService.updateActiveSubscription(
              userId,
              result.id,
              resultUpdate => {
                console.log(resultUpdate);
              },
              errorUpdate => {
                console.error(errorUpdate);
              },
            );
            this.#userService.updateEndDate(
              userId,
              this.state.subscriptionDateEndDB,
              resultUpdate => {
                console.log(resultUpdate);
              },
              errorUpdate => {
                console.error(errorUpdate);
              },
            );
            this.#userService.updateSubStartDate(
              userId,
              moment(this.props.route.params.startDate, 'DD-MM-YYYY').format(
                'DD/MM/YYYY',
              ),
              resultUpdate => {
                console.log(resultUpdate);
              },
              errorUpdate => {
                console.error(errorUpdate);
              },
            );
            this.#newUserService.addNewUserToList(
              userId,
              this.props.route.params.userDetails.name,
              moment(this.props.route.params.startDate, 'DD-MM-YYYY').format(
                'DD/MM/YYYY',
              ),
              resultNewUser => console.log(resultNewUser),
            );
          } else {
            this.#userService.updateFutureSubscription(
              userId,
              result.id,
              resultUpdate => {
                console.log(resultUpdate);
              },
              errorUpdate => {
                console.error(errorUpdate);
              },
            );
            futureSubscriptionApplied = true;
          }
          // alert(`Success: ${data.razorpay_payment_id}`);
          this.props.navigation.navigate('paymentSuccess', {
            totalAmount: totalPrice,
            subStartDate: this.state.subscriptionDateStart,
            subEndDate: this.state.subscriptionDateEnd,
            futureSubscriptionApplied: futureSubscriptionApplied,
          });
        },
        error => {
          this.setState({
            paymentLoading: false,
          });
          console.log(error);
        },
      );
      // console.log('TOTAL IS ZERO');
    }
  }

  /* Call razorpay - verify Payment, Error handling*/

  callRazorPay(totalPrice, userId, subscriptionId, orderId) {
    var options = {
      // description: 'Subscription Payment to Foozinga',
      // image: require('../assets/Images/Foozinga-square-small.png'),
      currency: 'INR',
      key: 'rzp_test_g2AybLcLjooMr1', // test key
      amount: totalPrice * 100,
      external: {
        wallets: ['paytm'],
      },
      name: 'Foozinga',
      order_id: orderId,
      prefill: {
        email: this.props.route.params.userDetails.email,
        contact: '+91' + this.props.route.params.userDetails.mobileNumber,
        name: this.props.route.params.userDetails.name,
      },
      theme: { hide_topbar: 'false', color: '#632dc2' },
    };
    RazorpayCheckout.open(options)
      .then(order => {
        // handle success
        this.#authService.getAuthToken((resultToken, error) => {
          if (error) {
            console.error(error);
            return;
          }

          postVerifyPayment(order, userId, subscriptionId, resultToken)
            .then(result => {
              let futureSubscriptionApplied = false;
              console.log(result);
              if (!this.props.route.params.userDetails.activeSubscriptionID) {
                this.#userService.updateActiveSubscription(
                  userId,
                  subscriptionId,
                  resultUpdate => {
                    console.log(resultUpdate);
                  },
                  errorUpdate => {
                    console.error(errorUpdate);
                  },
                );
                this.#userService.updateEndDate(
                  userId,
                  this.state.subscriptionDateEndDB,
                  resultUpdate => {
                    console.log(resultUpdate);
                  },
                  errorUpdate => {
                    console.error(errorUpdate);
                  },
                );
                this.#userService.updateSubStartDate(
                  userId,
                  moment(
                    this.props.route.params.startDate,
                    'DD-MM-YYYY',
                  ).format('DD/MM/YYYY'),
                  resultUpdate => {
                    console.log(resultUpdate);
                  },
                  errorUpdate => {
                    console.error(errorUpdate);
                  },
                );
                this.#newUserService.addNewUserToList(
                  userId,
                  this.props.route.params.userDetails.name,
                  moment(
                    this.props.route.params.startDate,
                    'DD-MM-YYYY',
                  ).format('DD/MM/YYYY'),
                  resultNewUser => console.log(resultNewUser),
                );
                this.#newRegistrationService.deleteRegisteredUserFromList(
                  userId,
                );
              } else {
                this.#userService.updateFutureSubscription(
                  userId,
                  subscriptionId,
                  resultUpdate => {
                    console.log(resultUpdate);
                  },
                  errorUpdate => {
                    console.error(errorUpdate);
                  },
                );
                futureSubscriptionApplied = true;
              }
              // alert(`Success: ${data.razorpay_payment_id}`);
              this.props.navigation.navigate('paymentSuccess', {
                totalAmount: totalPrice,
                subStartDate: this.state.subscriptionDateStart,
                subEndDate: this.state.subscriptionDateEnd,
                transactionNumber: this.state.transactionNumber,
                futureSubscriptionApplied: futureSubscriptionApplied,
              });
            })
            .catch(errorRes => {
              console.error(errorRes);
            });
        });
      })
      .catch(error => {
        // handle failure
        if (error.error) {
          alert(error.error.description);
        } else if (error) {
          alert(error.code);
        }
      });
    RazorpayCheckout.onExternalWalletSelection(data => {
      alert(`External Wallet Selected: ${data.external_wallet} `);
    });
  }

  onCouponTextChange(couponCode) {
    this.setState({ discountCode: couponCode });
  }
  renderCouponIcon() {
    if (this.state.couponValid === 'false') {
      return null;
    } else if (this.state.couponValid === 'true') {
      return (
        <MaterialCommunityIcons
          style={styles.rightCouponStyle}
          name="check-decagram"
        />
      );
    }
  }
  // addMealsToSubscription(numberOfMeals) {
  //   if (
  //     this.state.foodFrequencyPerDay.toLowerCase() === 'lunch' ||
  //     this.state.foodFrequencyPerDay.toLowerCase() === 'dinner'
  //   ) {
  //     console.log(this.state.subscriptionPlan.toLowerCase());
  //   }
  // }

  /* Coupon Button - matched from Firebase and applied if found */

  applyCouponBtnPress() {
    let selectedCode = {};
    let enteredDiscountCode = this.state.discountCode.trim();
    this.#applicationConstants
      .getCouponCodes()
      .then(documentSnapshot => {
        // console.log('COUPON CODES');
        // console.log(documentSnapshot.data());
        if (
          Object.keys(documentSnapshot.data()).includes(enteredDiscountCode)
        ) {
          selectedCode = documentSnapshot.data()[enteredDiscountCode];
          if (selectedCode.type.toLowerCase() === 'percent') {
            let value =
              (selectedCode.value / 100) * this.state.subscriptionPrice;
            this.setState({
              discountValue: Math.round(value),
              // mealsAdded: 0,
              couponValid: 'true',
              discountPercent: selectedCode.value,
            });
          } else if (selectedCode.type.toLowerCase() === 'amt') {
            this.setState({
              discountValue: selectedCode.value,
              // mealsAdded: 0,
              couponValid: 'true',
              discountPercent: 0,
            });
          }
          // else if (selectedCode.type.toLowerCase() === 'meals') {
          //   this.setState({
          //     discountValue: 0,
          //     mealsAdded: selectedCode.value,
          //     couponValid: 'true',
          //   });
          //   this.addMealsToSubscription(selectedCode.value);
          // }
        } else {
          this.setState({
            couponValid: 'false',
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  /* Remove coupon Text and reset values */
  removeCouponText() {
    this.textInput.clear();
    this.setState({
      couponValid: '',
      discountCode: '',
      discountValue: 0,
      discountPercent: 0,
      // mealsAdded: 0,
    });
  }

  render() {
    let totalPrice =
      this.state.subscriptionPrice * this.state.quantity -
      this.state.discountValue;
    return (
      // <StyleProvider style={getTheme(material)}>
      <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
        {CustomHeader('Summary', true, this.props)}
        <Content contentContainerStyle={styles.contentStyle}>
          <View style={styles.contentUsableStyle}>
            {/* <View> */}
            <View style={styles.planView}>
              <Image
                style={styles.SummaryFoodImage}
                source={require('../assets/Images/SummaryFoodImage1.jpg')}
              />
              <View style={{ flexDirection: 'column', marginLeft: 12 }}>
                <Text maxFontSizeMultiplier={1} style={styles.headlineText}>
                  {this.state.subscriptionEthnicity} |{' '}
                  {this.state.foodFrequencyPerDay}
                </Text>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  {this.state.diet.toLowerCase() === 'veg' ? (
                    <Image
                      style={styles.dietSymbol}
                      source={require('../assets/Images/VegSymbol.png')}
                    />
                  ) : (
                    <Image
                      style={styles.dietSymbol}
                      source={require('../assets/Images/NonVegSymbol.png')}
                    />
                  )}
                  <Text maxFontSizeMultiplier={1} style={styles.planViewText}>
                    {this.state.subscriptionPlan} Plan -{' '}
                    {this.state.subscriptionDays} days
                  </Text>
                </View>
                <Text
                  maxFontSizeMultiplier={1}
                  style={styles.planViewDatesText}>
                  {this.state.subscriptionDateStart} -{' '}
                  {this.state.subscriptionDateEnd}
                </Text>
              </View>
            </View>
            <View style={styles.dividerLine} />
            <View style={styles.deliveryPreferencesView}>
              <View style={{ flexDirection: 'row' }}>
                <Text
                  maxFontSizeMultiplier={1}
                  style={styles.deliveryPreferencesTitle}>
                  Delivery Preferences
                </Text>
                <TouchableOpacity
                  style={styles.changeButtonStyle}
                  onPress={() => this.props.navigation.goBack()}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.changeAddressButton}>
                    Change
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            {/* Delivery Preferences Card View */}
            <View style={{ flexDirection: 'row', marginTop: 20 }}>
              {/* Lunch Details card */}
              {this.state.selectedLunchLocation ? (
                <View style={styles.deliveryPreferenceCardStyle}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.deliveryPreferenceTitle}>
                    Lunch
                  </Text>
                  <View style={styles.deliveryPreferenceDivider} />
                  <View style={styles.lunchDinnerAddressSection}>
                    <View style={{ flexDirection: 'row' }}>
                      <MaterialCommunityIcons
                        // style={{justifyContent: 'left'}}
                        name="map-marker-radius"
                        style={{
                          color: '#757575',
                          fontSize: 20,
                        }}
                      />
                      <Text
                        maxFontSizeMultiplier={1}
                        style={styles.lunchDinnerAddressTitle}>
                        {this.state.selectedLunchLocation}
                      </Text>
                    </View>
                    <Text
                      maxFontSizeMultiplier={1}
                      ellipsizeMode="tail"
                      numberOfLines={2}
                      style={styles.lunchDinnerAddressDetails}>
                      {this.state.lunchAddress}
                    </Text>
                  </View>
                </View>
              ) : null}
              {/* Dinner Details card */}
              {this.state.selectedDinnerLocation ? (
                <View style={styles.deliveryPreferenceCardStyle}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.deliveryPreferenceTitle}>
                    Dinner
                  </Text>
                  <View style={styles.deliveryPreferenceDivider} />
                  <View style={styles.lunchDinnerAddressSection}>
                    <View style={{ flexDirection: 'row' }}>
                      <MaterialCommunityIcons
                        // style={{justifyContent: 'left'}}
                        name="map-marker-radius"
                        style={{
                          color: '#757575',
                          fontSize: 20,
                        }}
                      />
                      <Text
                        maxFontSizeMultiplier={1}
                        style={styles.lunchDinnerAddressTitle}>
                        {this.state.selectedDinnerLocation}
                      </Text>
                    </View>
                    <Text
                      ellipsizeMode="tail"
                      numberOfLines={2}
                      style={styles.lunchDinnerAddressDetails}>
                      {this.state.dinnerAddress}
                    </Text>
                  </View>
                </View>
              ) : (
                <View style={styles.deliveryPreferenceCardStyleEmpty} />
              )}
            </View>
            {/* Coupon Code */}
            <View style={styles.couponViewStyle}>
              {/* <Item disabled style={styles.CouponTextboxStyle}> */}
              <View style={styles.CouponTextboxStyle}>
                <TextInput
                  maxFontSizeMultiplier={1}
                  ref={input => {
                    this.textInput = input;
                  }}
                  style={styles.couponTextboxTextStyle}
                  autoCorrect={false}
                  spellCheck={false}
                  autoCapitalize="characters"
                  editable={!this.state.couponValid}
                  placeholder="Coupon Code"
                  onChangeText={text => this.onCouponTextChange(text)}
                />
              </View>
              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                {this.renderCouponIcon()}
              </View>
              {/* </Item> */}
              {this.state.couponValid === '' ? (
                <Button
                  disabled={this.state.discountCode === ''}
                  success
                  bordered
                  style={styles.CouponButtonStyle}
                  onPress={() => this.applyCouponBtnPress()}>
                  <Text
                    style={styles.couponButtonTextStyle}
                    maxFontSizeMultiplier={1}>
                    {' '}
                    Apply{' '}
                  </Text>
                </Button>
              ) : (
                <Button
                  disabled={this.state.discountCode === ''}
                  danger
                  bordered
                  style={styles.CouponButtonStyle}
                  onPress={() => this.removeCouponText()}>
                  <Text
                    style={styles.couponButtonTextStyle}
                    maxFontSizeMultiplier={1}>
                    Remove
                  </Text>
                </Button>
              )}
            </View>
            <View style={styles.couponMessageView}>
              {this.state.couponValid === 'false' ? (
                <Text maxFontSizeMultiplier={1} style={styles.couponErrorText}>
                  Coupon code is not Valid
                </Text>
              ) : this.state.couponValid === 'true' ? (
                <Text
                  maxFontSizeMultiplier={1}
                  style={styles.couponSuccessText}>
                  Your coupon code has been applied successfully
                </Text>
              ) : null}
            </View>
            {/* BILL DETAILS */}
            <View style={styles.billDetailsView}>
              <View>
                <Text maxFontSizeMultiplier={1} style={styles.billDetailsTitle}>
                  Bill Details
                </Text>
              </View>
              <View style={{ marginTop: 8, flexDirection: 'row' }}>
                <Text
                  maxFontSizeMultiplier={1}
                  style={styles.billDetailsComponentsName}>
                  Subscription Amount
                </Text>
                <Text
                  maxFontSizeMultiplier={1}
                  style={styles.billDetailsComponentsValue}>
                  {'\u20B9'} {this.state.subscriptionPrice}
                </Text>
              </View>
              <View style={{ marginTop: 8, flexDirection: 'row' }}>
                <Text
                  maxFontSizeMultiplier={1}
                  style={styles.billDetailsComponentsName}>
                  Number of Tiffins
                </Text>
                <Text
                  maxFontSizeMultiplier={1}
                  style={styles.billDetailsComponentsValue}>
                  {'x'} {this.state.quantity}
                </Text>
              </View>
              {this.state.discountValue ? (
                <View style={{ marginTop: 8, flexDirection: 'row' }}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.billDetailsComponentsName}>
                    Discount
                  </Text>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.billDetailsComponentsValue}>
                    {'\u20B9'} {this.state.discountValue}
                  </Text>
                </View>
              ) : null}
              {this.state.discountPercent > 0 ? (
                <View style={{ marginTop: 8, flexDirection: 'row' }}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.discountSuccessText}>
                    {this.state.discountPercent} % discount has been applied
                  </Text>
                </View>
              ) : null}

              <View style={{ marginTop: 8, flexDirection: 'row' }}>
                {/* <Text style={styles.billDetailsComponentsName}>Taxes</Text>
                  <Text style={styles.billDetailsComponentsValue}>
                    {'\u20B9'} {this.state.taxPrice}
                  </Text> */}
                <Text maxFontSizeMultiplier={1} style={styles.billDetailsTaxes}>
                  (Price is inclusive of all Taxes)
                </Text>
              </View>
              <View style={styles.dividerLine} />
              <View style={{ marginTop: 12, flexDirection: 'row' }}>
                <Text
                  maxFontSizeMultiplier={1}
                  style={styles.billDetailsTotalName}>
                  Total
                </Text>
                <Text
                  maxFontSizeMultiplier={1}
                  style={styles.billDetailsTotalValue}>
                  {'\u20B9'} {totalPrice}
                </Text>
              </View>
            </View>
            {/* RazorPay Payment Button */}
            <View style={styles.paymentView}>
              {!this.state.paymentLoading ? (
                <Button
                  rounded
                  style={styles.paymentButton}
                  onPress={() => this.onPaymentPressed()}>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.paymentButtonText}>
                    PAY {'\u20B9'} {totalPrice}
                  </Text>
                </Button>
              ) : (
                <Button rounded style={styles.paymentButtonDisabled} disabled>
                  <Text
                    maxFontSizeMultiplier={1}
                    style={styles.paymentButtonText}>
                    Processing ...
                  </Text>
                </Button>
              )}
            </View>
          </View>
        </Content>
      </Container>
      // </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  contentStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
  },
  contentUsableStyle: {
    marginTop: 30,
    marginLeft: 16,
    marginRight: 16,
    flex: 1,
  },
  headlineText: {
    fontFamily: 'PoppinsMedium',
    color: '#131e40',
    fontSize: 18,
    textTransform: 'capitalize',
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  planView: {
    flexDirection: 'row',
    marginTop: 12,
    // alignItems: 'center',
  },
  SummaryFoodImage: {
    height: 80,
    width: 80,
    borderRadius: 10,
  },
  dietSymbol: {
    marginTop: 2,
    height: 20,
    width: 20,
  },
  planViewText: {
    marginTop: 2,
    backgroundColor: 'rgba(99,45,194,0.06)',
    color: '#632dc2',
    fontFamily: 'Poppins-Medium',
    padding: 2,
    marginLeft: 12,
    textTransform: 'uppercase',
  },
  planViewDatesText: {
    marginTop: 2,
    color: '#656c82',
    fontFamily: 'Poppins-Regular',
    padding: 2,
    textTransform: 'capitalize',
  },
  dividerLine: {
    marginTop: 12,
    height: 0,
    borderStyle: 'solid',
    borderWidth: 0.7,
    borderColor: '#d2d2d2',
  },
  deliveryPreferencesView: {
    marginTop: 24,
  },
  deliveryPreferencesTitle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 18,
    lineHeight: 21,
    textAlign: 'left',
    color: '#343434',
    flex: 1,
  },
  changeButtonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5f5f5',
    borderRadius: 10,
    borderColor: '#eeeeee',
    borderWidth: 1,
    width: 80,
    height: 30,
  },
  changeAddressButton: {
    color: '#fc2250',
    fontFamily: 'Poppins-Regular',
  },
  deliveryPreferenceCardStyle: {
    // flex: 1,
    width: 170,
    paddingHorizontal: 10,
    borderRadius: 10,
    paddingVertical: 10,
    // alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: '#f3e5f5',
    marginHorizontal: 5,
  },
  deliveryPreferenceCardStyleEmpty: {
    flex: 1,
  },
  deliveryPreferenceTitle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    color: '#131e40',
  },
  deliveryPreferenceDivider: {
    borderBottomColor: '#632dc2',
    borderBottomWidth: 2,
    borderRadius: 10,
    width: 80,
  },
  lunchDinnerAddressSection: {
    flex: 1,
    marginTop: 10,
  },
  lunchDinnerAddressTitle: {
    marginLeft: 10,
    color: '#343434',
    fontFamily: 'Poppins-Medium',
    fontSize: 14,
    textTransform: 'capitalize',
  },
  lunchDinnerAddressDetails: {
    marginLeft: 32,
    fontFamily: 'Poppins-Light',
    color: '#656c82',
  },
  couponViewStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 30,
  },
  CouponTextboxStyle: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 5,
    borderBottomWidth: 0.5,
  },
  couponCodeTextDisabled: {
    color: '#656c82',
  },
  couponTextboxTextStyle: {
    fontFamily: 'Poppins-Medium',
    flex: 1,
    fontSize: 16,
  },
  couponTextboxTextStyleDisabled: {
    fontFamily: 'Poppins-Medium',
    color: 'red',
  },
  CouponButtonStyle: {
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 10,
    height: 40,
    padding: 5,
  },
  couponButtonTextStyle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 14,
    textTransform: 'capitalize',
  },
  rightCouponStyle: {
    color: 'green',
    fontSize: 30,
    marginRight: 10,
  },
  wrongCouponStyle: {
    color: 'red',
    fontSize: 20,
    marginRight: 10,
  },
  couponMessageView: {
    marginTop: 16,
  },
  couponErrorText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    color: 'red',
    marginLeft: 15,
  },
  couponSuccessText: {
    fontFamily: 'Poppins-Light',
    fontSize: 13,
    color: 'green',
    marginLeft: 15,
  },
  discountSuccessText: {
    fontFamily: 'Poppins-Light',
    fontSize: 13,
    color: '#fc2250',
  },
  billDetailsView: {
    marginTop: 8,
  },
  billDetailsTitle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 18,
    lineHeight: 21,
    textAlign: 'left',
    color: '#343434',
    flex: 1,
  },
  billDetailsComponentsName: {
    flex: 1,
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#656c82',
  },
  billDetailsTaxes: {
    flex: 1,
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#656c82',
    fontStyle: 'italic',
  },
  billDetailsComponentsValue: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#656c82',
  },
  billDetailsTotalName: {
    flex: 1,
    fontFamily: 'Poppins-Medium',
    fontSize: 18,
    color: '#131e40',
  },
  paymentView: {
    marginTop: 12,
    alignItems: 'center',
    justifyContent: 'center',
  },
  paymentButton: {
    backgroundColor: '#fc2250',
    alignSelf: 'center',
    marginBottom: 20,
  },
  paymentButtonDisabled: {
    backgroundColor: '#d8dadf',
    alignSelf: 'center',
    marginBottom: 20,
  },
  paymentButtonText: {
    paddingLeft: 50,
    paddingRight: 50,
    color: '#fff',
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
  },
});
