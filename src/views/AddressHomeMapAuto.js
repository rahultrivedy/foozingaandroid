import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Container } from 'native-base';
import { AnimatedRegion } from 'react-native-maps';
import GoogleMapsAutoComplete from '../components/GoogleMapsAutoComplete.js';

export default class AddressHomeMapAuto extends Component {
  state = {
    initialPosition: null,
    coordinate: new AnimatedRegion({
      latitude: null,
      longitude: null,
    }),
  };

  render() {
    return (
      <Container>
        {/* {CustomHeader()} */}
        <View style={styles.contentStyle}>
          {/* <View style={styles.contentUsableStyle}> */}
          <GoogleMapsAutoComplete
            // notifyChange={loc => this.getCoordsFromName(loc)}
            notifyChange={loc =>
              this.props.navigation.navigate('AddressHomeMap', {
                params: { addressAutocomplete: loc },
              })
            }
          />
          {/* </View> */}
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentStyle: {
    fontFamily: 'Poppins-Regular',
    // borderTopLeftRadius: 20,
    // borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
    flex: 1,
  },
  contentUsableStyle: {
    marginLeft: 16,
    marginRight: 16,
  },
  headlineText: {
    fontFamily: 'Poppins-Medium',
    color: '#2a3452',
  },
  subHeadingText: {
    fontFamily: 'Poppins-Regular',
    color: '#656c82',
  },
  mapContainer: {
    // ...StyleSheet.absoluteFillObject,
    flex: 1,
  },
  map: {
    // height: '100%',
    ...StyleSheet.absoluteFillObject,
  },
  markerFixed: {
    left: '50%',
    marginLeft: -24,
    marginTop: -48,
    position: 'absolute',
    top: '50%',
  },
  marker: {
    height: 48,
    width: 48,
  },
  currentLocationIcon: {
    bottom: 0,
    right: 0,
    position: 'absolute',
    margin: 20,
    backgroundColor: '#fff',
    padding: 5,
  },
});
