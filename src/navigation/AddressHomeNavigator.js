import React from 'react';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import AddressHomeMap from '../views/AddressHomeMap.js';
import AddressHomeMapAuto from '../views/AddressHomeMapAuto.js';
// import AddressHomeMap from '../views/ChatTab.js';

const Stack = createStackNavigator();
export default function AddressHomeNavigator() {
  // console.log('ADDRESS HOME NAVIGATOR');
  // console.log(props.route.params.params);
  return (
    <Stack.Navigator initialRouteName="AddressHomeMap" headerMode="none">
      <Stack.Screen
        name="AddressHomeMap"
        component={AddressHomeMap}
        // params={props.route.params.params}
      />
      <Stack.Screen
        name="AddressHomeMapAuto"
        component={AddressHomeMapAuto}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
    </Stack.Navigator>
  );
}
