import React from 'react';
import {
  createStackNavigator,
  CardStyleInterpolators,
  TransitionPresets,
} from '@react-navigation/stack';
import HomeUnregisteredUser from '../views/HomeUnregisteredUser/HomeUnregisteredUser.js';
import SubscribeAPlanScreen from '../views/SubscribeAPlanScreen.js';
import DeliveryPreferenceSelection from '../views/DeliveryPreferenceSelection.js';
import AddressHomeNavigator from './AddressHomeNavigator.js';
import SubscriptionSummary from '../views/SubscriptionSummary.js';
import paymentSuccess from '../views/paymentSuccessScreen.js';

const Stack = createStackNavigator();
export default function SubscribePlanNavigator() {
  return (
    <Stack.Navigator
      initialRouteName="Home"
      headerMode="none"
      mode="card"
      screenOptions={{ ...TransitionPresets.SlideFromRightIOS }}
      animation="fade">
      <Stack.Screen name="Home" component={HomeUnregisteredUser} />
      <Stack.Screen name="SubscribeAPlan" component={SubscribeAPlanScreen} />
      <Stack.Screen
        name="DeliveryPreference"
        component={DeliveryPreferenceSelection}
      />
      <Stack.Screen
        name="AddressNavigator"
        component={AddressHomeNavigator}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forVerticalIOS,
        }}
      />
      <Stack.Screen
        name="SubscriptionSummary"
        component={SubscriptionSummary}
      />
      <Stack.Screen name="paymentSuccess" component={paymentSuccess} />
    </Stack.Navigator>
  );
}
