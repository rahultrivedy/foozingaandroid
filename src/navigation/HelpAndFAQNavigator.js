import React from 'react';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import HelpAndFaq from '../views/UserAccount/Help&FAQ.js';
import Legal from '../views/UserAccount/Help&FAQLegal.js';
import generalFAQs from '../views/UserAccount/Help&FAQGeneralFAQs.js';
import HelpAndSupport from '../views/UserAccount/Help&FAQSupport.js';
import AppIssue from '../views/UserAccount/Help&FAQAppIssue.js';
import FeedbackSuggestion from '../views/UserAccount/Help&FAQFeedback.js';

const Stack = createStackNavigator();
export default function HelpAndFAQNavigator() {
  return (
    <Stack.Navigator initialRouteName="HelpAndFaq" headerMode="none">
      <Stack.Screen
        name="HelpAndFaq"
        component={HelpAndFaq}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
      <Stack.Screen
        name="HelpAndFaqLegal"
        component={Legal}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
      <Stack.Screen
        name="generalFAQs"
        component={generalFAQs}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
      <Stack.Screen
        name="HelpAndSupport"
        component={HelpAndSupport}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
      <Stack.Screen
        name="AppIssue"
        component={AppIssue}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
      <Stack.Screen
        name="FeedbackSuggestion"
        component={FeedbackSuggestion}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
    </Stack.Navigator>
  );
}
