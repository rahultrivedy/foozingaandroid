import React from 'react';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import FooterNavigator from './FooterNavigator.js';
import FooterNavigatorRegistered from './FooterNavigatorRegistered.js';
import FooterNavigatorSubscribed from './FooterNavigatorSubscribed.js';
import SubscribePlanNavigator from './SubscribePlanNavigator.js';
import ServiceAreaCheck from '../views/ServiceAreaCheck.js';
import HelpAndFAQNavigator from './HelpAndFAQNavigator.js';
import ManageAddress from '../views/UserAccount/ManageAddresses.js';
import AddressHomeNavigator from './AddressHomeNavigator.js';
import OurSocials from '../views/UserAccount/OurSocials.js';
import GLOBAL from '../components/Global.js';

const Stack = createStackNavigator();
export default function HomeStackNavigatorReg(props) {
  if (GLOBAL.screen.state.loggedIn) {
    return (
      <Stack.Navigator initialRouteName="Footer" headerMode="none">
        <Stack.Screen name="Footer" component={FooterNavigatorRegistered} />
        <Stack.Screen
          name="SubscribeAPlanNavigator"
          component={SubscribePlanNavigator}
        />
        <Stack.Screen
          name="HelpAndFAQNavigator"
          component={HelpAndFAQNavigator}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="ManageAddress"
          component={ManageAddress}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="OurSocials"
          component={OurSocials}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        />
        <Stack.Screen
          name="AddressNavigator"
          component={AddressHomeNavigator}
          options={{
            cardStyleInterpolator: CardStyleInterpolators.forVerticalIOS,
          }}
        />
        <Stack.Screen name="ServiceAreaCheck" component={ServiceAreaCheck} />
      </Stack.Navigator>
    );
  }
}
