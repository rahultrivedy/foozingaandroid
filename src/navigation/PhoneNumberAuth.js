import React from 'react';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import OtpEntry from '../views/UserAuth/OtpEntry/OtpEntry.js';
import LoginPhoneNumber from '../views/UserAuth/PhoneNumberEntry/PhoneNumberEntry.js';
import AuthProfileScreen from '../views/UserAuth/AuthProfileScreen/AuthProfileScreen.js';
import AddressHomeNavigator from './AddressHomeNavigator.js';

const Stack = createStackNavigator();
export default function PhoneAuthNavigator(props) {
  return (
    <Stack.Navigator initialRouteName="PhoneNumber" headerMode="none">
      <Stack.Screen
        name="PhoneNumber"
        component={LoginPhoneNumber}
        initialParams={props.prop}
      />
      <Stack.Screen
        name="OtpEntry"
        component={OtpEntry}
        initialParams={props.prop}
      />
      <Stack.Screen
        name="AuthProfileScreen"
        component={AuthProfileScreen}
        initialParams={props.prop}
      />
      <Stack.Screen
        name="AddressHomeNavigator"
        component={AddressHomeNavigator}
      />
    </Stack.Navigator>
  );
}
