import React from 'react';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import FooterNavigatorSubscribed from './FooterNavigatorSubscribed.js';
import SubscribePlanNavigator from './SubscribePlanNavigator.js';
import SubscribedUserMenuPage from '../views/SubscribedUserMenuPage.js';
import ManageAddress from '../views/UserAccount/ManageAddresses.js';
import OurSocials from '../views/UserAccount/OurSocials.js';
import AddressHomeNavigator from './AddressHomeNavigator.js';
import HelpAndFAQNavigator from './HelpAndFAQNavigator.js';
import CookProfilePage from '../views/CookProfilePage.js';

const Stack = createStackNavigator();
export default function HomeStackNavigatorSubscribed(props) {
  return (
    <Stack.Navigator initialRouteName="Footer" headerMode="none">
      <Stack.Screen name="Footer" component={FooterNavigatorSubscribed} />
      <Stack.Screen
        name="SubscribeAPlanNavigator"
        component={SubscribePlanNavigator}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
      <Stack.Screen
        name="SubscribedUserMenuPage"
        component={SubscribedUserMenuPage}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
      <Stack.Screen
        name="CookProfilePage"
        component={CookProfilePage}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
      <Stack.Screen
        name="HelpAndFAQNavigator"
        component={HelpAndFAQNavigator}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
      <Stack.Screen
        name="ManageAddress"
        component={ManageAddress}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
      <Stack.Screen
        name="OurSocials"
        component={OurSocials}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
      <Stack.Screen
        name="AddressNavigator"
        component={AddressHomeNavigator}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forVerticalIOS,
        }}
      />
    </Stack.Navigator>
  );
}
