import React from 'react';
import {
  createStackNavigator,
  CardStyleInterpolators,
  TransitionPresets,
} from '@react-navigation/stack';
import HomeStackNavigatorSubscribed from './HomeStackNavigatorSubscribed.js';
import HomeStackNavigatorUnreg from './HomeStackNavigatorUnreg.js';
import HomeStackNavigatorReg from './HomeStackNavigatorReg.js';

import UserContext from '../components/userContext.js';

const Stack = createStackNavigator();
export default function ParentStackNavigator(props) {
  // console.log(props);
  // console.log(props.userSubscriptionDetails);
  // let userSubDetails = props.userSubscriptionDetails;
  if (props) {
    if (props.loggedIn) {
      return (
        <Stack.Navigator headerMode="none">
          <Stack.Screen
            name="HomeStackNavigatorReg"
            component={HomeStackNavigatorReg}
            options={{
              cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            }}
          />
        </Stack.Navigator>
      );
    } else if (props.subscribed) {
      return (
        // <UserContext.Provider value={'other required prop'}>
        <Stack.Navigator headerMode="none">
          <Stack.Screen
            name="HomeStackNavigatorSubscribed"
            component={HomeStackNavigatorSubscribed}
            options={{
              cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            }}
          />
        </Stack.Navigator>
      );
    } else {
      return (
        // <UserContext.Provider value={'other required prop'}>
        <Stack.Navigator headerMode="none">
          <Stack.Screen
            name="HomeStackNavigatorUnreg"
            component={HomeStackNavigatorUnreg}
            options={{
              cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            }}
          />
        </Stack.Navigator>
      );
    }
  }
}
