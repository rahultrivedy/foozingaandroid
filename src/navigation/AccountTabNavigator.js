import React from 'react';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import UserAccount from '../views/UserAccount/UserAccount.js';
import HelpAndFaq from '../views/UserAccount/Help&FAQ.js';
import ManageAddress from '../views/UserAccount/ManageAddresses.js';
import AddressHomeNavigator from './AddressHomeNavigator.js';

const Stack = createStackNavigator();
export default function AccountTabNavigator() {
  return (
    <Stack.Navigator initialRouteName="UserAccount" headerMode="none">
      <Stack.Screen
        name="UserAccount"
        component={UserAccount}
        // params={props.route.params.params}
      />
      <Stack.Screen
        name="HelpAndFaq"
        component={HelpAndFaq}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
      <Stack.Screen
        name="ManageAddress"
        component={ManageAddress}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
      <Stack.Screen
        name="AddressNavigator"
        component={AddressHomeNavigator}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forVerticalIOS,
        }}
      />
    </Stack.Navigator>
  );
}