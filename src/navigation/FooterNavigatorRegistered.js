/**
 * @format
 * @flow strict-local
 */

import React from 'react';
import { StyleSheet, Image } from 'react-native';
import HomeRegisteredUser from '../views/HomeRegisteredUser/HomeRegisteredUser.js';
import SubscriptionTab from '../views/SubscriptionTab.js';
import UserAccount from '../views/UserAccount/UserAccount.js';
import AccountTabNavigator from '../navigation/AccountTabNavigator.js';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

const Tab = createBottomTabNavigator();

export default function FooterNavigatorRegistered() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      backBehavior="initialRoute"
      tabBarOptions={{
        activeTintColor: '#fc2250',
        inactiveTintColor: '#2a3452',
        style: styles.tabBarStyle,
        labelStyle: styles.labelStyle,
        allowFontScaling: false,
      }}>
      <Tab.Screen
        name="Home"
        component={HomeRegisteredUser}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ focused }) => {
            let tintColor;
            if (focused) {
              tintColor = '#fc2250';
            } else {
              tintColor = '#2a3452';
            }
            return (
              <Image
                source={require('../assets/FooterIcons/FooterHome.png')}
                resizeMode="contain"
                style={{ height: 30, width: 30, tintColor: tintColor }}
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="Subscription"
        component={SubscriptionTab}
        options={{
          tabBarIcon: ({ focused }) => {
            let tintColor;
            if (focused) {
              tintColor = '#fc2250';
            } else {
              tintColor = '#2a3452';
            }
            return (
              <Image
                source={require('../assets/FooterIcons/FooterSubs.png')}
                resizeMode="contain"
                style={{ height: 30, width: 30, tintColor: tintColor }}
              />
            );
          },
        }}
      />
      <Tab.Screen
        name="Account"
        component={UserAccount}
        options={{
          tabBarIcon: ({ focused }) => {
            let tintColor;
            if (focused) {
              tintColor = '#fc2250';
            } else {
              tintColor = '#2a3452';
            }
            return (
              <Image
                source={require('../assets/FooterIcons/FooterChat.png')}
                resizeMode="contain"
                style={{
                  height: 30,
                  width: 30,
                  tintColor: tintColor,
                }}
              />
            );
          },
        }}
      />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  tabBarStyle: {
    backgroundColor: '#fff',
    height: 65,
  },
  labelStyle: {
    fontSize: 12,
    paddingBottom: 5,
    fontFamily: 'Poppins-Regular',
  },
});
