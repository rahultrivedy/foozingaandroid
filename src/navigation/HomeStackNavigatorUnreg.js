import React from 'react';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import FooterNavigator from './FooterNavigator.js';
import FooterNavigatorRegistered from './FooterNavigatorRegistered.js';
import FooterNavigatorSubscribed from './FooterNavigatorSubscribed.js';
import SubscribePlanNavigator from './SubscribePlanNavigator.js';
import ServiceAreaCheck from '../views/ServiceAreaCheck.js';
import GLOBAL from '../components/Global.js';

const Stack = createStackNavigator();
export default function HomeStackNavigatorUnreg(props) {
  // if (GLOBAL.screen.state.loggedIn) {
  //   return (
  //     <Stack.Navigator initialRouteName="Footer" headerMode="none">
  //       <Stack.Screen name="Footer" component={FooterNavigatorRegistered} />
  //       <Stack.Screen
  //         name="SubscribeAPlanNavigator"
  //         component={SubscribePlanNavigator}
  //       />
  //       <Stack.Screen name="ServiceAreaCheck" component={ServiceAreaCheck} />
  //     </Stack.Navigator>
  //   );
  // } else {
  //   return (
  //     <Stack.Navigator initialRouteName="Footer" headerMode="none">
  //       <Stack.Screen name="Footer" component={FooterNavigator} />
  //       <Stack.Screen
  //         name="SubscribeAPlanNavigator"
  //         component={SubscribePlanNavigator}
  //       />
  //       <Stack.Screen name="ServiceAreaCheck" component={ServiceAreaCheck} />
  //     </Stack.Navigator>
  //   );
  // }
  return (
    <Stack.Navigator initialRouteName="Footer" headerMode="none">
      <Stack.Screen
        name="Footer"
        component={FooterNavigator}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      />
      {/* <Stack.Screen
          name="SubscribeAPlanNavigator"
          component={SubscribePlanNavigator}
        /> */}
      <Stack.Screen name="ServiceAreaCheck" component={ServiceAreaCheck} />
    </Stack.Navigator>
  );
}
