import * as React from 'react';
import { Text, StyleSheet, Image, TouchableOpacity, View } from 'react-native';
import {
  Header,
  Left,
  Right,
  Button,
  StyleProvider,
  Body,
  Title,
  Icon,
} from 'native-base';
import getTheme from '../../native-base-theme/components';
import material from '../../native-base-theme/variables/material';
import RBSheet from 'react-native-raw-bottom-sheet';
import PhoneAuthNavigator from '../navigation/PhoneNumberAuth.js';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import GLOBAL from './Global.js';
import auth from '@react-native-firebase/auth';

function logOut() {
  auth()
    .signOut()
    .then(() =>
      GLOBAL.screen.setState({
        loggedIn: false,
        subscribed: false,
      }),
    );
}
export function CustomHeader(title, backEnabled, props, loginInfo) {
  let usernameLength = '';
  let firstName = '';
  if (loginInfo) {
    usernameLength = loginInfo.length;
    let testArray = loginInfo.split(' ');
    firstName = testArray[0];
  }

  // console.log(loginInfo);
  return (
    <StyleProvider style={getTheme(material)}>
      <Header
        noShadow
        style={styles.appbarStyle}
        androidStatusBarColor="#632dc2">
        {backEnabled ? (
          <Left>
            <Button transparent onPress={() => props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
        ) : null}
        {title ? (
          <Body
            style={{
              flex: 3,
              flexDirection: 'row',
              paddingHorizontal: 20,
            }}>
            <Title maxFontSizeMultiplier={1}>{title}</Title>
          </Body>
        ) : (
          <Left>
            <Image
              source={require('../assets/Logo/logo-white(96x96).png')}
              resizeMode="contain"
              style={{ height: 25, width: 102 }}
            />
          </Left>
        )}
        {loginInfo === 0 ? (
          <Right>
            <TouchableOpacity
              style={styles.headerRightButton}
              onPress={() => {
                this.RBSheet.open();
              }}>
              <Text maxFontSizeMultiplier={1} style={styles.headerText}>
                Login
              </Text>
            </TouchableOpacity>
            <RBSheet
              ref={ref => {
                this.RBSheet = ref;
              }}
              closeOnPressMask={false}
              onClose={args => {
                if (args === true) {
                  GLOBAL.screen.setState({
                    loggedIn: true,
                  });
                }
              }}
              height={450}
              openDuration={250}
              customStyles={{
                container: {
                  borderTopLeftRadius: 20,
                  borderTopRightRadius: 20,
                  padding: 30,
                },
              }}>
              <TouchableOpacity
                style={styles.closeButtonStyle}
                onPress={() => {
                  this.RBSheet.close();
                }}>
                <MaterialCommunityIcons
                  name="close"
                  style={{ fontSize: 25, color: 'black' }}
                />
              </TouchableOpacity>
              <PhoneAuthNavigator prop={this} />
            </RBSheet>
          </Right>
        ) : usernameLength > 0 ? (
          <Right>
            <Button
              transparent
              // onPress={() => {
              //   logOut();
              // }}
            >
              {/* <Icon
                type="MaterialCommunityIcons"
                name="account-circle"
                style={styles.loginInfoAccountPlaceHolder}
              /> */}
              <Text maxFontSizeMultiplier={1} style={styles.headerNameStyle}>
                Hi, {firstName}
              </Text>
            </Button>
          </Right>
        ) : (
          <Right>
            <View style={{ width: 30 }} />
          </Right>
        )}
      </Header>
    </StyleProvider>
  );
}

const styles = StyleSheet.create({
  appbarStyle: {
    backgroundColor: '#632dc2',
  },
  headerRightButton: {
    marginRight: 10,
  },
  headerText: {
    color: '#fff',
    fontFamily: 'Poppins-Regular',
    marginRight: 10,
  },
  continueButton: {
    backgroundColor: '#fc2250',
    marginBottom: 20,
    alignSelf: 'center',
    marginTop: 20,
  },
  continueButtonText: {
    fontFamily: 'PoppinsMedium-Regular',
    fontSize: 16,
    paddingLeft: 50,
    paddingRight: 50,
    color: '#fff',
  },
  loginInfoAccountPlaceHolder: {
    color: '#fff',
    marginRight: 10,
  },
  closeButtonStyle: {
    alignItems: 'flex-end',
  },
  headerNameStyle: {
    color: '#fff',
    fontFamily: 'Poppins-Light',
    fontStyle: 'italic',
    marginRight: 8,
  },
});
