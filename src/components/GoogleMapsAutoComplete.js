import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { Icon, Button } from 'native-base';
navigator.geolocation = require('@react-native-community/geolocation');

export default class GoogleMapsAutoComplete extends Component {
  renderDescription(rowData) {
    if (rowData.isCurrentLocation !== true) {
      const title = rowData.structured_formatting.main_text;
      const address = rowData.structured_formatting.secondary_text;
      return (
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Icon type="MaterialCommunityIcons" name="map-marker-outline" />
          <View style={{ marginLeft: 10 }}>
            <Text maxFontSizeMultiplier={1} style={styles.rowTitle}>
              {title}
            </Text>
            <Text maxFontSizeMultiplier={1} style={styles.rowAddress}>
              {address}
            </Text>
          </View>
        </View>
      );
    } else {
      return (
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Icon
            style={styles.myLocationStyle}
            type="MaterialIcons"
            name="my-location"
          />
          <View style={{ marginLeft: 10 }}>
            <Button
              transparent
              onPress={() => {
                console.log(rowData);
                this.props.notifyChange(rowData);
              }}>
              <Text
                maxFontSizeMultiplier={1}
                style={styles.currentLocationStyle}>
                Current Location
              </Text>
            </Button>
          </View>
        </View>
      );
    }
  }
  render() {
    return (
      <GooglePlacesAutocomplete
        maxFontSizeMultiplier={1}
        placeholder="Search"
        minLength={3} // minimum length of text to search
        autoFocus={true}
        // returnKeyType={'search'} // Can be left out for default return key
        listViewDisplayed={false} // true/false/undefined
        fetchDetails={true}
        enableHighAccuracyLocation
        enablePoweredByContainer={false}
        multiline={true}
        numberOfLines={3}
        currentLocation={true}
        currentLocationLabel="Current Location"
        styles={{
          textInputContainer: {
            // backgroundColor: 'rgba(0,0,0,0)',
            backgroundColor: '#fff',
            // borderWidth: 1,
            borderBottomWidth: 0,
            height: 60,
            marginLeft: 16,
            marginRight: 16,
          },
          textInput: {
            borderWidth: 1,
            borderColor: '#2a3452',
            marginLeft: 16,
            marginRight: 16,
            height: 50,
            color: '#5d5d5d',
            fontSize: 16,
          },
          predefinedPlacesDescription: {
            color: '#1faadb',
          },
          // container: {
          //   backgroundColor: '#fff',
          // },
          row: {
            height: 70,
            padding: 12,
          },
        }}
        onPress={(data, details = null) => {
          // 'details' is provided when fetchDetails = true
          this.props.notifyChange(details);
        }}
        query={{
          key: 'AIzaSyBYAEY9EGT0in18UIVzM0tlSILfCW120cI',
          language: 'en',
          components: 'country:in',
          location: '12.970689, 77.584924',
          radius: '20000',
          // strictbounds: 'true',
        }}
        nearbyPlacesAPI="GooglePlacesSearch" //GoogleReverseGeocoding / GooglePlacesSearch
        debounce={300}
        renderRow={rowData => {
          return <View>{this.renderDescription(rowData)}</View>;
        }}
        textInputProps={{ onBlur: () => {} }}
      />
    );
  }
}

const styles = StyleSheet.create({
  textInputContainer: {
    backgroundColor: 'rgba(0,0,0,0)',
    borderTopWidth: 0,
    borderBottomWidth: 0,
  },
  textInput: {
    marginLeft: 0,
    marginRight: 0,
    height: 38,
    color: '#5d5d5d',
    fontSize: 16,
  },
  predefinedPlacesDescription: {
    color: '#1faadb',
  },
  rowTitle: {
    fontWeight: 'bold',
  },
  currentLocationStyle: {
    fontFamily: 'Poppins-SemiBold',
    color: '#1565c0',
    marginTop: 10,
  },
  myLocationStyle: {
    color: '#1565c0',
    marginLeft: 20,
    marginTop: 10,
  },
});
