import React, { Component } from 'react';
import { ImageBackground, StyleSheet, View } from 'react-native';
import { Container, Content, Text, Icon } from 'native-base';
import LottieView from 'lottie-react-native';
import { CustomHeader } from './CustomHeader';

export default class InternetNotAvailableView extends Component {
  render() {
    return (
      <Container style={{ flex: 1, backgroundColor: '#632dc2' }}>
        {CustomHeader(null, null, this.props, null)}
        <ImageBackground
          source={require('../assets/Images/internetNotWorkingBckGrnd.jpg')}
          imageStyle={{ borderTopLeftRadius: 20, borderTopRightRadius: 20 }}
          style={styles.contentContainerStyle}>
          <Text maxFontSizeMultiplier={1} style={styles.upperText}>
            OOPS!! Something went wrong
          </Text>
          <View style={styles.animationView}>
            <LottieView
              style={{ width: 300, height: 300 }}
              source={require('../assets/LottieAnimation/internetNotWorking.json')}
              autoPlay
              loop
            />
          </View>
          <View>
            <Text maxFontSizeMultiplier={1} style={styles.headingText}>
              Connection Error
            </Text>
            <Text maxFontSizeMultiplier={1} style={styles.subHeadingText}>
              Please check your internet connection
            </Text>
          </View>
        </ImageBackground>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentContainerStyle: {
    fontFamily: 'Poppins-Regular',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: '#fff',
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  upperText: {
    fontFamily: 'Poppins-Medium',
    color: '#283593',
    fontSize: 20,
  },
  animationView: {
    marginTop: 50,
    marginBottom: 50,
  },
  headingText: {
    fontFamily: 'Poppins-Medium',
    fontSize: 18,
    color: '#1565c0',
    textAlign: 'center',
  },
  subHeadingText: {
    marginTop: 20,
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: '#1565c0',
  },
});
