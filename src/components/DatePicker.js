import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import DatePicker from 'react-native-datepicker';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Calendar extends Component {
  constructor(props) {
    super(props);

    if (props.minDate) {
      this.state = {
        minDate: props.minDate,
      };
    } else {
      this.state = {
        minDate: 0,
      };
    }
  }
  componentDidMount() {
    var minDate = new Date();
    minDate.setDate(minDate.getDate() + this.state.minDate);
    if (this.state.minDate > 0) {
      this.state = {
        date: minDate,
      };
    } else {
      this.state = {
        date: '',
        minDate: 0,
      };
    }
  }
  render() {
    var minDate = new Date();
    minDate.setTime(minDate.getTime() + this.state.minDate * 86400000);
    var maxDate = new Date();
    maxDate.setTime(minDate.getTime() + 7 * 86400000);
    if (this.state.minDate > 0) {
      return (
        <DatePicker
          style={styles.datePickerInput}
          date={this.state.date}
          mode="date"
          androidMode="calendar"
          showIcon={true}
          // placeholder="select date"
          format="DD-MM-YYYY"
          minDate={minDate}
          maxDate={maxDate}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          iconComponent={
            <MaterialCommunityIcons
              name="calendar-month-outline"
              style={{
                color: '#000',
                fontSize: 24,
                marginRight: 12,
              }}
            />
          }
          customStyles={{
            dateInput: {
              // marginLeft: 36,
              borderRadius: 10,
            },
            // ... You can check the source to find the other keys.
          }}
          onDateChange={date => {
            this.setState({ date: date });
            this.props.notifyChange(date);
          }}
        />
      );
    } else {
      return (
        <DatePicker
          style={styles.datePickerInput}
          date={this.state.date}
          mode="date"
          androidMode="calendar"
          showIcon={true}
          placeholder="select date"
          format="DD-MM-YYYY"
          minDate={minDate}
          maxDate={maxDate}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          iconComponent={
            <MaterialCommunityIcons
              name="calendar-month-outline"
              style={{
                color: '#000',
                fontSize: 24,
                marginRight: 12,
              }}
            />
          }
          customStyles={{
            dateInput: {
              // marginLeft: 36,
              borderRadius: 10,
            },
            // ... You can check the source to find the other keys.
          }}
          onDateChange={date => {
            this.setState({ date: date });
            this.props.notifyChange(date);
          }}
        />
      );
    }
  }
}

const styles = StyleSheet.create({
  datePickerInput: {
    width: 180,
  },
  BodyText: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
  },
});
