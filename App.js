import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import {
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  YellowBox,
} from 'react-native';
import LottieView from 'lottie-react-native';
import { Root, Text, Container, Content, Button, Header } from 'native-base';
import SplashScreen from 'react-native-splash-screen';
import ParentStackNavigator from './src/navigation/ParentStackNavigator.js';
import HomeStackNavigatorUnreg from './src/navigation/HomeStackNavigatorUnreg.js';
import HomeStackNavigatorSubscribed from './src/navigation/HomeStackNavigatorSubscribed.js';
import AppIntroSlider from 'react-native-app-intro-slider';
import AsyncStorage from '@react-native-community/async-storage';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import RBSheet from 'react-native-raw-bottom-sheet';
import PhoneAuthNavigator from './src/navigation/PhoneNumberAuth.js';
import GLOBAL from './src/components/Global.js';
import NavigationBar from 'react-native-navbar-color';
import auth from '@react-native-firebase/auth';
import { subscribeToNetInfo } from './Services/netInfo.service';
import InternetNotAvailableView from './src/components/InternetCheck';
import { UserService } from './Services/User/user.service';
import { AuthService } from './Services/Authentication/auth.service';
import { UserProvider } from './src/components/userContext.js';
import moment from 'moment';

const slides = [
  {
    key: 'one',
    brandImage: require('./src/assets/Logo/logo-white(96x96).png'),
    title: 'Homemade food. \n At Home.        At Work.',
    text: 'Subscription based homemade food.\nDelivered to your doorstep.',
    image: require('./src/assets/Images/homemadeFood.png'),
    imageStyle: {
      // justifyContent: 'center',
      alignSelf: 'center',
      height: 200,
      width: 200,
      marginTop: 20,
    },
  },
  {
    key: 'two',
    brandImage: require('./src/assets/Logo/logo-white(96x96).png'),
    title: 'Ethnicity specific food\nby handpicked HomeChefs',
    text: 'HomeChefs from different ethnicities.\nChoose the food you love.',
    image: require('./src/assets/Images/UnregisteredUserReferal.png'),
    imageStyle: {
      // justifyContent: 'center',
      alignSelf: 'center',
      height: 200,
      width: 200,
      marginTop: 20,
      borderRadius: 100,
    },
  },
  {
    key: 'three',
    brandImage: require('./src/assets/Logo/logo-white(96x96).png'),
    title: 'No Groceries.   No Cooking.\nSubscribe to Foozinga.',
    text:
      'Give your precious time to yourself\nSave Time.   Stay Safe.   Stay Healthy.',
    image: require('./src/assets/Images/SliderIllus3.jpg'),
    imageStyle: {
      // justifyContent: 'center',
      alignSelf: 'center',
      height: 200,
      width: 200,
      marginTop: 20,
      borderRadius: 100,
    },
  },
];

export default class App extends Component {
  #userService;
  constructor(props) {
    super();
    this.#userService = new UserService();
    this.unsubscribe = null;
    this.unsubscribe2 = null;
    this.state = {
      showRealApp: '',
      loading: true,
      //To show the main page of the app
      loggedIn: false,
      user: null,
      userSubscriptionDetails: '',
      userDetails: '',
    };
    GLOBAL.screen = this;
    global.isinternet = true;
    subscribeToNetInfo(this.handleFirstConnectivityChange.bind(this));
  }
  componentDidMount() {
    YellowBox.ignoreWarnings([
      'Animated: `useNativeDriver` was not specified. This is a required option and must be explicitly set to `true` or `false`',
    ]);
    let userId = AuthService.getCurrentUserUserId();

    NavigationBar.setColor('#212121');
    AsyncStorage.getItem('first_time').then(value => {
      if (this.state.user && !this.state.userDetails) {
        // GLOBAL.screen.setState({
        //   loggedIn: true,
        // });
        // this.checkUserDetails();
      }
      // console.log('FIRST_TIME');
      // console.log(value);
      if (value === null) {
        value = false;
      }
      this.setState({ showRealApp: value });
      // console.log('VALUE', value);
      if (value === false) {
        this.setState({ loading: false });
      }
    });

    // console.log('Component did mount');
    if (this.state.user) {
      this.setState({ loading: true });
      this.#userService
        .getUserDetails(userId)
        .then(documentSnapshot => {
          if (documentSnapshot.data()) {
            this.setState({
              userDetails: documentSnapshot.data(),
            });
            if (documentSnapshot.data().activeSubscriptionID) {
              GLOBAL.screen.setState({
                loggedIn: true,
                subscribed: true,
              });
              this.setState({ loading: false });
            } else {
              GLOBAL.screen.setState({
                loggedIn: true,
                subscribed: false,
              });
              this.setState({ loading: false });
            }
          }
        })
        .catch(error => console.log(error));
    }
    this.unsubscribe = auth().onAuthStateChanged(user => {
      // console.log('AUTH CHANGE USER');
      // console.log(user);
      // this.setState({
      //   loading: true,
      // });
      if (user) {
        // console.log('STILL LOGGED IN');
        let userId1 = AuthService.getCurrentUserUserId();
        this.setState({ user: user.toJSON() });
        this.#userService.getUserDetails(userId1).then(documentSnapshot => {
          // console.log(documentSnapshot);
          // this.setState({ loading: true });
          if (documentSnapshot.data()) {
            this.setState({
              userDetails: documentSnapshot.data(),
            });
          }
        });
        // this.setState({ loading: false });
      } else {
        // console.log('NO USER CODE');
        this.setState({ user: '', userDetails: '' });
        GLOBAL.screen.setState({
          loggedIn: false,
          subscribed: false,
        });
        this.setState({ loading: false });
      }
      // let userId1 = AuthService.getCurrentUserUserId();
      // console.log('USERID: ', userId1);
      // if (user) {
      //   this.setState({ user: user.toJSON() });
      //   console.log('WE ARE HERE');
      //   this.#userService.getUserDetails(userId1).then(documentSnapshot => {
      //     console.log(documentSnapshot);
      //     this.setState({ loading: true });
      //     this.setState({
      //       userDetails: documentSnapshot.data(),
      //     });
      //     if (documentSnapshot.data()) {
      //       if (documentSnapshot.data().activeSubscriptionID) {
      //         GLOBAL.screen.setState({
      //           loggedIn: true,
      //           subscribed: true,
      //         });
      //         this.setState({ loading: false });
      //       } else {
      //         GLOBAL.screen.setState({
      //           loggedIn: true,
      //           subscribed: false,
      //         });
      //         this.setState({ loading: false });
      //       }
      //     } else {
      //       this.setState({ loading: false });
      //     }
      //   });
      //   // this.setState({ loading: false });
      // } else {
      //   GLOBAL.screen.setState({
      //     loggedIn: false,
      //   });
      //   this.setState({ loading: false });
      // }
    });
    this.timeout = setInterval(() => {
      if (this.slider) {
        this.tick();
      }
    }, 4000);

    SplashScreen.hide();
  }
  componentDidUpdate() {
    // console.log('COMPONENT DID UPDATE');
    //condition when user and userDetails have been extracted but user has not logged in.
    // if (
    //   this.state.user &&
    //   this.state.userDetails &&
    //   !GLOBAL.screen.state.loggedIn
    // ) {
    //   console.log('CONDITION WHEN USER NOT LOGGED IN');
    //   this.setState({ user: '', userDetails: '' });
    //   GLOBAL.screen.setState({
    //     loggedIn: false,
    //     subscribed: false,
    //   });
    //   this.setState({ loading: false });
    // }
    if (
      this.state.user &&
      !this.state.userDetails &&
      GLOBAL.screen.state.loggedIn
    ) {
      // console.log('CONDITION WHEN LOGGED IN BUT NO USER DETAILS');
      let userId1 = AuthService.getCurrentUserUserId();
      this.#userService.getUserDetails(userId1).then(documentSnapshot => {
        // console.log(documentSnapshot);
        // this.setState({ loading: true });
        if (documentSnapshot.data()) {
          this.setState({
            userDetails: documentSnapshot.data(),
          });
        } else {
          this.setState({ user: '', userDetails: '' });
          GLOBAL.screen.setState({
            loggedIn: false,
            subscribed: false,
          });
          this.setState({ loading: false });
        }
      });
    }
    if (
      this.state.user &&
      this.state.userDetails &&
      !GLOBAL.screen.state.loggedIn
    ) {
      this.setState({ loading: true });
      let todaysDate = moment().startOf('day');
      let endDate = moment();
      let userId = AuthService.getCurrentUserUserId();
      // console.log('USERID: ', userId);
      // console.log('USER DETAILS');
      // console.log(this.state.userDetails);
      if (this.state.userDetails.activeSubscriptionID) {
        endDate = moment(this.state.userDetails.endDate, 'DD/MM/YYYY');
        let dateDiff = endDate.diff(todaysDate, 'days');
        // console.log('DATE DIFF', dateDiff);
        if (dateDiff >= 0) {
          // console.log('DATE DIFF > 0');
          GLOBAL.screen.setState({
            loggedIn: true,
            subscribed: true,
          });
          this.setState({ loading: false });
        } else if (this.state.userDetails.futureSubscriptionID) {
          //Future SubID to ActiveSUbId
          //End Date of futureSubId to EndDate
          // console.log('DATE DIFF > 0');
          this.#userService
            .getSubscriptionDetails(
              userId,
              this.state.userDetails.futureSubscriptionID,
            )
            .then(documentSnapshot => {
              this.#userService.updateFutureToActiveSubscription(
                userId,
                this.state.userDetails.futureSubscriptionID,
                documentSnapshot.data().subscriptionEndDate,
                documentSnapshot.data().subscriptionStartDate,
                resultUpdate => {
                  console.log('resultUpdate', resultUpdate);
                  this.checkUserDetails();
                },
              );
            })
            .catch(error => console.log(error));
          this.setState({ userDetails: '' });
          GLOBAL.screen.setState({
            loggedIn: true,
            subscribed: true,
          });
        } else {
          this.#userService.updateActiveSubscription(
            userId,
            '',
            resultUpdate => {
              console.log(resultUpdate);
            },
            errorUpdate => {
              console.error(errorUpdate);
            },
          );
          GLOBAL.screen.setState({
            loggedIn: true,
            subscribed: false,
          });
          this.setState({ loading: false });
        }
      } else {
        GLOBAL.screen.setState({
          loggedIn: true,
          subscribed: false,
        });
        this.setState({ loading: false });
      }
    }

    // CHECK SUBSCRIPTION
    if (
      GLOBAL.screen.state.subscribed &&
      !this.state.userDetails.activeSubscriptionID
    ) {
      let userId = AuthService.getCurrentUserUserId();
      // this.setState({ loading: true });
      this.#userService
        .getUserDetails(userId)
        .then(documentSnapshot => {
          // console.log(documentSnapshot);
          if (documentSnapshot.data()) {
            this.setState({
              userDetails: documentSnapshot.data(),
            });
            if (documentSnapshot.data().activeSubscriptionID) {
              GLOBAL.screen.setState({
                loggedIn: true,
                subscribed: true,
                // showRealApp: true,
              });
            } else {
              GLOBAL.screen.setState({
                loggedIn: true,
                subscribed: false,
                // showRealApp: true,
              });
            }
          }
        })
        .catch(error => console.log(error));
      // this.setState({ loading: false });
    }
  }

  componentWillUnmount() {
    clearInterval(this.timeout);
    // if (this.unsubscribe) {
    //   this.unsubscribe();
    // }
  }

  handleFirstConnectivityChange(state) {
    global.isinternet = state.isConnected;
    this.setState({ isinternet: state.isConnected });
  }

  i = 0;
  tick = () => {
    this.slider.goToSlide(this.i); //this.slider is ref of <AppIntroSlider....
    this.i += 1;
    if (this.i === slides.length) {
      this.i = 0;
    }
  };

  _onDone = () => {
    this.RBSheet.open();
  };

  _onSkip = () => {
    // After user skip the intro slides. Show real app through
    // navigation or simply by controlling state
    AsyncStorage.setItem('first_time', 'true').then(() => {
      this.setState({ showRealApp: true });
    });
  };

  _renderItem = ({ item }) => {
    let { height, width } = Dimensions.get('window');
    return (
      <View style={styles.sliderMainView}>
        <ImageBackground
          source={require('./src/assets/Images/colorSplash5.png')}
          style={{ width, height, resizeMode: 'cover' }}>
          <Image source={item.brandImage} style={styles.brandImageStyle} />
          <Text maxFontSizeMultiplier={1} style={styles.title}>
            {item.title}
          </Text>
          <Image style={item.imageStyle} source={item.image} />
          <Text maxFontSizeMultiplier={1} style={styles.text}>
            {item.text}
          </Text>
        </ImageBackground>
      </View>
    );
  };

  checkUserDetails = () => {
    this.setState({ loading: true });
    let userId = AuthService.getCurrentUserUserId();
    this.#userService
      .getUserDetails(userId)
      .then(documentSnapshot => {
        this.setState({ loading: true });
        // console.log('CheckUSerDETAILS');
        // console.log(documentSnapshot);

        if (documentSnapshot.data()) {
          this.setState({
            userDetails: documentSnapshot.data(),
          });
          if (documentSnapshot.data().activeSubscriptionID) {
            GLOBAL.screen.setState({
              loggedIn: true,
              subscribed: true,
              showRealApp: true,
            });
          } else {
            GLOBAL.screen.setState({
              loggedIn: true,
              subscribed: false,
              showRealApp: true,
            });
          }
        }
        this.setState({ loading: false });
      })
      .catch(() => this.setState({ loading: false }));
  };
  // GLOBAL.screen.setState({
  //   loggedIn: true,
  // });

  render() {
    // console.log('sub details in render');
    // console.log(this.state.userDetails);
    // console.log('GLOBAL VALUES');
    // console.log(GLOBAL.screen.state);
    if (!global.isinternet && !this.state.isinternet) {
      return <InternetNotAvailableView />;
    }
    if (this.state.loading) {
      return (
        <Container>
          <Header androidStatusBarColor="#632dc2" style={{ display: 'none' }} />
          <Content
            contentContainerStyle={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {/* <View>
              <ActivityIndicator size="large" color="#fc2250" />
            </View> */}
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              {/* <ActivityIndicator small color="#fc2250" /> */}
              <LottieView
                style={{ width: 80, height: 80 }}
                source={require('./src/assets/LottieAnimation/foodLoader.json')}
                autoPlay
                loop
              />
              <Text maxFontSizeMultiplier={1} style={styles.loaderText}>
                Bringing delicious homemade food {'\n'}to your HOME and OFFICE.
              </Text>
            </View>
          </Content>
        </Container>
      );
    }
    if (this.state.showRealApp) {
      if (GLOBAL.screen.state.loggedIn) {
        if (GLOBAL.screen.state.subscribed) {
          return (
            <Root>
              <NavigationContainer>
                <UserProvider value={this.state.userDetails}>
                  <ParentStackNavigator subscribed />
                </UserProvider>
              </NavigationContainer>
            </Root>
          );
        } else {
          return (
            <Root>
              <NavigationContainer>
                <UserProvider value={this.state.userDetails}>
                  <ParentStackNavigator loggedIn />
                </UserProvider>
              </NavigationContainer>
            </Root>
          );
        }
      } else if (GLOBAL.screen.state.loggedIn === false) {
        return (
          <Root>
            <NavigationContainer>
              <ParentStackNavigator />
            </NavigationContainer>
          </Root>
        );
      }
    } else if (this.state.showRealApp === false) {
      return (
        <Container>
          <Header androidStatusBarColor="#4c298c" style={{ display: 'none' }} />
          <Content
            contentContainerStyle={{
              flex: 1,
              backgroundColor: 'rgba(76, 41, 140,1)',
            }}>
            <View style={{ flex: 3 }}>
              <AppIntroSlider
                ref={ref => (this.slider = ref)}
                renderItem={this._renderItem}
                data={slides}
                dotStyle={{
                  backgroundColor: 'rgba(255,255,255,0.1)',
                  borderColor: '#fc2250',
                  borderWidth: 2,
                }}
                activeDotStyle={{ backgroundColor: '#fc2250' }}
                showDoneButton={false}
                showNextButton={false}
              />
            </View>
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                flex: 1,
                backgroundColor: '#fff',
                borderTopRightRadius: 20,
                borderTopLeftRadius: 20,
              }}>
              <View>
                <Button
                  style={styles.actionButtonStyle2}
                  onPress={() => {
                    this._onDone();
                  }}>
                  <Text style={styles.actionButtonText2}>Login</Text>
                </Button>
              </View>
              <View>
                <Button
                  transparent
                  style={styles.actionButtonStyle}
                  onPress={() => {
                    this._onSkip();
                  }}>
                  <Text uppercase={false} style={styles.actionButtonText}>
                    {' '}
                    Skip{' '}
                  </Text>
                </Button>
              </View>
            </View>
          </Content>
          <RBSheet
            ref={ref => {
              this.RBSheet = ref;
            }}
            closeOnPressMask={false}
            onClose={args => {
              if (args === true) {
                // console.log('loggedIn user');
                AsyncStorage.setItem('first_time', 'true').then(() =>
                  this.checkUserDetails(),
                );
              }
            }}
            height={450}
            openDuration={250}
            customStyles={{
              container: {
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20,
                padding: 30,
              },
            }}>
            <TouchableOpacity
              style={styles.closeButtonStyle}
              onPress={() => {
                this.RBSheet.close();
              }}>
              <MaterialCommunityIcons
                name="close"
                style={{ fontSize: 25, color: 'black' }}
              />
            </TouchableOpacity>
            <Root>
              <NavigationContainer>
                <PhoneAuthNavigator prop={this} />
              </NavigationContainer>
            </Root>
          </RBSheet>
        </Container>
      );
    } else {
      return (
        <Container>
          <Header androidStatusBarColor="#632dc2" style={{ display: 'none' }} />
          <Content
            contentContainerStyle={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {/* <View>
              <ActivityIndicator size="large" color="#fc2250" />
            </View> */}
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              {/* <ActivityIndicator small color="#fc2250" /> */}
              <LottieView
                style={{ width: 80, height: 80 }}
                source={require('./src/assets/LottieAnimation/foodLoader.json')}
                autoPlay
                loop
              />
              <Text maxFontSizeMultiplier={1} style={styles.loaderText}>
                Bringing delicious homemade food {'\n'}to your HOME and OFFICE
              </Text>
            </View>
          </Content>
        </Container>
      );
    }
  }
}

const styles = StyleSheet.create({
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sliderMainView: {
    flex: 1,
    alignItems: 'center',

    backgroundColor: 'rgba(76, 41, 140,1)',
  },
  brandImageStyle: {
    alignSelf: 'center',
    resizeMode: 'contain',
    width: 180,
    height: 44,
    marginTop: 20,
  },
  title: {
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Poppins-Medium',
    fontSize: 24,
    color: '#fff',
    textAlign: 'center',
    marginTop: 30,
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
  text: {
    marginTop: 40,
    fontFamily: 'Poppins-Regular',
    fontSize: 18,
    textAlign: 'center',
    color: '#fff',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  closeButtonStyle: {
    alignItems: 'flex-end',
  },
  actionButtonText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#fc2250',
  },
  actionButtonText2: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    color: '#fff',
  },
  actionButtonStyle: {
    justifyContent: 'center',
    width: 200,
  },
  actionButtonStyle2: {
    width: 200,
    justifyContent: 'center',
    backgroundColor: '#fc2250',
    borderRadius: 40,
  },
  loaderText: {
    color: '#fc2250',
    marginTop: 50,
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    textAlign: 'center',
  },
});
