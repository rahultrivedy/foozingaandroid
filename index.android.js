/**
 * @format
 */
import React from 'react';
import { AppRegistry } from 'react-native';
import App from './App';
import { Provider } from 'react-redux';
import { configureStore, store } from './Services/ReduxServices/store.service';

configureStore();

const Root = () => (
  <Provider store={store}>
    <App />
  </Provider>
);
console.disableYellowBox = true;
AppRegistry.registerComponent('FoozingaAndroid', () => Root);
